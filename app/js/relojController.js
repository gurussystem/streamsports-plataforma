angular.module('streamSports')
    .controller('RelojCtrl', function ($scope, $location, Broadcast, AUTH_EVENTS) {

        $scope.broadcast = Broadcast;

        // redirect if no session is loaded
        if ($scope.broadcast.getToken() == undefined){
            $scope.broadcast.clear();
            $location.path("/");
        }

        // load settings
        //$scope.broadcast.getSettings();
        
    });
