angular.module('streamSports.broadcast', ['pusher-angular'])
    .service("Broadcast", function ($q, $http, $sce, transformRequestAsFormPost, BASE_URL, $pusher) {
        var token;
        var urlLive;
        var onAir;
        var paused;
        var playingMoves;
        var id;
        var idUser;
        var title;
        var localTeamName;
        var visitorTeamName;
        var localTeam;
        var visitorTeam;
        var localScore;
        var visitorScore;
        var movements;
        var minutesLeft;
        var timeMatch;
        var period;

        function init() {
            reset();
        }

        function initPusher() {
            // init pusher
            var client = new Pusher('ac1fc38a568abb366b77', {
                encrypted: true
            });
            var pusher = $pusher(client);
            var channel = pusher.subscribe('sp_38a568abb.' + idUser);
            channel.bind('jugada', function (data) {
                if (data.idPartido == id) {
                    movements.forEach(function (move) {
                        if (move.id == data.idJugada) {
                            move.time = data.minutoJugada;
                            move.thumbnail = data.urlThumbnail;
                            move.processing = false;
                        }
                    });
                }
            });
        }

        function restoreSession(data) {
            if (data.idPartido != undefined && data.idPartido != '') {
                id = data.idPartido;
                title = data.titulo;
                localTeamName = data.local;
                localTeam = data.localCodigo;
                visitorTeamName = data.visitante;
                visitorTeam = data.visitanteCodigo;
                localScore = data.localGol;
                visitorScore = data.visitanteGol;
                if (data.fechaFinParte== null){
                  timeMatch= data.fechaIniParte;
                  period= {id:data.tituloParte,name:''};

                }

                // add movements
                movements = [];
                if (data.jugadas != undefined) {
                    data.jugadas.forEach(function(jugada){
                        move = {
                            id: jugada.idJugadas,
                            thumbnail:jugada.urlSnapshot,
                            jugador:jugada.jugador,
                            kind:jugada.idTipoJugada,
                            processing:false,
                            playing:false,
                            time:jugada.minuto
                        };
                        movements.push(move);
                    });
                }
                // check for started time
                if (data.idParte != undefined && data.idParte != '') {
                    paused = false;
                }
                // set on air
                onAir = true;
            }
        }

        function reset() {
            id = '';
            onAir = false;
            paused = true;
            title = '';
            localTeamName = '';
            visitorTeamName = '';
            localTeam = 'LOC';
            visitorTeam = 'VIS';
            localScore = 0;
            visitorScore = 0;
            movements = [];
            minutesLeft = undefined;
            playingMoves = false;
        }

        function check() {
            var def = $q.defer();

            $http({
                method: 'GET',
                url: BASE_URL + 'user/' + idUser,
                transformRequest: transformRequestAsFormPost,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
            }).success(function (response) {
                if (response != null && response.success) {
                    if (response.live == 1) {
                        def.resolve();
                    }else{
                        reset();
                        def.reject();
                    }
                } else {
                    reset();
                    def.reject();
                }
            }).error(function () {
                reset();
                def.reject();
            });
            return def.promise;
        }

        function start() {
            var def = $q.defer();

            $http({
                method: 'PUT',
                url: BASE_URL + 'inipartido',
                transformRequest: transformRequestAsFormPost,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                data: {
                    idUsuario: idUser,
                    titulo: title,
                    local: localTeamName,
                    visitante: visitorTeamName
                }
            }).success(function (response) {
                if (response != null && response.success) {
                    onAir = true;
                    id = response.idPartido;
                    def.resolve();
                } else {
                    reset();
                    def.reject();
                }
            }).error(function () {
                reset();
                def.reject();
            });
            return def.promise;
        }

        function stop() {
            var def = $q.defer();

            $http({
                method: 'POST',
                url: BASE_URL + 'finpartido/' + id,
                transformRequest: transformRequestAsFormPost,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                data: {
                    idUsuario: idUser
                }
            }).success(function (response) {
                reset();
                def.resolve();
            }).error(function () {
                def.reject();
            });
            return def.promise;
        }

        function startTime(time) {
            period=time;
            paused = false;
            $http({
                method: 'PUT',
                url: BASE_URL + 'parte/' + id,
                transformRequest: transformRequestAsFormPost,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                data: {
                    numParte: time.id,
                    idUsuario: idUser,
                    parte: time.name
                }
            });
            playingMoves = false;
            var d = new Date();
            timeMatch = d.getTime();
        }

        function endTime() {
            paused = true;
            $http({
                method: 'POST',
                url: BASE_URL + 'parte/' + id,
                transformRequest: transformRequestAsFormPost,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                data: {
                    idUsuario: idUser,
                    fin: 1
                }
            });
        }
        function changeParteTime(){
          $http({
              method: 'POST',
              url: BASE_URL + 'reloj/' + id,
              transformRequest: transformRequestAsFormPost,
              headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
              data: {
                  idUsuario: idUser,
                  tiempo: timeMatch
              }
          });

        }
        function newMove(callback) {
            $http({
                method: 'POST',
                url: BASE_URL + 'jugada',
                transformRequest: transformRequestAsFormPost,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                data: {
                    idUsuario: idUser,
                    idPartido: id
                }
            }).then(function (response) {
                if (response != null && response.data.success == 1)
                    callback(response.data.idJugada);
                else
                    callback(false);
            });
        }
        function newQuickMove(callback,move) {

            $http({
                method: 'POST',
                url: BASE_URL + 'jugada_rapida',
                transformRequest: transformRequestAsFormPost,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                data: {
                    idUsuario: idUser,
                    idPartido: id,
                    idTipoJugada: move.kind,
                    localGol: localScore,
                    visitanteGol: visitorScore
                }
            }).then(function (response) {
                if (response != null && response.data.success == 1)
                    callback(response.data.idJugada);
                else
                    callback(false);
            });
        }
        function editMove(move) {
            move.thumbnail = '';
            move.time = '';
            move.processing = true;
            move.playing = false;
            $http({
                method: 'POST',
                url: BASE_URL + 'jugada/' + move.id,
                transformRequest: transformRequestAsFormPost,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                data: {
                    idUsuario: idUser,
                    idPartido: id,
                    jugador: move.jugador,
                    idTipoJugada: move.kind
                }
            }).then(function (response) {
                movements.unshift(move);
            });
        }

        function playMove(move, play) {
            move.processing = true;
            $http({
                method: 'POST',
                url: BASE_URL + 'jugada/' + move.id,
                transformRequest: transformRequestAsFormPost,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                data: {
                    idUsuario: idUser,
                    idPartido: move.id,
                    emitir: play
                }
            }).then(function (response) {
                move.processing = false;
                //move.playing = play;
            });
        }

        function playMoves() {
            $http({
                method: 'POST',
                url: BASE_URL + 'resumen/' + id,
                transformRequest: transformRequestAsFormPost,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                data: {
                    idUsuario: idUser,
                    emitir: true
                }
            }).then(function (response) {
                playingMoves = true;
            });
        }

        function stopPlayingMoves() {
            $http({
                method: 'POST',
                url: BASE_URL + 'resumen/' + id,
                transformRequest: transformRequestAsFormPost,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                data: {
                    idUsuario: idUser,
                    emitir: false
                }
            }).then(function (response) {
                playingMoves = false;
            });
        }

        function updateScore() {
            $http({
                method: 'PUT',
                url: BASE_URL + 'marcador/' + id,
                transformRequest: transformRequestAsFormPost,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                data: {
                    idUsuario: idUser,
                    golesLocal: localScore,
                    golesVisitante: visitorScore,
                    nombreLocal: localTeam,
                    nombreVisitante: visitorTeam
                }
            });
        }

        function getSettings() {
            // get minutesLeft
            $http({
                method: 'GET',
                url: BASE_URL + 'minutesLeft',
                transformRequest: transformRequestAsFormPost,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                data: {
                    idUsuario: idUser
                }
            }).then(function (response) {
                minutesLeft = response.data.minutos;
            });
        }

        function getAds(callback) {
            // get ad list
            $http({
                method: 'GET',
                url: BASE_URL + 'publi?idUsuario=' + idUser,
                transformRequest: transformRequestAsFormPost,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                data: {}
            }).then(function (response) {
                if (response != null && response.data.success)
                    callback(response.data.lista);
                else
                    callback([]);
            });
        }

        function newAd(ad, callback) {
            // add ad
            $http({
                method: 'POST',
                url: BASE_URL + 'publi',
                transformRequest: transformRequestAsFormPost,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                data: {
                    idUsuario: idUser,
                    titulo: ad.title,
                    archivo: ad.file,
                    minutosVisible: ad.minutosVisible
                }
            }).then(function (response) {
                callback(response.data);
            });
        }

        function deleteAd(ad, callback) {
            // delete ad
            $http({
                method: 'DELETE',
                url: BASE_URL + 'publi/' + ad.idpubli,
                transformRequest: transformRequestAsFormPost,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                data: {
                    idUsuario: idUser
                }
            }).then(function (response) {
                callback(response.status == 200);
            });
        }

        function toggleAd(ad, callback) {
            // toggle ad state
            $http({
                method: 'POST',
                url: BASE_URL + 'publi/' + ad.idpubli,
                transformRequest: transformRequestAsFormPost,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                data: {
                    idUsuario: idUser,
                    activo: ad.activo,
                    titulo: null,
                    minutosVisible: null
                }
            }).then(function (response) {
                callback(response.data);
            });
        }

        function saveChangesAd(ad, callback) {
            // toggle ad state
            $http({
                method: 'POST',
                url: BASE_URL + 'publi/' + ad.idpubli,
                transformRequest: transformRequestAsFormPost,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                data: {
                    idUsuario: idUser,
                    activo: ad.activo,
                    titulo: ad.titulo,
                    minutosVisible: ad.minutosVisible
                }
            }).then(function (response) {
                callback(response.status == 200);
            });
        }

        init();
        return {
            restoreSession: function(data){
                restoreSession(data);
            },
            reset: reset,
            clear: function() {
                token = undefined;
                urlLive = undefined;
                idUser = undefined;
                reset();
            },
            getIdPartido: function () {
                return id;
            },
            getToken: function () {
                return token;
            },
            setToken: function (value) {
                token = value;
            },
            setIdUser: function (value) {
                idUser = value;
                initPusher();
            },
            getUrlLive: function () {
                return $sce.trustAsResourceUrl(urlLive);
            },
            setUrlLive: function (value) {
                urlLive = value;
            },
            setTitle: function (value) {
                title = value;
            },
            getTitle: function () {
                return title;
            },
            setLocalTeamName: function (value) {
                localTeamName = value;
                if (localTeamName.length > 2) localTeam = localTeamName.substring(0,3).toUpperCase();
            },
            setVisitorTeamName: function (value) {
                visitorTeamName = value;
                if (visitorTeamName.length > 2) visitorTeam = visitorTeamName.substring(0,3).toUpperCase();
            },
            setLocalTeam: function (value) {
                localTeam = value.toUpperCase();
            },
            getLocalTeam: function () {
                return localTeam;
            },
            setVisitorTeam: function (value) {
                visitorTeam = value.toUpperCase();
            },
            getVisitorTeam: function () {
                return visitorTeam;
            },
            getLocalScore: function () {
                return localScore;
            },
            getVisitorScore: function () {
                return visitorScore;
            },
            scoreUpLocal: function () {
                if (onAir) {
                    localScore++;
                    updateScore();
                }
            },
            scoreDownLocal: function () {
                if (onAir && localScore > 0) {
                    localScore--;
                    updateScore();
                }
            },
            scoreUpVisitor: function () {
                if (onAir) {
                    visitorScore++;
                    updateScore();
                }
            },
            scoreDownVisitor: function () {
                if (onAir && visitorScore > 0) {
                    visitorScore--;
                    updateScore();
                }
            },
            changeParteTime: function () {
                changeParteTime();
            },
            newMove: function (callback) {
                newMove(callback);
            },
            newQuickMove: function (callback,move) {
                newQuickMove(callback,move);
            },
            editMove: function (move) {
                editMove(move);
            },
            getMovements: function () {
                return movements;
            },
            playMove: function(move) {
                playMove(move, true);
            },
            stopMove: function(move) {
                playMove(move, false);
            },
            playMoves: function() {
                playMoves();
            },
            stopPlayingMoves: function() {
                stopPlayingMoves();
            },
            check: check,
            start: start,
            stop: stop,
            startTime: function (name) {
                startTime(name);
            },
            endTime: endTime,
            onAir: function () {
                return onAir;
            },
            isPaused: function () {
                return paused;
            },
            isPlayingMoves: function () {
                return playingMoves;
            },
            getSettings: function() {
                getSettings();
            },
            getAds: function(callback) {
                getAds(callback);
            },
            newAd: function(ad, callback) {
                newAd(ad, callback);
            },
            deleteAd: function(ad, callback) {
                deleteAd(ad, callback);
            },
            toggleAd: function(ad, callback) {
                toggleAd(ad, callback);
            },
            saveChangesAd: function(ad, callback) {
                saveChangesAd(ad, callback);
            },
            getMinutesLeft: function() {
                return minutesLeft;
            },
            getTimeMinutes:function(){
              var d = new Date();
              var timeNow = d.getTime()-timeMatch;
              return moment.duration(parseFloat(timeNow), "milliseconds").format("mm:ss", { trim: false });

            },
            getTimeSeconds:function(){
              var d = new Date();
              var timeNow = d.getTime()-timeMatch;
              return timeNow/1000;

            },
            getTimeMatch:function(){
              return timeMatch;
            },
            setTimeMatch:function(time){
              timeMatch=time;
            },
            getPeriod:function(){
              var value="";
              if(period!= undefined){
                if (period.id<3){
                  value= period.id+"P";
                }else{
                    value= period.name;
                }
              }

              return value;
            }
        };
    });
