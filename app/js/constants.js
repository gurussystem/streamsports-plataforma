angular.module('streamSports')
    .constant('AUTH_EVENTS', {
        notAuthenticated: 'auth-not-authenticated',
        notAuthorized: 'auth-not-authorized'
    })
    .constant('BASE_URL', 'https://apisports.streamgps.com/rest/v1/');