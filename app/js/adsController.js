angular.module('streamSports')
    .controller('AdsCtrl', function ($scope, $location, Broadcast, $ionicModal, $ionicPopup, AUTH_EVENTS) {

        $scope.broadcast = Broadcast;

        // redirect if no session is loaded
        if ($scope.broadcast.getToken() == undefined){
            $scope.broadcast.clear();
            $location.path("/");
        }

        $scope.updateAds = function(){
            $scope.broadcast.getAds(function (response) {
                $scope.ads = [];
                response.forEach(function(item){
                    $scope.ads.push(angular.copy(item));
                });
            });
        };

        // load settings
        $scope.updateAds();

        $scope.newAd = function(){
            $ionicModal.fromTemplateUrl('templates/adModal.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.adModal = modal;
                $scope.adModal.show();
            });
        };

        $scope.submitAd = function(ad) {
            $scope.broadcast.newAd(ad, function(response) {
                if (response.success) {
                    $scope.updateAds();
                    $scope.adModal.hide();
                } else {
                    $ionicPopup.alert({
                        title: 'Error',
                        template: 'No se ha podido completar la operación'
                    });
                }
            });
        };

        $scope.deleteAd = function(ad){
            var confirmPopup = $ionicPopup.confirm({
                title: 'Eliminar Banner',
                template: '¿Eliminar el banner seleccionado?'
            });
            confirmPopup.then(function(res) {
                if(res) {
                    $scope.broadcast.deleteAd(ad, function(success){
                        if (success) {
                            $scope.updateAds();
                        } else {
                            $ionicPopup.alert({
                                title: 'Error',
                                template: 'No se ha podido completar la operación'
                            });
                        }
                    });
                }
            });
        };


        $scope.toggleAd = function(ad){
            $scope.broadcast.toggleAd(ad, function(response){
                if (!response.success) {
                    // reset to original state
                    ad.activo = ad.activo == 1 ? 0 : 1;
                    $ionicPopup.alert({
                        title: 'Error',
                        template: 'No se ha podido completar la operación'
                    });
                }
            });
        };

        $scope.saveChangesAd = function(ad){
            $scope.broadcast.saveChangesAd(ad, function(success){
              if (success) {
                  $scope.updateAds();
              } else {
                  $ionicPopup.alert({
                      title: 'Error',
                      template: 'No se ha podido completar la operación'
                  });
              }
            });
        };
    });
