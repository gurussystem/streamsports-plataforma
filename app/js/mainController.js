angular.module('streamSports')
    .controller('MainCtrl', function ($scope, $ionicModal, $location, Broadcast, $ionicPopup, $timeout, AuthService,$state,$ionicActionSheet) {
        $scope.broadcast = Broadcast;
        $scope.showRetry = false;
        $scope.syncTime={};
        $scope.refreshVideo = function () {
            if ($scope.broadcast.getToken() != undefined) {
                $scope.showRetry = false;
                $scope.videoPlayer.sources = [{src: $scope.broadcast.getUrlLive(), type: "video/mp4"}];
                $timeout(function(){
                    $scope.videoPlayer.play();
                }, 1000);
            }
        };

        // redirect if no session is loaded
        if ($scope.broadcast.getToken() == undefined){
            $scope.broadcast.clear();
            $location.path("/");
            $scope.showRetry = true;
        }
        $scope.onPlayerReady = function(videoPlayer) {
            $scope.videoPlayer = videoPlayer;
            $scope.refreshVideo();
        };

        $scope.movements = Broadcast.getMovements();

        $ionicModal.fromTemplateUrl('templates/broadcastModal.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.broadcastModal = modal;
        });

        $ionicModal.fromTemplateUrl('templates/moveModal.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.moveModal = modal;
        });

        $scope.onPlayerError = function() {
            $scope.showRetry = true;
        };

        $scope.startBroadcast = function () {
          $scope.broadcast.reset();
          $scope.step = 1;
          $scope.broadcastModal.show();
          /*
            $scope.broadcast.check().then(function(){
                $scope.broadcast.reset();
                $scope.step = 1;
                $scope.broadcastModal.show();
            }, function(){
                $ionicPopup.alert({
                    title: 'Error',
                    template: 'Stream no activado. Pon el stream a LIVE y vuelve a intentar'
                });
            });
            */
        };

        $scope.nextStep = function () {
            $scope.step++;
            var input = document.getElementById("boradcastModalInput"+$scope.step);
            if (input != undefined) {
                $timeout(function(){
                    input.focus();
                }, 500);
            }
        };

        $scope.submitBroadcast = function (broadcast) {
            $scope.broadcastModal.hide();
            $scope.broadcast.setTitle(broadcast.title);
            $scope.broadcast.setLocalTeamName(broadcast.localTeamName);
            $scope.broadcast.setVisitorTeamName(broadcast.visitorTeamName);
            $scope.broadcast.start().then(function () {

            }, function () {
                $ionicPopup.alert({
                    title: 'Error',
                    template: 'Ha ocurrido un error'
                });
            });
        };

        $scope.endTimeBroadcast = function () {
            $scope.broadcast.endTime();
        };
        $scope.openTimePicker = function (){
          $scope.syncTime.minutes=0;
          $scope.syncTime.seg=0;
          $scope.syncTime.step=1;
          $ionicModal.fromTemplateUrl('templates/relojModal.html', {
              scope: $scope,
              animation: 'slide-in-up'
          }).then(function (modal) {
              $scope.relojModal = modal;
              $scope.relojModal.show();
          });
        }
        //Increasing the minutes
        $scope.increaseMinutes = function () {
          $scope.syncTime.minutes = Number($scope.syncTime.minutes);
          $scope.syncTime.minutes = ($scope.syncTime.minutes + $scope.syncTime.step) % 60;
          $scope.syncTime.minutes = ($scope.syncTime.minutes < 10) ? ('0' + $scope.syncTime.minutes) : $scope.syncTime.minutes;
        };

        //Decreasing the minutes
        $scope.decreaseMinutes = function () {
          $scope.syncTime.minutes = Number($scope.syncTime.minutes);
          $scope.syncTime.minutes = ($scope.syncTime.minutes + (60 - $scope.syncTime.step)) % 60;
          $scope.syncTime.minutes = ($scope.syncTime.minutes < 10) ? ('0' + $scope.syncTime.minutes) : $scope.syncTime.minutes;
        };
        //Increasing the seconds
        $scope.increaseSeconds = function () {
          $scope.syncTime.seg = Number($scope.syncTime.seg);
          $scope.syncTime.seg = ($scope.syncTime.seg + $scope.syncTime.step) % 60;
          $scope.syncTime.seg = ($scope.syncTime.seg < 10) ? ('0' + $scope.syncTime.seg) : $scope.syncTime.seg;
        };

        //Decreasing the seconds
        $scope.decreaseSeconds = function () {
          $scope.syncTime.seg = Number($scope.syncTime.seg);
          $scope.syncTime.seg = ($scope.syncTime.seg + (60 - $scope.syncTime.step)) % 60;
          $scope.syncTime.seg = ($scope.syncTime.seg < 10) ? ('0' + $scope.syncTime.seg) : $scope.syncTime.seg;
        };
        $scope.relojModalGuardarCambios = function(){
          console.log("relojModalGuardarCambios");
          console.log("$scope.broadcast.timeMatch:"+$scope.broadcast.timeMatch);
          console.log("getTimeMatch:"+$scope.broadcast.getTimeMatch());
          var segundosOffset= $scope.syncTime.minutes*60+$scope.syncTime.seg;
          var m = moment($scope.broadcast.getTimeMatch());
          m.add(segundosOffset + $scope.broadcast.getTimeSeconds(),'seconds');
          $scope.broadcast.setTimeMatch(m.valueOf());
          $scope.broadcast.changeParteTime();
          $scope.relojModal.hide();
          console.log("segundosOffset:"+segundosOffset);
          console.log("$scope.broadcast.getTimeSeconds():"+$scope.broadcast.getTimeSeconds());
          console.log("m.valueOf();:"+m.valueOf());


        }
        $scope.startTimeBroadcast = function (time) {
            if (time == undefined) {
                $ionicModal.fromTemplateUrl('templates/timeModal.html', {
                    scope: $scope,
                    animation: 'slide-in-up'
                }).then(function (modal) {
                    $scope.timeModal = modal;
                    $scope.timeModal.show();
                });
            } else {
                $scope.timeModal.hide();
                $scope.broadcast.startTime(time);
            }
        };

        $scope.finishBroadcast = function () {
            $scope.broadcast.stop().then(function () {
                //$scope.$digest();
            }, function () {
                $ionicPopup.alert({
                    title: 'Error',
                    template: 'Ha ocurrido un error'
                });
            });
        };

        $scope.scoreUpLocal = function () {
            $scope.broadcast.scoreUpLocal();

        };

        $scope.scoreDownLocal = function () {
            $scope.broadcast.scoreDownLocal();
        };

        $scope.scoreUpVisitor = function () {
            $scope.broadcast.scoreUpVisitor();
        };

        $scope.scoreDownVisitor = function () {
            $scope.broadcast.scoreDownVisitor();
        };

        $scope.newMove = function () {
            $scope.broadcast.newMove(function (moveId) {
                if (moveId) {
                    $scope.move = {id: moveId};
                    $scope.moveModal.show();
                } else {
                    console.log("error");
                }
            });
        };

        $scope.submitMove = function (move) {
            $scope.broadcast.editMove(move);
            $scope.moveModal.hide();
        };

        $scope.playMove = function (move) {
            $scope.broadcast.playMove(move);
        };

        $scope.stopPlay = function (move) {
            $scope.broadcast.stopPlay(move);
        };

        $scope.playMoves = function () {
            $scope.broadcast.playMoves();
        };

        $scope.stopPlayingMoves = function () {
            $scope.broadcast.stopPlayingMoves();
        };

        $scope.logout = function() {
            $scope.broadcast.reset();
            AuthService.logout();
            $location.path("/");
        }
        $scope.publi = function() {
            $state.go("publi");
        }

    var mytimeout = null; // the current timeoutID
    // actual timer method, counts down every second, stops on zero
    $scope.onTimeout = function() {

        $scope.counter=$scope.broadcast.getTimeMinutes();
        mytimeout = $timeout($scope.onTimeout, 1000);
    };
    $scope.startTimer = function() {
        mytimeout = $timeout($scope.onTimeout, 1000);
    };
    // stops and resets the current timer
    $scope.stopTimer = function() {
        $scope.$broadcast('timer-stopped', $scope.counter);

        $timeout.cancel(mytimeout);
        $scope.counter="00:00";
    };
    // triggered, when the timer stops, you can do something here, maybe show a visual indicator or vibrate the device
    $scope.$on('timer-stopped', function(event, remaining) {

    });
    if($scope.broadcast.getTimeMatch()!= undefined){
      $scope.startTimer();
    }

    $scope.showEventos = function() {

  // Show the action sheet
  var hideSheet = $ionicActionSheet.show({
    buttons: [
      { text: '<b>Gol Local</b>' },
      { text: '<b>Gol Visitante</b>' },
      { text: 'Jugada Peligrosa' },
      { text: 'Anular Gol Local' },
      { text: 'Anular Gol Visitante' }
    ],
    titleText: 'Tipo Evento',
    cancelText: 'Cancel',
    cancel: function() {
         // add cancel code..
       },
    buttonClicked: function(index) {
      //TIPO JUGADAS:
            // 1 GOL
            // 2 Amarilla
            // 3 Doble Amarilla
            // 4 Roja
            // 5 Otras
            // 11 GOL Local
            // 12 Amarilla Local
            // 13 Doble Amarilla Local
            // 14 Roja Local
            // 15 Jugada Peligrosa Local
            // 21 GOL Visitante
            // 22 Amarilla Visitante
            // 23 Doble Amarilla Visitante
            // 24 Roja Visitante
            // 25 Jugada Peligrosa Visitante
      if(index==0){
        $scope.broadcast.scoreUpLocal();
        move = {id: $scope.broadcast.getIdPartido(),kind:11};

        $scope.broadcast.newQuickMove(function (moveId) {
            if (moveId) {
                $scope.move = {id: moveId};
                //$scope.moveModal.show();
            } else {
                console.log("error");
            }
        },move);

      }
      if(index==1){
        $scope.broadcast.scoreUpVisitor();
        move = {id: $scope.broadcast.getIdPartido(),kind:21};
        $scope.broadcast.newQuickMove(function (moveId) {
            if (moveId) {
                $scope.move = {id: moveId};
                //$scope.moveModal.show();
            } else {
                console.log("error");
            }
        },move);
      }
      if(index==2){
          move = {id: $scope.broadcast.getIdPartido(),kind:15};
        $scope.broadcast.newQuickMove(function (moveId) {
            if (moveId) {
                $scope.move = {id: moveId};
                //$scope.moveModal.show();
            } else {
                console.log("error");
            }
        },move);
      }
      if(index==3){
        $scope.broadcast.scoreDownLocal();
        move = {id: $scope.broadcast.getIdPartido(),kind:110};
        $scope.broadcast.newQuickMove(function (moveId) {
            if (moveId) {
                $scope.move = {id: moveId};
                //$scope.moveModal.show();
            } else {
                console.log("error");
            }
        },move);


      }
      if(index==4){
        $scope.broadcast.scoreDownVisitor();
        move = {id: $scope.broadcast.getIdPartido(),kind:210};
        $scope.broadcast.newQuickMove(function (moveId) {
            if (moveId) {
                $scope.move = {id: moveId};
                //$scope.moveModal.show();
            } else {
                console.log("error");
            }
        },move);
      }
      return true;
    }
  });

};
    });
