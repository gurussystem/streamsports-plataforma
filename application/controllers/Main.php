<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'controllers/Base.php';

class Main extends Base {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it"s displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        $this->data['videos'] = $this->user->getVideos($this->data['user']->id);
		$this->load->view('videos', $this->data);
	}

    public function play($videoId)
	{
        if ($video = $this->user->getVideo($this->data['user']->id, $videoId)){
            $this->data['video'] = $video;
            $this->load->view('video', $this->data);
        } else {
            redirect('/');
        }
	}

    public function air()
	{
        $this->load->view('air', $this->data);
	}

	 public function configuracion()
	{
        $this->load->view('configuracion', $this->data);
	}
	
    public function login()
    {
        if ($this->session->has_userdata('logged')) {
            redirect('/');
        } else {
            if ($user = $this->input->post('user') and $pass = $this->input->post('pass')){
                if ($this->user->login($this->data['user']->user, $user, $pass)){
                    $this->session->logged = true;
                    redirect('/');
                } else {
                    $this->data['error'] = true;
                    $this->load->view('login', $this->data);
                }
            } else {
                $this->load->view('login', $this->data);
            }
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('logged');
        redirect('/');
    }

    public function delete($videoId)
    {
        if ($this->session->has_userdata('logged')) {
            $this->user->deleteVideo($this->data['user']->id, $videoId);
        }
        redirect('/');
    }
}
