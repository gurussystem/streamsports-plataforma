<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Base extends CI_Controller {

    public $data = [];

    public function __construct()
    {
        parent::__construct();

        if ($subdomain = array_shift((explode(".",$_SERVER['HTTP_HOST'])))) {
            // check for valid name
            if ($user = $this->user->get($subdomain)) {
                $this->data['subdomain'] = $subdomain;
                $this->data['user'] = $user;
            } else {
                $this->load->view('no_user');
                die($this->output->get_output());
            }
        }
    }
}
