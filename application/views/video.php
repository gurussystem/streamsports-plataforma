<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $title = $video->local." - ".$video->visitante ?>
<?php include "includes/header.php" ?>
	<div class="ui container">

		<h1><?php echo $video->local ?> - <?php echo $video->visitante ?> (<?php echo $video->localGol ?> - <?php echo $video->visitanteGol ?>)</h1>
				<div class="ui grid">
					<div class="left floated left aligned column">
						<h3>
							<?php echo date('d/m/Y H:i', strtotime($video->fechaIni.' UTC')) ?>
						</h3>
						<a id="volver" href="javascript:window.history.back();" class="ui circular arrow left icon button tiny" title="Volver a la página anterior" style="width: 100px;"> 
						<i class="arrow left icon"> </i>  Volver atrás 
					</a>
					</div>
			
					
			
					<div class="right floated right aligned wide column">						
						<?php if ($this->session->has_userdata('logged')): ?>
						<a id="eliminarP" class="circular ui icon button tiny right floated"  onclick="return confirm('¿Eliminar vídeo?')" href="<?php echo base_url('delete/'.$video->idGrabacion) ?>" >
							<i class="icon trash"></i>
						</a>
						<?php endif ?>			
					</div>
				</div>

		<div class="ui divider"></div>
		<div class="ui embed" data-url="<?php echo $video->urlVideo ?>">
		</div>
		<div class="ui divider"></div>
		<?php include "includes/social_buttons.php" ?>
	
	</div>
	<script>
		$('.ui.embed').embed();
	</script>
<?php include "includes/footer.php" ?>