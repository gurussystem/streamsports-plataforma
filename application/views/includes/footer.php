<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="ui basic center aligned segment">

	<?php echo $nomDominio ?> 
	<?= date('Y') ?> 
	- StreamGPS S.L. - info@streamgps.com

</div>

<script>
	$(function() {
		var prev = {start: 0, stop: 0},
		paging = $("#paging"),
		cont = $(".video-page");

		if (paging.length > 0) {
			paging.paging(cont.length, {

				format: "< nnnnnnn >",
				perpage: 1,
				lapping: 0,
				page: 1,
				onSelect: function (page) {

						var data = this.slice;

						cont.slice(prev[0], prev[1]).css('display', 'none');
						cont.slice(data[0], data[1]).fadeIn("slow");

						prev = data;

						return true; // locate!
				},
				onFormat: function (type) {

					switch (type) {

						case 'block':

							if (!this.active)
								return '<span class="ui button disabled">' + this.value + '</span>';
							else if (this.value != this.page)
								return '<em><a href="#' + this.value + '" class="ui button">' + this.value + '</a></em>';
							return '<span class="ui button primary">' + this.value + '</span>';

						case 'next':

							if (this.active)
								return '<a href="#' + this.value + '" class="ui button"><i class="angle right icon"></i></a>';
							return '<span class="ui button disabled"><i class="angle right icon"></i></span>';

						case 'prev':

							if (this.active)
								return '<a href="#' + this.value + '" class="ui button"><i class="angle left icon"></i></a>';
							return '<span class="ui button disabled"><i class="angle left icon"></i></span>';

						case 'first':

							if (this.active)
								return '<a href="#' + this.value + '" class="first">|<</a>';
							return '<span class="disabled">|<</span>';

						case 'last':

							if (this.active)
								return '<a href="#' + this.value + '" class="last">>|</a>';
							return '<span class="disabled">>|</span>';

						case "leap":

							if (this.active)
								return "...";
							return "";

						case 'fill':

							if (this.active)
								return "...";
							return "";
					}
				}
			});
		}
	});

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59878378-4', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
