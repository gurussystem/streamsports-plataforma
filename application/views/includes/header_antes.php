<?php
defined('BASEPATH') OR exit('No direct script access allowed');

# momentaneo
$serverName = explode(".", $_SERVER['SERVER_NAME']);
$subdominio = $serverName[0];
$dominio = $serverName[1];
// echo "['REDIRECT_URL']: ". $_SERVER['REDIRECT_URL'];

if(isset($_SERVER['REDIRECT_URL'])){
	//echo "<br> ['REDIRECT_URL']: SI ";
}else{
	//echo "<br> ['REDIRECT_URL']: NO ";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />	
	<meta property="fb:app_id" content="966242223397117" />
	<title><?php echo isset($title) ? $title : $dominio ?></title>
	
	<!-- jquery -->
	<script src="<?php echo base_url('assets/jquery-2.1.4.min.js') ?>"></script>
	
	<!-- semantic -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/semantic/semantic.min.css') ?>">
	<script src="<?php echo base_url('assets/semantic/semantic.min.js') ?>"></script>
		
	<!-- paging -->
	<script src="<?php echo base_url('assets/jquery.paging.min.js') ?>"></script>
	
		

	<?php if(isset($_SERVER['REDIRECT_URL']) == '/configuracion'){     // echo 'path: ' . $_SERVER['REDIRECT_URL'];  ?>
	
	<!-- bootstrap -->
		<!-- 
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"  rel="stylesheet">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js" type="text/javascript"/>
			-->
		
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
			<script src="<?php echo base_url('assets/libs_internas/bootstrap/js/bootstrap.min.js') ?>"></script>
	
	
		

		
		<!--  typeahead -->	
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery.typeahead.css') ?>">
		<script src="<?php echo base_url('assets/js/jquery.typeahead.js') ?>"></script>
		
		<!-- datetimepicker -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/libs_internas/datetimepicker/jquery.datetimepicker.css') ?>">
		<script src="<?php echo base_url('assets/libs_internas/datetimepicker/jquery.datetimepicker.full.js') ?>"></script>
		<script src="<?php echo base_url('assets/libs_internas/datetimepicker/jquery.datetimepicker.js') ?>"></script>	
				
				<!-- bootstrap-fileinput -->
		<!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet"> -->
     <!-- bootstrap-fileinput -->
        <link href="<?php echo base_url('assets/libs_internas/bootstrap-fileinput/css/fileinput.css') ?>" media="all" rel="stylesheet" type="text/css" />
		 <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
        <script src="<?php echo base_url('assets/libs_internas/bootstrap-fileinput/js/fileinput.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/libs_internas/bootstrap-fileinput/js/fileinput_locale_fr.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/libs_internas/bootstrap-fileinput/js/fileinput_locale_es.js') ?>" type="text/javascript"></script>
      <!--  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js" type="text/javascript"></script>-->
		
			
		<!-- las mias -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/configuracion.css') ?>">	
		<script src="<?php echo base_url('assets/js/configuracion.js') ?>"></script>
		
	<?php  } // fin if configuracion ?>


	<!-- script stream -->
	<script src="<?php echo base_url('assets/js/streamScript.js') ?>"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/streamStyle.css') ?>">
	
	<style>
	
		#header .menu .triangle {
			width: 0;
			height: 0;
			position: absolute;
			bottom: 0;
			border-style: solid;
			border-width: 0 0 56px 56px;
			border-color: transparent transparent #AD1F25 transparent;
			<?php /*	right: <?php echo ($this->session->has_userdata('logged')) ? "500px" : "400px" ?>; */ ?>
			right: <?php echo ($this->session->has_userdata('logged')) ? "645px" : "400px" ?>;
		}
		#header .menu .buttons {
			background-color: #AD1F25;
			height: 56px;
			position: absolute;
			right: 0;
			bottom: 0;
			padding-top:10px;
			<?php /*	width: <?php echo ($this->session->has_userdata('logged')) ? "500px" : "400px" ?>; */ ?>
			width: <?php echo ($this->session->has_userdata('logged')) ? "645px" : "400px" ?>; 
			padding-left: 30px;
		}
		
	</style>
</head>
<body>

	<?php 
	if($dominio=='streamevents'){  $nomDominio = "StreamEvents"; ?>
	<div id="header" style="background-image:url(<?=base_url('assets/img/streamevents/'.$subdomain.'.jpg')?>)">
	<?php  }else{ 	$nomDominio = "StreamSports"; ?>
	<div id="header" style="background-image:url(<?=base_url('assets/img/streamsports/'.$subdomain.'.jpg')?>)">
	<?php  }  ?>
	
	<div class="logo"><img src="<?php echo base_url('assets/img/header-logo.png') ?>"></div>
	<div class="menu">
		<span class="triangle"></span>
		<span class="buttons">
			<a href="<?php echo base_url('/') ?>" class="ui button <?php echo uri_string() == '' ? 'primary' : '' ?>">Vídeos</a>
			<a href="<?php echo base_url('/air') ?>" class="ui button <?php echo uri_string() == 'air' ? 'primary' : '' ?>">TV Directo</a>
			<?php if ($this->session->has_userdata('logged')): ?>
				<a href="<?php echo base_url('/transmit') ?>" class="ui button" target="_blank">Retransmitir</a>
				<a href="<?php echo base_url('/configuracion') ?>" class="ui button <?php echo uri_string() == 'configuracion' ? 'primary' : '' ?>">Configuración</a>
				<a href="<?php echo base_url('/logout') ?>" class="ui button">Desconectar</a>
			<?php else: ?>
				<a href="<?php echo base_url('/login') ?>" class="ui button <?php echo uri_string() == 'login' ? 'primary' : '' ?>">Iniciar Sesión</a>
			<?php endif ?>
		</span>
	</div>
</div>