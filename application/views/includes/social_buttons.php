<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="ui two column grid">
	<div class="left floated left aligned column">
		<a href="https://www.facebook.com/sharer/sharer.php?u=<?= base_url('play/'.$video->idGrabacion)?>" target="_blank" class="ui circular facebook icon button tiny compartirFB" id="">
			<i class="facebook icon"></i>
		</a>
		<a href="https://twitter.com/home?status=<?= base_url('play/'.$video->idGrabacion)?>" target="_blank" class="ui circular twitter icon button tiny compartirTW" id="">
			<i class="twitter icon"></i>
		</a>
	
		<?php /*<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?= base_url('play/'.$video->idGrabacion)?>&title=Partido&summary=<?= urlencode($video->local.' - '.$video->visitante)?>&source=<?= base_url('play/'.$video->idGrabacion)?>" class="ui circular linkedin icon button">
			<i class="linkedin icon"></i>
		</a>
		<button class="ui circular google plus icon button">
			<i class="google plus icon"></i>
		</button>
		<a href="mailto:?subject=<?= $video->local.' - '.$video->visitante?>&amp;body=<?= base_url('play/'.$video->idGrabacion)?>" class="ui circular icon button">
			<i class="icon envelope"></i>
		</a>*/ ?>
	</div>
	<div class="right floated right aligned column">
		<?php if ($this->session->has_userdata('logged')): ?>
			<?php 	/*
			<a class="circular ui icon button tiny" href="#" title="Descargar partido completo">
				<i class="icon download tiny"></i>
			</a>
			*/	?>
			<a class="circular ui icon button tiny  descargarResumen" href="<?php echo $video->urlVideo ?>"  id="">
				<i class="icon cloud download tiny"></i>
			</a>
			<!--
				Descargar el primer video que encuentre
			-->	
			
			<a class="circular ui icon button tiny" onclick="download_video(<?php echo $video->idGrabacion ?>)"  title="Descargar partido entero">
				<i class="icon film tiny"></i>
			</a>
	
			<a  class="circular ui icon button tiny verEstadisticas" target="_blank" href="<?php echo 'https://'.$subdomain.'.streamsports.es/statistics/results.html?'.$video->idPartido ?>" id="" >
				<i class="line chart icon"></i>
			</a>
			
			
		<?php /* else: ?>
			<a href="<?php echo base_url('login') ?>" class="circular ui icon button">
				<i class="icon cloud download"></i>
			</a>
		endif*/ ?>
		<?php endif ?>
	</div>
</div>
