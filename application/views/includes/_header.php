<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />	
	<meta property="fb:app_id" content="966242223397117" />
	<title><?php echo isset($title) ? $title : 'StreamSports' ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/semantic/semantic.min.css') ?>">
	<script src="<?php echo base_url('assets/jquery-2.1.4.min.js') ?>"></script>

	
	<script src="<?php echo base_url('assets/semantic/semantic.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/jquery.paging.min.js') ?>"></script>
	
	<?php
	/*
		if($title == 'Configuracion'){ 
			echo "hola";
		}
		*/
	?>
	
	<?php /*if($title == 'Configuracion'){ ?>
	<!-- <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />	-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bloquearPoblacion.css') ?>">
	<script src="<?php echo base_url('assets/js/jquery.typeahead.js') ?>"></script>
	<script src="<?php echo base_url('assets/libs_internas/bootstrap/js/bootstrap.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/libs_internas/moment/moment-with-locales.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/configuracion.js') ?>"></script>
	<?php } */?>

<!--	<script src="http://code.jquery.com/jquery-latest.js"></script>-->

	

	<style>
		h1 {
			margin-top: 3em !important;
		}
		.video-thumbnail{
			position: relative;
		}
		.video-thumbnail img:hover{
			opacity: 0.7;
		}
		.video-thumbnail i{
			position: absolute;
			top: 50%;
			left:0;
			width: 100%;
			color:white;
			font-size:40px;
		}
		#paging{
			margin-top:20px;
		}
		#header {
			height:300px;
			position:relative;
			background-size:cover;
			background-repeat: no-repeat;
		}
		#header .logo{
			position:absolute;
			top:0;
			left:0;
		}

		#header .menu{
			position: absolute;
			bottom: 0;
			right: 0;
			height: 56px;
			display: inline;
		}
		#header .menu .triangle {
			width: 0;
			height: 0;
			position: absolute;
			bottom: 0;
			border-style: solid;
			border-width: 0 0 56px 56px;
			border-color: transparent transparent #AD1F25 transparent;
				right: <?php echo ($this->session->has_userdata('logged')) ? "500px" : "400px" ?>;
		<?php /*right: <?php echo ($this->session->has_userdata('logged')) ? "645px" : "400px" ?>; */ ?>
		}
		#header .menu .buttons {
			background-color: #AD1F25;
			height: 56px;
			position: absolute;
			right: 0;
			bottom: 0;
			padding-top:10px;
			width: <?php echo ($this->session->has_userdata('logged')) ? "500px" : "400px" ?>; 
		<?php /*	width: <?php echo ($this->session->has_userdata('logged')) ? "645px" : "400px" ?>; */ ?>
			padding-left: 30px;
		}
		#header .menu .buttons a {
			color:white;
		}
		#header .menu .buttons a.ui.button {
			background-color: transparent;
		}
		#header .menu .buttons a.ui.button:hover {
			background-color: #9b9b9b;
		}
		.ui.button.primary {
			background-color: black !important;

		}
		.ui.card{
			margin: 0 auto;
		}

		#paging{
			text-align: center;
		}
		.ui.card .content img, .ui.cards>.card .content img{
			width: 100%;
		}
		@media (max-width: 640px) {
			#header .menu .triangle {
				display: none;
			}
			#header .menu {
				width: 100%;
				background-color: #AD1F25;
				padding: 5px;
				height:auto;
			}
			#header .menu .buttons {
				position: relative;
				width:auto;
			}
		}
	</style>
</head>
<body>
<div id="header" style="background-image:url(<?=base_url('assets/img/'.$subdomain.'.jpg')?>)">
	<div class="logo"><img src="<?php echo base_url('assets/img/header-logo.png') ?>"></div>
	<div class="menu">
		<span class="triangle"></span>
		<span class="buttons">
			<a href="<?php echo base_url('/') ?>" class="ui button <?php echo uri_string() == '' ? 'primary' : '' ?>">Vídeos</a>
			<a href="<?php echo base_url('/air') ?>" class="ui button <?php echo uri_string() == 'air' ? 'primary' : '' ?>">TV Directo</a>
			<?php if ($this->session->has_userdata('logged')): ?>
				<a href="<?php echo base_url('/transmit') ?>" class="ui button" target="_blank">Retransmitir</a>
				<?php /*<a href="<?php echo base_url('/configuracion') ?>" class="ui button <?php echo uri_string() == 'configuracion' ? 'primary' : '' ?>">Configuración</a>
				*/ ?>
				<a href="<?php echo base_url('/logout') ?>" class="ui button">Desconectar</a>
			<?php else: ?>
				<a href="<?php echo base_url('/login') ?>" class="ui button <?php echo uri_string() == 'login' ? 'primary' : '' ?>">Iniciar Sesión</a>
			<?php endif ?>
		</span>
	</div>
</div>