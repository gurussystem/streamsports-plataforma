	
			
	

			
	<style>
		#sinDatosSMS {
			text-align: center;
			margin-top: 60px;
			margin-bottom: 50px;
			font-family: "Titillium Web",sans-serif;
			font-size: 24px;
			color: #ad1f25;
			font-weight: bold;
			line-height: 32px;
		}

		#totalEspectadores {
			text-align: center;
			margin-top: 20px;
			margin-bottom: 25px;
			font-family: "Titillium Web",sans-serif;
			font-size: 24px;
			color: #ad1f25;
			font-weight: bold;
			line-height: 32px;
		}

		#img_esperar{
			  margin-top: 60px;
			margin-bottom: 50px;
			
		}
	</style>

	
	<div id="cont_bloquear_text"> 
		<div id="bloquear_text" class="bloquear_text"> 
			<h3 class="introTitulo" id="introTitulo">  Estadísticas de emisión por rango de fechas </h3>
			<p class="introtxt" id="introtxt01"> Escoge entre una fecha de inicio y otra de fin para hacer recoger unas estadisticas globales.</p>
		</div>
	</div>
		
		
	<div id="rango_fechas" style="width:100%;">
	
		<!-- fecha inicio -->
		<div class="form-group" style="float: left; width:49%;">
			<label  id="fechaInicio_lbl">Introduce una fecha de inicio: </label>
			<input type="text" id="fechaInicio" class="form-control"  onfocus="comprobarSms()"/>
		</div>

		<!-- fecha fin -->
		<div class="form-group" style="float: right;  width:49%;" >
			<label  id="fechaFin_lbl">Introduce una fecha fin: </label>
			<input type="text" id="fechaFin" class="form-control"  onfocus="comprobarSms()"/>
		</div>
		
		<button type="button" class="btn btn-default btnStream"  id="mostrarDatos" title="Mostrar datos" onclick="mostrarDatos()"> Mostrar datos </button> 
	</div> <!-- fecha_equipos-->
		
	
					 
	<div id="cont_esperando_estadisticas" style="display:none;">
		<div id="img_esperar"> 
			<center> <img src="../../assets/img/preloader.GIF" > </center>
		</div>
	</div>
		 
	 <div id="sinDatosSMS"> </div>
	 
	<div id="cont_estadisticas" style="display:none;">
		<div id="totalEspectadores"> </div>
		<div id="cont_grafica" class="chart"> 
			<div id="chart_div"></div>
		</div>	
	</div>

	
	<script>
	
		jQuery.datetimepicker.setLocale('es');
		jQuery('#fechaInicio').datetimepicker({
			  timepicker:false,
			// format:'d-m-Y H:i:00', // Si se cambia el formato cuidado con el insert
			format:'Y-m-d', // Si se cambia el formato cuidado con el insert
			//lang:'es',
		});	
		
		jQuery('#fechaFin').datetimepicker({
			  timepicker:false,
			format:'Y-m-d', 
		});

		function mostrarDatos(){
			$("#cont_esperando_estadisticas").css("display", "block");
			$("#sinDatosSMS").css("display", "none");	
			$("#cont_estadisticas").css("display", "none");		
						
			var fechaInicio = $("#fechaInicio").val() ;
			var fechaFin = $("#fechaFin").val() ;
			
			// console.log("desde : " + fechaInicio + " hasta " + fechaFin );
			
			// desde : 01-09-2016 15:57:00 hasta 05-10-2016 15:58:00
		//	enviarDatos(fechaI, fechaF);
			enviarDatos(fechaInicio, fechaFin);
		}
		
			$(document).ready(function(){ });
			

		var  datoUsuario = new Array();	
		var users = new Array();
		var arrayFechaEspect2  = new Array();
		var contadorTotal = 0;
		function enviarDatos(fechaI, fechaF){
			// console.log( "enviarDatos(" + fechaI + " , " +  fechaF + ")" );
			
			$.ajax({
				async:true, 
				cache:false,
				type: 'GET',
				data: { 
					'fechaI' : fechaI, 	
					'fechaF' : fechaF	
				},
				url: '../assets/io/estadisticasRangoConsulta.php', 
				success: function(response){
					contadorTotal = 0;
					arrayFechaEspect2 = new Array();
					datoUsuario =  $.parseJSON(response);				
					 console.log("El resultado es el siguiente: "); console.log(datoUsuario);
					$("#cont_esperando_estadisticas").css("display", "none");
						
					if(datoUsuario.proceso != 0){
						
						//espectador min y maximo
						var minEsp = datoUsuario.espectadores[0];
						var maxEsp =datoUsuario.espectadores[0];
						for(var e=0; e<datoUsuario.espectadores.length; e++){			
							
							
						}// fin for

					
						
						$("#sinDatosSMS").css("display", "none");	
						$("#cont_estadisticas").css("display", "block");	
						
						
						// if(datoUsuario.proceso != 0){

							// recorremos todos los partidos /eventos	
							for(var j=0; j<datoUsuario.datos.length; j++){	

							// maximo
							if(datoUsuario.datos[j].numEspectadores  > maxEsp)
								maxEsp = datoUsuario.datos[j].numEspectadores;
								
							// minimo
							if(datoUsuario.datos[j].numEspectadores < minEsp)
								minEsp=datoUsuario.datos[j].numEspectadores;
							
								if(datoUsuario.datos[j].numEspectadores != 0 ){
									var f2 = new Array();
									f2.push(new Date(datoUsuario.datos[j].fechaIni ));
												
									for(var h=0; h<datoUsuario.datos.length; h++){
										if(h==j){
											f2.push(datoUsuario.datos[j].numEspectadores);
											contadorTotal +=  datoUsuario.datos[j].numEspectadores;
										}else{
											f2.push(0);
										}
									
									}// for h	
									arrayFechaEspect2.push(f2);
								}					
							} // for j
							
								console.log("minEsp: " + minEsp);
						console.log("maxEsp: " + maxEsp);
							
							$("#totalEspectadores").html("Total espectadores:  " + contadorTotal );	

							google.charts.load('current', {packages: ['corechart', 'bar'] , 'language': 'es' } );							
							 // console.log("nuevo array ");	console.log( arrayFechaEspect2);							
							 google.charts.setOnLoadCallback(drawStacked);

							function drawStacked() {
								//console.log("El resultado es el siguiente aaaa: "); console.log(datoUsuario);
								var data = new google.visualization.DataTable();
								data.addColumn('date', 'Time of Day');
			  
								for(var i=0; i<datoUsuario.datos.length; i++){
									var etiqueta = "partido " + datoUsuario.datos[i].titulo;
									data.addColumn('number', etiqueta);
								}
								data.addRows(arrayFechaEspect2);
							
								var fechaMax =  datoUsuario.fechas.length - 1;
								console.log(fechaMax);
								
								var widthGrafica = $("#cont_estadisticas").width() ;
								$("#chart_div").css('width', widthGrafica);
								console.log(widthGrafica);
								//	console.log(datoUsuario.fecha[0]['fechaEntera']);
								
								var options = {
									bar: { groupWidth: '20%' }, // grosor de la barra
									title: "Espectadores por día", // titulo de la grafica
									legend: 'none', // leyenda de la grafica
									isStacked: true, // Barra unificada en la misma fecha, sino saldrian varias barras
								//	colors: ['#795548'], // color de las barras o linea
									// tooltip: { isHtml: true },    // CSS styling affects only HTML tooltips.
									// backgroundColor: '#f1f8e9', // color del fondo de la grafica
									height: 600,
									
									hAxis: {
										title: 'Fecha ', 
										format:'YYYY-MM-DD', 
										//format: 'long',
										//minValue: datoUsuario.fechas[0].fechaEntera,
										///maxValue: datoUsuario.fechas[fechaMax].fechaEntera
										
										gridlines: {
											count: -1,
										/*	units: {
												days: {format: ['MMM dd']},
												hours: {format: ['HH:mm', 'ha']},
											}	
										},*/
										/*minorGridlines: {
											units: {
												hours: {format: ['hh:mm:ss a', 'ha']},
												minutes: {format: ['HH:mm a Z', ':mm']}
											}
										},*/
							
										viewWindow: {
											min: datoUsuario.fechas[0].fechaEntera,
											max:  datoUsuario.fechas[fechaMax].fechaEntera,
										},			
										
									},
										
										
									}, //  fin hAxis
						
									vAxis: {
										title: 'Espectadores', 
										gridlines: {
											count: -1,
											/*units: {
												days: {format: ['MMM dd']},
												hours: {format: ['HH:mm', 'ha']},
											}	*/
										},
										minorGridlines: {
											/*units: {
												hours: {format: ['hh:mm:ss a', 'ha']},
												minutes: {format: ['HH:mm a Z', ':mm']}
											}*/
										},
							
										viewWindow: {
											min: 0,
											max: maxEsp,
										},						
									}, //  fin vAxis
									
									
									
									//  series: { datoUsuario.fechas },
									
								//	pointSize: 20,
								//	pointShape: 'square',								
									
									//width: widthGrafica,
								
									
									//	legend: { position: "none" },
									// legend: { position: 'top', maxLines: 3 },
									
								
										//series: [{'color': '#D9544C'}],
									//intervals: { style: 'boxes' },
										
								};				
								

								var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
								chart.draw(data, options);
								
								
								
		 
							}	
							$("#cont_esperando_estadisticas").css("display", "none");
							$("#cont_estadisticas").css("display", "block");
						
						
						
						}	
					// }
					else{
						$("#cont_estadisticas").css("display", "none");						
					
						$("#sinDatosSMS").css("display", "block");					
						$("#sinDatosSMS").html("Esta consulta no tiene datos para poder hacer estadísticas. ");
					}
					
				}, // fin success					
			});	
		} // fin funcion


		function utf8_decode(s) {
		  return decodeURIComponent(escape(s));
		}
				
			
	
	
	

		
	
	</script>