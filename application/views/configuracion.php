<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php include "includes/header.php" ?>


<?php if ($this->session->has_userdata('logged')): ?>
	<div class="container">
		<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
			<li role="presentation"  id="tab_bloqueoIp" class="active">
				<a href="#bloqueoIp" aria-controls="home" role="tab" data-toggle="tab" >  </a>
			</li>
		
			<li role="presentation" id="tab_proximoPartido" >
				<a href="#proximoPartido" aria-controls="profile" role="tab" data-toggle="tab" > </a>
			</li>

			 <li role="presentation" id="tab_configurarDatos" >
				<a href="#configurarDatos" aria-controls="profile" role="tab" data-toggle="tab" > </a>
			</li>
			
			<li role="presentation" id="tab_estadisticasRango" >
				<a href="#estadisticasRango" aria-controls="profile" role="tab" data-toggle="tab" > </a>
			</li>
		
			
		</ul>

		<!-- Tab panes -->
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="bloqueoIp">
				<?php require "bloqueacionPoblacion.php" ?>
			</div>
		
			<div role="tabpanel" class="tab-pane " id="proximoPartido">
				<?php require "proximoPartido.php" ?>
			</div>
				
			<div role="tabpanel" class="tab-pane " id="configurarDatos">
				<?php require "configurarDatos.php" ?>
			</div>	
			
			<div role="tabpanel" class="tab-pane " id="estadisticasRango">
				<?php require "estadisticasRango.php" ?>
			</div>
			
		
			
		</div>
	</div>
	
	<script>
	
	jQuery(document).ready(function() {
		console.log("ready inicio");
		loader_textos();
		console.log("ready fin ");
	});	
	
	
	</script>
	
	<?php else: ?>
		<?php header('Location: '. base_url(''));  ?>
	<?php endif ?>


	<?php include "includes/footer.php" ?>
	