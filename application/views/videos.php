<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php include "includes/header.php" ?>
	<div class="ui container">
		<h1>Vídeos <?php echo $subdomain ?></h1>
		
		
		<?php $i = 0 ?>
		<?php $itemsPages = 3 ?>

		<?php foreach ($videos as $video): ?>
			<?php if ($itemsPages == 3): ?>
				<div class="video-page">
				<?php $itemsPages = 0 ?>
			<?php endif ?>
			<?php if ($i == 0): ?>
				<div class="ui stackable three column grid">
			<?php endif ?>
			<div class="column">
				<div class="ui card">
					<div class="content">
						<div class="header" style="float: left; width: 100%;">
							<?php echo $video->local ?> - <?php echo $video->visitante ?>
							 <?php if ($this->session->has_userdata('logged')): ?>
								<div class="right floated">
									<a  id="eliminarP" class="circular ui icon button tiny eliminarP" onclick="return confirm('¿Eliminar vídeo?')" href="<?php echo base_url('delete/'.$video->idGrabacion) ?>"  >
										<i class="icon trash"></i>
									</a>
								</div>
							<?php endif ?>
						</div>
						<div class="meta">
							<span class="right floated time"><?php echo date('d/m/Y H:i', strtotime($video->fechaIni.' UTC')) ?></span>
							<span class="category"><?php echo $video->localGol ?> - <?php echo $video->visitanteGol ?></span>
						</div>
						<a id="bestJugadas" class="description video-thumbnail" href="<?php echo base_url('play/'.$video->idGrabacion) ?>" >
							<div style="display: inline-block;vertical-align: middle;width:100%;height:135px;background-image:url(<?php echo $video->urlSnapShot ?>), url('assets/img/playDefault2.png'); background-size:cover;background-position:center;border: 1px solid rgba(0, 0, 0, 0.4); "></div>
						
							<i class="icon play"></i>
							
						</a>
					</div>
					<div class="extra content">
						<?php include "includes/social_buttons.php" ?>
					</div>
				</div>
			</div>
			<?php if ($i == 2): ?>
				</div>
				<?php $i = 0 ?>
				<?php $itemsPages++ ?>
			<?php else: ?>
				<?php $i++ ?>
			<?php endif ?>
			<?php if ($itemsPages == 3): ?>
				</div>
			<?php endif ?>
		<?php endforeach ?>
		<?php if ($i != 0): ?>
			</div>
		<?php endif ?>
		<?php if ($itemsPages != 3): ?>
			</div>
		<?php endif ?>
		<div id="paging"></div>
	</div>
<?php include "includes/footer.php" ?>