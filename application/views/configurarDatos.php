	
	<div id="configDatos_text"  class="configDatos_text"> 
		<h3 class="introTitulo" id="introTitulo"> Configuración de Datos </h3>
		<p class="introtxt" id="txt01_proximo"> Desde este apartado podrá personalizar su plataforma configurando las siguientes opciones.</p>
	</div>	
	
	<br>
	<br>
	<div id="imgPortadaActual"> </div>	
	
	<!-- Imagen de portada -->		
	<div class="container kv-main">
		<div class="page-header">	
			<h1> Imagén de portada </h1>
			<p class="introtxt" id="txt01_proximo">Para que la imagén se vea correctamente en todos los dispositivos no debe de superar las medidas 652x492 </p>
            <form enctype="multipart/form-data">
				<hr style="border: 2px dotted">
                <!--
				<input id="subirImgPortada" name="subirImgPortada" type="file" multiple class="file-loading">
				<input id="input-1" type="file" class="file ">
				-->
				<input id="subirImgPortada" name="subirImgPortada" type="file" class="file-loading">
				
				<!-- <div id="errorBlock" class="help-block" ></div> -->
			 

            </form>
            <hr>
			
			<div class="form-group" style="display:none;">
				<button class="btn btn-warning" type="button">Disable Test</button>
				<button class="btn btn-info" type="reset">Refresh Test</button>
				<button class="btn btn-primary">Submit</button>
				<button class="btn btn-default" type="reset">Reset</button>
			</div>
				
			<hr>
            <br>
        </div>
	</div>

		
	<script>
	
	$(document).ready(function() {
		
		console.log("configurarDatos.PHP ");
		
		// Mostramos la imagen que hay en la portada
		recogerImagenPortada();
		
		$("#subirImgPortada").fileinput({

		uploadUrl: "/assets/io/configDatos.php", // server upload action
			autoReplace: true,
			//uploadAsync: true,
			//maxFileCount: 1,
			//showBrowse: false,
			//browseOnZoneClick: true,
			language: 'es',
			allowedFileExtensions : ['jpg', 'png','gif'],		
			//maxFileSize: 1000,
			maxFilesNum: 1


		});
		
	// CATCH RESPONSE

$('#subirImgPortada').on('filebatchuploadsuccess', function(event, data, previewId, index) {
    var form = data.form, files = data.files, extra = data.extra,
        response = data.response, reader = data.reader;
    console.log('File batch upload success 1');
});


// pre
    $('#subirImgPortada').on('filebatchpreupload', function(event, data, previewId, index) {
        var form = data.form, files = data.files, extra = data.extra,
            response = data.response, reader = data.reader;
        console.log('File batch pre upload');
    });
	
	
	    $('#subirImgPortada').on('filebatchuploadcomplete', function(event, files, extra) {
			console.log('File batch upload complete');
			recogerImagenPortada();

			// Asi conseguimos que no salga el error
			$(".file-upload-indicator i").removeClass( "glyphicon glyphicon-exclamation-sign text-danger" );
			$(".file-upload-indicator i").addClass( "glyphicon glyphicon-ok-sign text-success" );
			//$(".has-error .form-control").css("border", "1px solid #ccc !important");
			$(".has-error").css("border-color", "#ccc");
			$(".form-control").css("border-color", "#ccc");
			
    });
	
	
	
	    $('#subirImgPortada').on('filesuccessremove', function(event, id) {
        if (some_processing_function(id)) {
           console.log('Uploaded thumbnail successfully removed');
        } else {
            return false; // abort the thumbnail removal
        }
		
    });
	
    }); // fin jquery 
	
	

	
	
	</script>