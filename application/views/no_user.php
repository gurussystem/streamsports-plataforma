<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Error</title>
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/semantic/dist/semantic.min.css') ?>">
	<script src="<?= base_url('assets/jquery-2.1.4.min.js') ?>"></script>
	<script src="<?= base_url('assets/semantic/dist/semantic.min.js') ?>"></script>
	<style>
		h1 {
			margin-top: 3em !important;
		}
	</style>
</head>
<body>
<div class="ui container">

	<h1>Error</h1>

	<p>No hay ningún usuario con este nombre</p>
</div>
</body>
</html>