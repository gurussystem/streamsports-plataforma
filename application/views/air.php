<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $title = "Directo" ?>
<?php include "includes/header.php" ?>
	<div class="ui container">
		<div class="ui embed" data-url="https://<?php echo $subdomain ?>.streamsports.es/directo">
		</div>

		<?php if ($this->session->has_userdata('logged')): ?>
			<?php $idUsuario = $this->data['user']->id; ?>
			<script>  //var idUsuario2 = "<?php echo $idUsuario; ?>"; </script>
		<?php endif ?>
	</div>
	<script>
		$('.ui.embed').embed();
	</script>

<?php include "includes/footer.php" ?>
