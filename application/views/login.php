<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $title = "Acceso" ?>
<?php include "includes/header.php" ?>
    <div class="ui container">
<div class="ui two column centered stackable grid">
  <div class="column">
<h1>Acceso</h1>
		<?php if (isset($error)):?>
            <div class="ui inverted red segment">Usuario o contraseña incorrectos</div>
        <?php endif?>
        <form class="ui form" method="post">
            <div class="field">
                <label>Usuario</label>
                <input type="text" name="user" placeholder="Usuario" required autofocus>
            </div>
            <div class="field">
                <label>Contraseña</label>
                <input type="password" name="pass" placeholder="Contraseña" required >
            </div>
            <button class="ui button" type="submit">Entrar</button>
        </form>
</div>

</div>
        
    </div>
<?php include "includes/footer.php" ?>