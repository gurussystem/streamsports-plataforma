	
	<div id="proximo_text" class="proximo_text"> 
		<h3 class="introTitulo" id="introTitulo_proximo"> </h3>
		<p class="introtxt" id="txt01_proximo"> </p>
		<p class="introtxt" id="txt02_proximo"> </p>
	</div>
			
	<div id="contenedor_1">
			
		<div id="con_equipos" > 
			<div class="form-group">
				<label for="titulo" id="tituloPartidoLBL"></label>
				<input 
					type="text" 
					class="form-control" 
					id="tituloPartido" 
					onfocus="comprobarSms()"
				/>
			</div>
			
			<div class="form-group" id="cont_descripcionEvento">
				<label for="local"  id="descripcionEventoLBL"></label>
				<textarea
					class="form-control" 
					id="descripcionEvento" 
					onfocus="comprobarSms()"
				></textarea>
				
			</div>
			
			<div class="form-group" id="cont_equipo1">
				<label for="local"  id="equipo1LBL"></label>
				<input 
					type="text" 
					class="form-control" 
					id="equipo1" 
					onfocus="comprobarSms()"
				/>
			</div>

			<div class="form-group"  id="cont_equipo2">
				<label for="visit" id="equipo2LBL"></label>
				<input 
					type="text" 
					class="form-control" 
					id="equipo2" 
					onfocus="comprobarSms()"
					/>
			</div>
			
			<input id="idPartido" type="hidden" />
		</div> <!-- con_equipos -->
		
		<div id="fecha_equipos"> 
			<div class="form-group" style="float: right;">
				<label for="fh"  id="fechaLBL"></label>
			
				<div class="btn-group" role="group">
					<button type="button" class="btn btn-default btnStream"  id="open" title="Abrir calendario"> Abrir </button> 
					<button type="button" class="btn btn-default btnStream"  id="close"  title="Cerrar calendario"> Cerrar </button>
					<button type="button" class="btn btn-default btnStream"  id="reset" title="Reiniciar calendario">Reset</button>
				</div>
				<input type="text" id="datetimepicker4" class="form-control"  onfocus="comprobarSms()"/>
			</div>

			<script>
				// console.log("proximo partido .PHP ");
				jQuery.datetimepicker.setLocale('es');
				jQuery('#datetimepicker4').datetimepicker({
					format:'d-m-Y H:i:00', // Si se cambia el formato cuidado con el insert
					lang:'es',
				});

				$('#open').click(function(){
					$('#datetimepicker4').datetimepicker('show');
				});

				$('#close').click(function(){
					$('#datetimepicker4').datetimepicker('hide');
				});

				$('#reset').click(function(){
					$('#datetimepicker4').datetimepicker('reset');
				});
				
			</script>
		</div> <!-- fecha_equipos-->
		
		<div id="btnAnyadirPartido">
			<button type="button" class="btn btn-default"  id="addPartido" onclick="addPartido()"> </button>	
			<button type="button" class="btn btn-default"  id="editPartido" onclick="updatePartido()"  style="display:none;"></button>
		</div>
	</div>  <!-- fin  contenedor_1 -->
		
	<hr class="hr_clase "/>  
			
	<div id="lista_proximos_partidos" style="clear: both; color:#888;">	
		<div id="mensajes" ></div>
		<table id="listaPartidos" style="width: 100%;" ></table>
	</div>	