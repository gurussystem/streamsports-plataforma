		
		<div id="cont_bloquear_text"> 
			<div id="bloquear_text" class="bloquear_text"> 
				<h3 class="introTitulo" id="introTitulo"> Bloquear poblaciones / provincias</h3>
				<p class="introtxt" id="introtxt01"> Escriba el nombre de la población/provincia y abajo aparecerán las coincidencias.	</p>
				<p class="introtxt" id="introtxt02"> Seleccione la población/provincia desee bloquear y se le añadirá automaticamente en la lista de "poblaciones bloquedas". </p>
				<p class="introtxt" id="introtxt03"> A la derecha aparecera lista de poblaciones bloqueadas. </p>
				<p class="introtxt" id="introtxt04"> Puede borrar la lista pulsando "Borrar todo". </p>
			</div>
		</div>
			
			
		<div id="cont_bloquear_pob" > 
		<h1> BLOQUEAR POBLACIÓN </h1>
			<div id="mensajes_pob"> </div>
			<div id="bloquear_select_pob">
			    <form >
					<div class="typeahead-container">
						<div class="typeahead-field">
							<span class="typeahead-query">
								<input id="q"
									name="q"
									type="search"
									placeholder="Introduzca población a bloquear"
									autocomplete="off"
									title="Introduzca el nombre de la población donde quiere restringir la emisión."
								>
								 <!-- onkeypress="return pulsar(event)"  -->
							</span>
							
							<span class="typeahead-button" style="display:none;">
								<button type="submit">
									<span class="typeahead-search-icon"></span>
								</button>
							</span>
						</div>
					</div>
				</form>	
			</div> 
			
			<div id="bloquear_flecha_pob"> 
				<center> <img src="../assets/img/rigth_arrow.png" > <img src="../assets/img/rigth_arrow.png" >  </center>
				
			</div>
			
			<div id="bloquear_list_pob"> 
				<div id="listaBloqueada_pob"> 
					<table id="tabla_pob" >
						<tr> <th colspan="2" id="tituloPB_pob" class="tituloPB"> Poblaciones Bloqueadas </th> 	</tr> 
					</table>						
				</div>	
			</div> 	
	
	
			<div id="bloquear_btn_pob"> 
				<button type="button" id="btnEliminar_pob" onclick="eliminar_todo('pob');"  title="Desbloquear todas las poblaciones">Desbloquear </button> 
			</div>
		
		</div>
		
		<!--
		<br>
		<br>
		<hr style="clear: both;"> 
		<br>
		<br>
		-->
		
		<!------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
		
		
		
			<div id="cont_bloquear_pro"> 
			<h1> BLOQUEAR PROVINCIA </h1>	
			<div id="mensajes_pro"> </div>
			<div id="bloquear_select_pro">
			    <form >
					<div class="typeahead-container">
						<div class="typeahead-field">
							<span class="typeahead-query">
								<input id="q2"
									name="q2"
									type="search"
									placeholder="Introduzca provincia a bloquear"
									autocomplete="off"
									title="Introduzca el nombre de la provincia donde quiere restringir la emisión."
								>
								 <!-- onkeypress="return pulsar(event)"  -->
							</span>
							
							<span class="typeahead-button" style="display:none;">
								<button type="submit">
									<span class="typeahead-search-icon"></span>
								</button>
							</span>
						</div>
					</div>
				</form>	
			</div> 
			
			<div id="bloquear_flecha_pro"> 
				<center> <img src="../assets/img/rigth_arrow.png" > <img src="../assets/img/rigth_arrow.png" >  </center>
				<!-- <center> <img src="img/rigth_arrow2.png" />  </center> -->
				
			</div>
			
			<div id="bloquear_list_pro"> 
				<div id="listaBloqueada_pro"> 
					<table id="tabla_pro" >
						<tr> <th colspan="2" id="tituloPB_pro" class="tituloPB"> Provincias Bloqueadas </th> 	</tr> 
					</table>						
				</div>	
			</div> 	
	
	
			<div id="bloquear_btn_pro"> 
				<button type="button" id="btnEliminar_pro" onclick="eliminar_todo('pro');" title="Desbloquear todas las provincias">Desbloquear </button> 
			</div>
		</div>
		
		
		<div id="bloquear_footer"></div>

	<script>
	
	jQuery(document).ready(function() {
	//	console.log("ready inicio bloquear poblaciones");
		infoCliente('bloqueoIp');
		
	});	
	
	
	</script>