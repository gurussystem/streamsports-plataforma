	<script type="text/javascript" charset="utf-8" src="../assets/libs_internas/chartingLibrary/jquery.min.js" ></script>
	
	<!-- Include Date Range Picker -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.0/moment.min.js"></script>
	<script  src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js" type="text/javascript"></script>
	<link href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" rel="stylesheet" type="text/css" />		
			
	<!-- Libreria -->	
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
			
	<style>
		#sinDatosSMS {
			text-align: center;
			margin-top: 60px;
			margin-bottom: 50px;
			font-family: "Titillium Web",sans-serif;
			font-size: 24px;
			color: #ad1f25;
			font-weight: bold;
			line-height: 32px;
		}

		#totalEspectadores {
			text-align: center;
			margin-top: 20px;
			margin-bottom: 25px;
			font-family: "Titillium Web",sans-serif;
			font-size: 24px;
			color: #ad1f25;
			font-weight: bold;
			line-height: 32px;
		}

		#img_esperar{
			  margin-top: 60px;
			margin-bottom: 50px;
			
		}
	</style>

	
	<div id="cont_bloquear_text"> 
		<div id="bloquear_text" class="bloquear_text"> 
			<h3 class="introTitulo" id="introTitulo">  Estadísticas de emisión por rango de fechas </h3>
			<p class="introtxt" id="introtxt01"> Escoge entre una fecha de inicio y otra de fin para hacer recoger unas estadisticas globales.</p>
		</div>
	</div>
		
		
	<div class="form-group">
		<label for="titulo" id="fechasEstadisticasLBL">Selecciona las fechas: </label>
		<input type="text" class="form-control"  id="fechasEstadisticas"  name="daterange" />
	</div>
					 
	<div id="cont_esperando_estadisticas" style="display:none;">
		<div id="img_esperar"> 
			<center> <img src="../../assets/img/preloader.GIF" > </center>
		</div>
	</div>
		 
	 <div id="sinDatosSMS"> </div>
	 
	<div id="cont_estadisticas" style="display:none;">
		<div id="totalEspectadores"> </div>
		<div id="cont_grafica" class="chart"> 
			<div id="chart_div"></div>
		</div>	
	</div>

	
	<script>
	
		var minPartido = new Array();
			
		$('input[name="daterange"]').daterangepicker({
			//timePicker: true,
			//timePickerIncrement: 30,
			locale: {
				format: 'YYYY-MM-DD',
				cancelLabel: 'Cerrar' ,
				applyLabel: 'Aplicar' 
			},
			startDate: '2016-01-01',
			endDate: '2016-12-31'
		}, 
		function(start, end, label) {		
			var fechaInicio =  start.format('YYYY-MM-DD') ;
			var fechaFin = end.format('YYYY-MM-DD'); 
			// alert("A new date range was chosen: " + fechaInicio+ ' to ' + fechaFin );
						
			if(fechaInicio && fechaFin){
				$("#cont_esperando_estadisticas").css("display", "block");
				$("#sinDatosSMS").css("display", "none");	
				$("#cont_estadisticas").css("display", "none");	
				
				enviarDatos(fechaInicio,  fechaFin);			
				
			}else{
				alert("Falta alguna fecha");
			}
		});
		
		$(document).ready(function(){ });
			

		var  datoUsuario = new Array();	
		var users = new Array();
		var arrayFechaEspect2  = new Array();
		var contadorTotal = 0;
		function enviarDatos(fechaI, fechaF){
			// console.log( "enviarDatos(" + fechaI + " , " +  fechaF + ")" );
			
			$.ajax({
				async:true, 
				cache:false,
				type: 'GET',
				data: { 
					'fechaI' : fechaI, 	
					'fechaF' : fechaF	
				},
				url: '../assets/io/estadisticasRangoConsulta.php', 
				success: function(response){
					contadorTotal = 0;
					arrayFechaEspect2 = new Array();
					datoUsuario =  $.parseJSON(response);				
					 console.log("El resultado es el siguiente: "); console.log(datoUsuario);
					$("#cont_esperando_estadisticas").css("display", "none");
							
					if(datoUsuario.proceso != 0){
						
						//espectador min y maximo
						var minEsp = datoUsuario.espectadores[0];
						var maxEsp =datoUsuario.espectadores[0];
						for(var e=0; e<datoUsuario.espectadores.length; e++){			
							
							
						}// fin for

					
						
						$("#sinDatosSMS").css("display", "none");	
						$("#cont_estadisticas").css("display", "block");	
						
						
						// if(datoUsuario.proceso != 0){

							// recorremos todos los partidos /eventos	
							for(var j=0; j<datoUsuario.datos.length; j++){	

							// maximo
							if(datoUsuario.datos[j].numEspectadores  > maxEsp)
								maxEsp = datoUsuario.datos[j].numEspectadores;
								
							// minimo
							if(datoUsuario.datos[j].numEspectadores < minEsp)
								minEsp=datoUsuario.datos[j].numEspectadores;
							
								if(datoUsuario.datos[j].numEspectadores != 0 ){
									var f2 = new Array();
									f2.push(new Date(datoUsuario.datos[j].fechaIni ));
												
									for(var h=0; h<datoUsuario.datos.length; h++){
										if(h==j){
											f2.push(datoUsuario.datos[j].numEspectadores);
											contadorTotal +=  datoUsuario.datos[j].numEspectadores;
										}else{
											f2.push(0);
										}
									
									}// for h	
									arrayFechaEspect2.push(f2);
								}					
							} // for j
							
								console.log("minEsp: " + minEsp);
						console.log("maxEsp: " + maxEsp);
							
							$("#totalEspectadores").html("Total espectadores:  " + contadorTotal );	

							google.charts.load('current', {packages: ['corechart', 'bar'] , 'language': 'es' } );							
							 // console.log("nuevo array ");	console.log( arrayFechaEspect2);							
							 google.charts.setOnLoadCallback(drawStacked);

							function drawStacked() {
								//console.log("El resultado es el siguiente aaaa: "); console.log(datoUsuario);
								var data = new google.visualization.DataTable();
								data.addColumn('date', 'Time of Day');
			  
								for(var i=0; i<datoUsuario.datos.length; i++){
									var etiqueta = "partido " + datoUsuario.datos[i].titulo;
									data.addColumn('number', etiqueta);
								}
								data.addRows(arrayFechaEspect2);
							
								var fechaMax =  datoUsuario.fechas.length - 1;
								console.log(fechaMax);
								
								var widthGrafica = $("#cont_estadisticas").width() ;
								$("#chart_div").css('width', widthGrafica);
								console.log(widthGrafica);
								//	console.log(datoUsuario.fecha[0]['fechaEntera']);
								
								var options = {
									bar: { groupWidth: '90%' }, // grosor de la barra
									title: "Espectadores por día", // titulo de la grafica
									legend: 'none', // leyenda de la grafica
									isStacked: true, // Barra unificada en la misma fecha, sino saldrian varias barras
									colors: ['#795548'], // color de las barras o linea
									// tooltip: { isHtml: true },    // CSS styling affects only HTML tooltips.
									// backgroundColor: '#f1f8e9', // color del fondo de la grafica
									
									hAxis: {
										title: 'Fecha ', 
										format:'YYYY-MM-DD', 
										//format: 'long',
										//minValue: datoUsuario.fechas[0].fechaEntera,
										///maxValue: datoUsuario.fechas[fechaMax].fechaEntera
										
										gridlines: {
											count: -1,
										/*	units: {
												days: {format: ['MMM dd']},
												hours: {format: ['HH:mm', 'ha']},
											}	
										},*/
										/*minorGridlines: {
											units: {
												hours: {format: ['hh:mm:ss a', 'ha']},
												minutes: {format: ['HH:mm a Z', ':mm']}
											}
										},*/
							
										viewWindow: {
											min: datoUsuario.fechas[0].fechaEntera,
											max:  datoUsuario.fechas[fechaMax].fechaEntera,
										},			
										
									},
										
										
									}, //  fin hAxis
						
									vAxis: {
										title: 'Espectadores', 
										gridlines: {
											count: -1,
											/*units: {
												days: {format: ['MMM dd']},
												hours: {format: ['HH:mm', 'ha']},
											}	*/
										},
										minorGridlines: {
											/*units: {
												hours: {format: ['hh:mm:ss a', 'ha']},
												minutes: {format: ['HH:mm a Z', ':mm']}
											}*/
										},
							
										viewWindow: {
											min: 0,
											max: maxEsp,
										},						
									}, //  fin vAxis
									
									
									
									//  series: { datoUsuario.fechas },
									
								//	pointSize: 20,
								//	pointShape: 'square',								
									
									//width: widthGrafica,
								//	height: 600,
									
									//	legend: { position: "none" },
									// legend: { position: 'top', maxLines: 3 },
									
								
										//series: [{'color': '#D9544C'}],
									//intervals: { style: 'boxes' },
										
								};				
								

								var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
								chart.draw(data, options);
								
								
								
		 
							}	
							$("#cont_esperando_estadisticas").css("display", "none");
							$("#cont_estadisticas").css("display", "block");
						
						
						
						}	
					// }
					else{
						$("#cont_estadisticas").css("display", "none");						
					
						$("#sinDatosSMS").css("display", "block");					
						$("#sinDatosSMS").html("Esta consulta no tiene datos para poder hacer estadísticas. ");
					}
					
				}, // fin success					
			});	
		} // fin funcion


		function utf8_decode(s) {
		  return decodeURIComponent(escape(s));
		}
	</script>