angular.module('streamSports')
    .controller('LoginCtrl', function($scope, $state, $ionicPopup, AuthService, Broadcast) {

        $scope.session = Broadcast;
        $scope.login = function(data) {
            AuthService.login(data.user, data.pass).then(function () {
                $state.go('main', {}, {reload: true});
                $scope.setCurrentUsername(data.username);
                // clear screen
                data.user = '';
                data.pass = '';
            }, function () {
                // clear pass
                data.pass = '';
                // show alert
                $ionicPopup.alert({
                    title: 'Login incorrecto',
                    template: 'Credenciales incorrectos'
                });
            });
        }
    });