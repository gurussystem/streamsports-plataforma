angular.module('streamSports')
    .service('AuthService', function ($q, $http, transformRequestAsFormPost, Broadcast, BASE_URL) {
        var LOCAL_TOKEN_KEY = 'authorization';
        var username = '';
        var isAuthenticated = false;
        var authToken;

        function loadUserCredentials() {
            var token = window.localStorage.getItem(LOCAL_TOKEN_KEY);
            if (token) {
                useCredentials(token);
            }
        }

        function storeUserCredentials(data) {
            Broadcast.setToken(data.token);
            Broadcast.setUrlLive(data.urlPlayer);
            Broadcast.setIdUser(data.idUsuario);
            window.localStorage.setItem(LOCAL_TOKEN_KEY, data.token);
            useCredentials(data.token);
            // Configure current session if any
            Broadcast.restoreSession(data);
        }

        function useCredentials(token) {
            username = token.split('.')[0];
            isAuthenticated = true;
            authToken = token;

            // Set the token as header for your requests!
            $http.defaults.headers.common['Authorization'] = token;
        }

        function destroyUserCredentials() {
            authToken = undefined;
            username = '';
            isAuthenticated = false;
            $http.defaults.headers.common['Authorization'] = undefined;
            window.localStorage.removeItem(LOCAL_TOKEN_KEY);
        }

        var login = function (name, pw) {
            destroyUserCredentials();
            var def = $q.defer();

            $http({
                method: 'POST',
                url: BASE_URL + 'login',
                transformRequest: transformRequestAsFormPost,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                data: {
                    user: name,
                    password: pw
                }
            }).success(function (response) {
                if (response == undefined || response.code != undefined) {
                    def.reject();
                } else {
                    storeUserCredentials(response);
                    def.resolve();
                }
            })
                .error(function () {
                    def.reject();
                });
            return def.promise;
        };

        var logout = function () {
            destroyUserCredentials();
        };

        loadUserCredentials();

        return {
            login: login,
            logout: logout,
            isAuthenticated: function () {
                return isAuthenticated;
            },
            username: function () {
                return username;
            }
        };
    })
    .factory('AuthInterceptor', function ($rootScope, $q, AUTH_EVENTS) {
        return {
            responseError: function (response) {
                $rootScope.$broadcast({
                    401: AUTH_EVENTS.notAuthenticated,
                    403: AUTH_EVENTS.notAuthorized
                }[response.status], response);
                return $q.reject(response);
            }
        };
    })
    .config(function ($httpProvider) {
        $httpProvider.interceptors.push('AuthInterceptor');
    })
    .factory("transformRequestAsFormPost", function () {
        // I prepare the request data for the form post.
        function transformRequest(data, getHeaders) {
            var headers = getHeaders();
            headers["Content-Type"] = "application/x-www-form-urlencoded; charset=utf-8";
            return ( serializeData(data) );
        }

        // Return the factory value.
        return ( transformRequest );
        // ---
        // PRIVATE METHODS.
        // ---
        // I serialize the given Object into a key-value pair string. This
        // method expects an object and will default to the toString() method.
        // --
        // NOTE: This is an atered version of the jQuery.param() method which
        // will serialize a data collection for Form posting.
        // --
        // https://github.com/jquery/jquery/blob/master/src/serialize.js#L45
        function serializeData(data) {
            // If this is not an object, defer to native stringification.
            if (!angular.isObject(data)) {
                return ( ( data == null ) ? "" : data.toString() );
            }
            var buffer = [];
            // Serialize each key in the object.
            for (var name in data) {
                if (!data.hasOwnProperty(name)) {
                    continue;
                }
                var value = data[name];
                buffer.push(
                    encodeURIComponent(name) +
                    "=" +
                    encodeURIComponent(( value == null ) ? "" : value)
                );
            }
            // Serialize the buffer and clean it up for transportation.
            var source = buffer
                    .join("&")
                    .replace(/%20/g, "+")
                ;
            return ( source );
        }
    });
