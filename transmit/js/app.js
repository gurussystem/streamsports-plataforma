// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('streamSports', ['ionic', 'ionMdInput', 'streamSports.broadcast'])

    .run(function ($rootScope, $ionicPlatform, $state, $ionicLoading, AuthService) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs).
            // The reason we default this to hidden is that native apps don't usually show an accessory bar, at
            // least on iOS. It's a dead giveaway that an app is using a Web View. However, it's sometimes
            // useful especially with forms, though we would prefer giving the user a little more room
            // to interact with the app.
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                // Set the statusbar to use the default style, tweak this to
                // remove the status bar on iOS or change it to use white instead of dark colors.
                StatusBar.styleDefault();
            }
            $rootScope.$on('$stateChangeStart', function (event, next, nextParams, fromState) {
                if (!AuthService.isAuthenticated()) {
                    if (next.name !== 'login') {
                        event.preventDefault();
                        $state.go('login');
                    }
                }
            });

            $rootScope.$on('loading:show', function() {
                $ionicLoading.show({template: '<ion-spinner class="spinner-positive" icon="lines"></ion-spinner>'})
            });

            $rootScope.$on('loading:hide', function() {
                $ionicLoading.hide()
            });
        });
    })

    .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider, $httpProvider) {
        $ionicConfigProvider.views.transition('none');
        $stateProvider
            .state('login', {
                url: '/login',
                controller: 'LoginCtrl',
                templateUrl: 'templates/login.html'
            })
            .state('main', {
                url: '/main',
                controller: 'MainCtrl',
                templateUrl: 'templates/main.html'
            })
            .state('config', {
                url: '/config',
                //controller: 'ConfigCtrl',
                templateUrl: 'templates/config.html'
            })
            .state('config.patrocinadores', {
                url: '/patrocinadores',
                views: {
                    'content-view': {
                        templateUrl: 'templates/config/patrocinadores.html',
                        controller: 'AdsCtrl'
                    }
                }
            })
            .state('config.settings', {
                url: '/settings',
                views: {
                    'content-view': {
                        templateUrl: 'templates/config/settings.html',
                        controller: 'SettingsCtrl'
                    }
                }
            });

        $urlRouterProvider.otherwise(function ($injector) {
            var $state = $injector.get("$state");
            $state.go("login");
        });

        $httpProvider.interceptors.push(function($rootScope) {
            return {
                request: function(config) {
                    $rootScope.$broadcast('loading:show');
                    return config
                },
                response: function(response) {
                    $rootScope.$broadcast('loading:hide');
                    return response
                },
                responseError: function(response) {
                    $rootScope.$broadcast('loading:hide');
                    return response
                }
            }
        });
    });
