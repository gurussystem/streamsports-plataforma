angular.module('streamSports')
    .controller('MainCtrl', function ($scope, $ionicModal, $location, Broadcast, $ionicPopup, $timeout, AuthService) {
        $scope.broadcast = Broadcast;
        $scope.showRetry = false;

        $scope.refreshVideo = function () {
            if ($scope.broadcast.getToken() != undefined) {
                $scope.showRetry = false;
                $scope.videoPlayer.sources = [{src: $scope.broadcast.getUrlLive(), type: "video/mp4"}];
                $timeout(function(){
                    $scope.videoPlayer.play();
                }, 1000);
            }
        };

        // redirect if no session is loaded
        if ($scope.broadcast.getToken() == undefined){
            $scope.broadcast.clear();
            $location.path("/");
            $scope.showRetry = true;
        }

        $scope.onPlayerReady = function(videoPlayer) {
            $scope.videoPlayer = videoPlayer;
            $scope.refreshVideo();
        };

        $scope.movements = Broadcast.getMovements();

        $ionicModal.fromTemplateUrl('templates/broadcastModal.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.broadcastModal = modal;
        });

        $ionicModal.fromTemplateUrl('templates/moveModal.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.moveModal = modal;
        });

        $scope.onPlayerError = function() {
            $scope.showRetry = true;
        };

        $scope.startBroadcast = function () {
            $scope.broadcast.check().then(function(){
                $scope.broadcast.reset();
                $scope.step = 1;
                $scope.broadcastModal.show();
            }, function(){
                $ionicPopup.alert({
                    title: 'Error',
                    template: 'Stream no activado. Pon el stream a LIVE y vuelve a intentar'
                });
            });
        };

        $scope.nextStep = function () {
            $scope.step++;
            var input = document.getElementById("boradcastModalInput"+$scope.step);
            if (input != undefined) {
                $timeout(function(){
                    input.focus();
                }, 500);
            }
        };

        $scope.submitBroadcast = function (broadcast) {
            $scope.broadcastModal.hide();
            $scope.broadcast.setTitle(broadcast.title);
            $scope.broadcast.setLocalTeamName(broadcast.localTeamName);
            $scope.broadcast.setVisitorTeamName(broadcast.visitorTeamName);
            $scope.broadcast.start().then(function () {

            }, function () {
                $ionicPopup.alert({
                    title: 'Error',
                    template: 'Ha ocurrido un error'
                });
            });
        };

        $scope.endTimeBroadcast = function () {
            $scope.broadcast.endTime();
        };

        $scope.startTimeBroadcast = function (time) {
            if (time == undefined) {
                $ionicModal.fromTemplateUrl('templates/timeModal.html', {
                    scope: $scope,
                    animation: 'slide-in-up'
                }).then(function (modal) {
                    $scope.timeModal = modal;
                    $scope.timeModal.show();
                });
            } else {
                $scope.timeModal.hide();
                $scope.broadcast.startTime(time);
            }
        };

        $scope.finishBroadcast = function () {
            $scope.broadcast.stop().then(function () {
                //$scope.$digest();
            }, function () {
                $ionicPopup.alert({
                    title: 'Error',
                    template: 'Ha ocurrido un error'
                });
            });
        };

        $scope.scoreUpLocal = function () {
            $scope.broadcast.scoreUpLocal();
        };

        $scope.scoreDownLocal = function () {
            $scope.broadcast.scoreDownLocal();
        };

        $scope.scoreUpVisitor = function () {
            $scope.broadcast.scoreUpVisitor();
        };

        $scope.scoreDownVisitor = function () {
            $scope.broadcast.scoreDownVisitor();
        };

        $scope.newMove = function () {
            $scope.broadcast.newMove(function (moveId) {
                if (moveId) {
                    $scope.move = {id: moveId};
                    $scope.moveModal.show();
                } else {
                    console.log("error");
                }
            });
        };

        $scope.submitMove = function (move) {
            $scope.broadcast.editMove(move);
            $scope.moveModal.hide();
        };

        $scope.playMove = function (move) {
            $scope.broadcast.playMove(move);
        };

        $scope.stopPlay = function (move) {
            $scope.broadcast.stopPlay(move);
        };

        $scope.playMoves = function () {
            $scope.broadcast.playMoves();
        };

        $scope.stopPlayingMoves = function () {
            $scope.broadcast.stopPlayingMoves();
        };

        $scope.logout = function() {
            $scope.broadcast.reset();
            AuthService.logout();
            $location.path("/");
        }
    });
