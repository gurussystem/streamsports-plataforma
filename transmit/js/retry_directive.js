angular.module('streamSports')
    .directive("videoRetryPlugin",
    [function() {
        return {
                restrict: "E",
                require: "^videogular",
                template:'<div class="center" ng-show="!API.isLive"><br><br><br><br><button class="button button-assertive icon ion-refresh button-large" ng-click="retryVideo()"></button></div>',
                link: function(scope, elem, attrs, API) {
                    scope.API = API;
                    scope.retryVideo = function() {
                        API.play();
                    };
                }
        }
    }]);