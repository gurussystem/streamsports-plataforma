	var $j = jQuery.noConflict(); 
	var minPartido = new Array();
	var minPartido_aux = new Array();
	var urlPartes = new Array();
	var listaCiudades = new Array();
	var listaCiudades_sort = new Array();
	var productosArray = new Array();
	var r;
	var numOrdenadores;  
	var numMoviles ; 
	var viewersHours;
	var mediaVisualizacion; 
	var espectadoresTotal; 
	var urlActual = location.search;
	var idPartido ;
		
	$j(document).ready(function() {
		idPartido = urlActual.substr(1);	
		// console.log("urlActual: " + urlActual);	
		// console.log("idPartido: " + idPartido);			
		minutosPartido();
	});
		
		
	//Convierte objeto a array
	function inspeccionar(obj){

		var msg = new Array();

		for (var property in obj) {
			if (typeof obj[property] == 'function'){
				var inicio = obj[property].toString().indexOf('function');
				var fin = obj[property].toString().indexOf(')')+1;
				var propertyValue=obj[property].toString().substring(inicio,fin);
				msg[msg.length] = {'type' : (typeof obj[property]), 'name' : property, 'value' : propertyValue};
			}
			else if (typeof obj[property] == 'unknown'){
				msg[msg.length] = {'type' : 'unknown', 'name' : property, value : 'unknown'};
			}else{
				msg[msg.length] ={'type' : (typeof obj[property]), 'name' : property, 'value' : obj[property]};
			}
		} // fin for
		return msg;
	} //fin funcion

			
			
	var minutosTotal;
	var procesoEstadistica;
	function minutosPartido(){
		//console.log("minutosPartido " + idPartido);
		$j("#cargando").html(idPartido);
		
		$j.ajax({
		//	async:true, 
		//	cache:false,
			type: 'POST',
   // encoding:"UTF-8",
   // contentType: "text/json; charset=UTF-8",
			// url:  'consulta.php', 
			data: { 
				'idPartido' : idPartido,
				//'fechaInicio' : fechaInicio,
				//'fechaFin' : fechaFin
			},
			success: function(response){ 				
				// console.log(response);				
				productosArray = $j.parseJSON(response);
				// console.log(productosArray);
				procesoEstadistica = productosArray['proceso'];
				if(procesoEstadistica == 0 ){
					$j("#cont_esperando_estadisticas").css("display", "none");
					$j("#cont_estadisticas").css("display", "none");
					
					$j("#sinDatosSMS").css("display", "block");					
					$j("#sinDatosSMS").html("Este partido no tiene datos para poder hacer estadísticas. ");
				
				}
				else{
					$j("#sinDatosSMS").css("display", "none");
					
					minPartido = productosArray['minPartido'];
				
					// Copiamos el array pero con intervalos de 10 minutos
					//Estaria bien hacer multipo
					cont = 0;
					cont2 = 0;
					for(var i = 0; i<= minPartido.length-1; i++){						
						if( ( cont ==  0) || ( cont ==  5)  || ( cont2 ==  minPartido.length-1 ) ){ // primera , cada 5 y la ultima posicion
							minPartido_aux[i] = minPartido[i];
							cont=0;
							//console.log(minPartido[i] + " cont2:" + cont2 );
						}else{
							minPartido_aux[i] = "";
						}			
				
						cont++;
						cont2++;
					}//fin for
					// console.log(minPartido_aux);
						
					arrayUsuarios = productosArray['contUsuarios'];
					// console.log(arrayUsuarios);
					minutosTotal = productosArray['minutoTotal'];
					numOrdenadores = productosArray['numOrdenadores'];
					numMoviles = productosArray['numMoviles'];
					espectadoresTotal = productosArray['espectadores'];
					espectadoresTotal_aux = productosArray['espectadores_aux'];
					viewersHours  = productosArray['viewersHours'];
					mediaVisualizacion = productosArray['tmpMediaVisualizaion'];
					titulo = productosArray['titulo'];
					fechaTitulo = productosArray['fechaTitulo'];
					eLocal = productosArray['eLocal'];
					eVisitante = productosArray['eVisitante'];
					listaCiudades =  inspeccionar(productosArray['listaCiudades']); //Convierto el objero a un array manipulable.
				}// fin hay datos
			},
			
			complete: function(){ 
				if( procesoEstadistica != 0 ){
				
					$j("#cont_esperando_estadisticas").css("display", "none");
					$j("#cont_estadisticas").css("display", "block");
				
					// console.log("vamos a ordenar");
					//	listaCiudades_sort = arsort(listaCiudades);
					listaCiudades_sort  = listaCiudades.sort(function (a, b){			return (b.value - a.value) })	
					
					//Asignamos valores a la tabla del donut para poder generar el donut.
					$j("#table_th_pc").html('Ordenador');
					$j("#table_th_app").html('Móviles');
					$j("#table_td_pc").html(numOrdenadores +"%");
					$j("#table_td_app").html(numMoviles+"%");
					$j("#numEspectadores").html(espectadoresTotal);
					$j("#numMediaVisualizacion").html(mediaVisualizacion);
					$j("#numViewersHours").html(viewersHours);
					$j("#subtitulo").html(titulo + " - " + fechaTitulo);
					$j("#subtitulo_equipos").html(eLocal + " - " + eVisitante);
						
					// chart donut
					var values = [],
					labels = [],
					legends = true,
					legendsElement = $j('#TicketByDepartmentLegends'),
					colors = ['#6196aa','#98cdd2','#d5664e','#e67157'];

					$j("#TicketByDepartment tr").each(function () {
					  values.push(parseInt($j("td", this).text(), 10));
					  labels.push($j("th", this).text());
					});

					$j("#TicketByDepartment").hide();
						var r = Raphael("DonutTicketsByDepartment", 200, 200);
						r.donutChart(100, 100, 88, 35, values, labels, colors, legends, legendsElement, colors);
					
						var widthLineal =minutosTotal *30;
						//	console.log("completado"); 
						//	console.log(minPartido); 
						//	console.log(minPartido_aux); 
						$j('linegraph2').css("border", "1px solid blue");
					
						var linegraph2 = new Grafico.LineGraph($('linegraph2'),{ workload: arrayUsuarios },
						{					
							grid :  true,
							plot_padding :0,
							curve_amount :  1,	
							start_at_zero : true,
							font_size : 11,
							vertical_label_unit :   "",
							colors :   {workload: '#4b80b6'},
							background_color : "#fff",
							label_color :  "#666",
							grid_color:"#fff", //ccf
							markers : "value",	
							colors : {workload: '#4b80b6'},					
							draw_axis : true, //lineas basicas de la grafica
							height:	250,
							labels :  minPartido_aux,
							//labels :  minPartido,
							//meanline :   true,
							label_max_size: 4,
							//area_opacity :  1,
							draw_hovers: true,						
							datalabels : {workload:function(idx, value) { return value+  " usuarios conectados en el minuto " + idx + " del partido"}},
							//width: widthLineal ,
						}
					);//fin grafica lineal
				
					// Lista de ciudades
					// listaCiudades_sort. name / value
					var codigoTabla = '<table id="lugarAcceso" cellpadding="0" cellspacing="0" border="1" style="    font-family: sans-serif;">';
					var resto_name;
					var resto_value = 0;
					var restoCont = 0;
				//	var contAccesosTotal =0;
					var contandoPosicion =0;
					
					for(var i = 0; i<= listaCiudades_sort.length-1; i++){	
						// console.log("listaCiudades_sort length: " + listaCiudades_sort.length);
					//	console.log(listaCiudades_sort[i]);
						
						
						if(listaCiudades_sort[i].name == ""){ 
							// console.log("esta vacio " +  listaCiudades_sort[i].name  ) ; 					
						} 
						else if(listaCiudades_sort[i].name == 'Resto'){
							resto_name =	listaCiudades_sort[i].name;
							resto_value = resto_value + listaCiudades_sort[i].value;
							restoCont = 1;
						}else{
							if(contandoPosicion < 9){
								codigoTabla += '<tr>';
									codigoTabla += '<th scope="row"  id="" >'+listaCiudades_sort[i].name+' </th>';
									// console.log(listaCiudades_sort[i].name);
									codigoTabla += '<td id="" > '+listaCiudades_sort[i].value +' </td>';
								codigoTabla += '</tr>';
							}else{
								resto_value = resto_value + listaCiudades_sort[i].value;
							}
						}
						//contAccesosTotal = contAccesosTotal + listaCiudades_sort[i].value;
						contandoPosicion = contandoPosicion + 1 ;
					}//fin for
					
					//console.log("contAccesosTotal : " + contAccesosTotal);
					masResto_value = espectadoresTotal - espectadoresTotal_aux;
					if(masResto_value > 0){
						resto_value = resto_value + masResto_value;
					}
					if(restoCont == 1){
						
					
						//añado Resto
						codigoTabla += '<tr>';
						codigoTabla += '<th scope="row"  id="" >'+resto_name+' </th>';
						codigoTabla += '<td id="" > '+resto_value+' </td>';
						codigoTabla += '</tr>';
						codigoTabla += '</table>';
					}
					// console.log(codigoTabla);
					$j("#cont_ciudades").html(codigoTabla);	
					
				} // fin if procesoEstadistica				
			}//complete
		});
	}
		