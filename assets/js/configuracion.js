
	/***********************************************************************************************
	@Descripcion	Según la 'tab' pulsada de la parte de 'configuración' haremos unas cosas u otras
						
	***********************************************************************************************/	
		$(document).on( 'shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
		//	console.log(e.target) // activated tab e.target
			//console.log(e.currentTarget['hash']) // activated tab e.target
			
			switch(e.currentTarget['hash']){
				case '#proximoPartido':
					loader_textos_proximoPartido(); // cargamos los textos
					
					if(noRecargar_tab_proximoPartido == 0){
						noRecargar_tab_proximoPartido = 1;
						infoCliente('proximoPartido');	
					}				
					mensajes_fin("proximoP");
					break;
				
				case '#bloqueoIp':
					if(noRecargar_tab_bloqueo == 1){
						ini_BloqueIp();	
					}
					 mensajes_fin('pro');
					 mensajes_fin('pob');
					break;
					
				case '#configurarDatos':
					break;
			}	
			
		// 	loader_textos();
			
		});

	
	
	
	
	
	
	/***********************************************************************************************
	@Descripcion	
						
	***********************************************************************************************/	
	function comprobarSms(){
		// Eliminamos el 'No hay ... ' en caso que exista
		if( $("#mensajes").is(':visible') ){
			$( "#mensajes").css("display", "none");
			// console.log("he quitado el mensjae");
		}
	}
	
	
	
	/***********************************************************************************************
	@Descripcion	Primeras ejecuciones
						
	***********************************************************************************************/	
	jQuery(document).ready(function() { 

	}); // fin jquery


	
	
	
	/***********************************************************************************************
	@Descripcion	Primera ejecución de bloquear Ip
						
	***********************************************************************************************/	
	function ini_BloqueIp(){
		loaderDatos('Cargando poblaciones...','pob'); // img cargando datos
		loaderDatos('Cargando provincias...','pro'); // img cargando datos	
		infoCliente('bloqueoIp');
	}

	
	/***********************************************************************************************
	@Descripcion	Sacamos los datos del cliente a través de una consulta sql
	@opcionMenu	Nos indica de donde viene para hacer unas funcionalidades y otras.
						Si viene de 'bloqueoIp'
							Actualizamos el array poblaciones.
							Actualizamos el array provincias.
							Cargar los input-serch de información con las todas las poblaciones y todas las provincias
							
						Si viene de 'proximoPartido'
							Llamamos a la funcion 'listado_proximos_partidos()'
	***********************************************************************************************/	
	function infoCliente(opcionMenu){
		
		
		var resultHost = (location.hostname).split(".");  // Sacamos el subdominio 
		//console.log("infoCliente - " + resultHost);
		//	console.log("infoCliente - " + location.hostname);
		// Hacemos la consulta
		$.ajax({
			async:true, 
			cache:false,
			type: 'GET',
			data: { 'subdominio' : resultHost[0]	},
			url: '/assets/io/userWhat.php', 
			success: function(response){
				datoUsuario =  $.parseJSON(response);	
			//	console.log("El resultado es el siguiente: "); 				console.log(datoUsuario);				
			}, // fin success					
			complete: function(){
				switch(opcionMenu){
					case 'bloqueoIp':						
						actualizarArray('pro', 'insert');
						actualizarArray('pob', 'insert');
						listaPoblaciones(); // me guardo en un array todas las poblaciones que existen en españa
						break;
					
					case 'proximoPartido': 						
						listado_proximos_partidos('inicial');
						break;	
				} // fin switch
			} // fin succes
		});	
	} // Fin datosUsuario
	
	
	/***********************************************************************************************
	@Descripcion	Sacamos un listado de todos los partidos que ha programado el cliente, y lo guardamos en un array.
	@opcion 		Nos indica de donde viene para hacer unas funcionalidades y otras.
	
	***********************************************************************************************/	
	function listado_proximos_partidos(opcion){
		// console.log("listado_proximos_partidos : " +  datoUsuario[0].subdominio +","+ datoUsuario[0].idUsuario);
		$.ajax({
			async:true, 
			cache:false,
			type: 'GET',
			data: { 
				'subdominio' : datoUsuario[0].subdominio,
				'idUsuario' : datoUsuario[0].idUsuario				
			},
			url: '/assets/io/proximoPartido/listado.php', 
			success: function(response){
				datos_partidos =  $.parseJSON(response);	
								
				// Comprobamos que la consulta no ha dado error
				if(datos_partidos[0].numResult == 0){
					// console.log(datos_partidos[0].smsResult); // mensaje de error
				}else{					
					$("#listaPartidos").html(thPartidos); // lo limpio					
			
					switch(opcion){
						case 'inicial':							
							if(datos_partidos[0].numRows == 0){ // no hay datos
								$("#listaPartidos").append(noHay_partidos);
								datos_partidos.length=0;
							}else{
								for(var i=0; i<datos_partidos.length; i++){			
									appendPartido(datos_partidos[i]);
								} // Fin for 	
							}
							break;
										
						case 'update':
							if(datos_partidos[0].numRows == 0){ // no hay datos
								datos_partidos.length=0;
								// console.log(noHay_partidos);
							}
							actualizar_listaPartido();
						break;
					} // fin switch
				}// fin else	
			} // fin success
		});	
	}
	
	
	/***********************************************************************************************
	@Descripcion		Añade visualmente una nueva fila en la tabla
	@arrayPartido		array que tenemos que tratar.
	
	***********************************************************************************************/	
	function appendPartido(arrayPartido){
		// Paso a variables 
		var id =arrayPartido.id;
		

		if(dominio=='streamsports'){
			var titulo = arrayPartido.tituloPartido; 
			var eLocal = arrayPartido.equipoLocal;
			var eVisitante = arrayPartido.equipoVisitante;
			var fecha = arrayPartido.fechaPartido2;		
			var param_evento = id + " , &#39;"+titulo+"&#39; , &#39;"+eLocal+"&#39;  ,  &#39;"+eVisitante+"&#39;  ,  &#39;" + fecha + "&#39";
		}else{
			var titulo = arrayPartido.titulo; 
			var descripcion = arrayPartido.descripcion;
			var fecha = arrayPartido.fechaEvento2;		
			var param_evento = id + " , &#39;"+titulo+"&#39; , &#39;"+descripcion+"&#39;  ,  &#39;" + fecha + "&#39";
		}
		var labelHTML = "";
		
		labelHTML += "<tr id='tr_pp_"+id+"'  class='pblock_tr' >";
		labelHTML += "<td class='lbl_listaPartidos pblock_txt' id='lbl_"+id+"'> " ;
	//	console.log("dominio: " + dominio );
		if(dominio=='streamsports')
			labelHTML += titulo + " :  "  + eLocal + " - " + eVisitante  + " (" + fecha + ")";
		else
			labelHTML += titulo + " :  "  + descripcion   + " (" + fecha + ")";
			
		labelHTML += "</td>";
		/** btn editar (forma rapida) */
		labelHTML += "<td  class='pblock_img_proximo'>";	
		if(dominio=='streamsports')
		labelHTML += "<button type='button' class='circular ui icon button tiny edit_lbl'  title='Editar evento'  onclick='editarPartido("+param_evento+")'>";
		else
		labelHTML += "<button type='button' class='circular ui icon button tiny edit_lbl'  title='Editar evento'  onclick='editarEvento("+param_evento+")'>";
			
		labelHTML += "<i class='icon edit'></i>";
		labelHTML += "</button>";
		labelHTML += "</td>";
		/** btn eliminar */
		labelHTML += "<td  class='pblock_img_proximo'>";	
		labelHTML += "<button type='button' class='circular ui icon button tiny delete_lbl'  title='Eliminar evento'  onclick='deleteEvento("+id+")'>";
		labelHTML += "<i class='icon delete'></i>";
		labelHTML += "</button>";
		labelHTML += "</td>";
		labelHTML += "</tr>";
		
		$("#listaPartidos").append(labelHTML);
		
	}
	
	

	/***********************************************************************************************
	@Descripcion	Guardamos el nuevo partido programado por sql
					
	***********************************************************************************************/
	function addPartido(){
		mensajes_fin("proximoP");
		
		// 1. Asigno campos a variables 
		var titulo = $("#tituloPartido").val() ;
		var fecha = $("#datetimepicker4").val() ;
		
		if(dominio == "streamsports"){
			var eLocal = $("#equipo1").val();
			var eVisitante = $("#equipo2").val();
			var descripcion = "";
		}else{
			var descripcion = $("#descripcionEvento").val();
			var eLocal = "";
			var eVisitante = ""; 
		}
		/*
		console.log("titulo: " + titulo);
		console.log("fecha: " + fecha);
		console.log("descripcion: " + descripcion);
		console.log("eLocal: " + eLocal);
		console.log("eVisitante: " + eVisitante);
		*/
		// 2.  comprobar que esta todo bien
		if ((titulo != "") &&  (fecha != "") && ( ((eLocal != "") && (eVisitante != "") ) || ((descripcion != "")) ) ){
		
console.log("fecha: " + fecha);			
var fechaHora = convertir_fechaHora(fecha) ; // 2017-05-31 14:00:00
			console.log("fechaHora: " + fechaHora);
		
		
			$.ajax({
				async:true, 
				cache:false,
				type: 'GET',
				data: { 
					'subdominio' : datoUsuario[0].subdominio,
					'idUsuario' : datoUsuario[0].idUsuario,
					'tituloPartido' :titulo,
					'equipoLocal' : eLocal,
					'equipoVisitante' : eVisitante,
					'descripcion' : descripcion,
					'fechaHoraPartido' : fechaHora,
					'tiempoDesfase': desfaseHoraria
				},
				url: '/assets/io/proximoPartido/insertar.php',  // falta hacer la consulta
				success: function(response){
			//		console.log("RESPUESTA: " + response);
					var datos =  $.parseJSON(response);	
					
						
					// Comprobamos que la consulta no ha dado error
					if(datos[0].numResult == 0){
						$("#mensajes").html("Ha ocurrido algún error al añadir el partido. ");
						$("#mensajes").css("background-color", messageColor_danger);
					}else{			
						$("#mensajes").html(datos[0].smsResult);
						$("#mensajes").css("background-color", messageColor_success);
						//Añado en el array un nuevo objeto
						nuevoPartidoProgramado(datos[0].idPartido, titulo,  eLocal, eVisitante, fecha, fechaHora);					
					}
					mensajes_ini('proximoP');
				} // fin success
			});	
			
		}else{
			// mostramos mensaje que faltan datos por rellenar.			
			call_comprobar_datos();	
		}
	}
	
		
	/***********************************************************************************************
	@Descripcion	Actualizamos el array de proximos partidos.
						Si el array no esta vacio lo que  hacemos es ordenarlo, y despues mostrar los cambios por pantalla						
	***********************************************************************************************/	
	function actualizar_listaPartido(){
		if (datos_partidos.length != 0){				
			
			//Ordenamos el array existente
			datos_partidos.sort(function(a,b) {
				if (a.fechaPartido > b.fechaPartido )
					return 1;
				else if ( a.fechaPartido < b.fechaPartido )
					return -1;
				else
					return 0;
			});
					
			// console.log("Array ordenado: "); console.log(datos_partidos);
			
			// Mostrar en pantalla		
			$("#listaPartidos").html("");// lo limpio 
			$("#listaPartidos").html(thPartidos);// lo limpio 
		
			for(var i=0; i<datos_partidos.length; i++){
				appendPartido(datos_partidos[i]);
			}
		
		}else{			
			// Mostrar en pantalla		
			$("#listaPartidos").html("");// lo limpio 
			$("#listaPartidos").html(thPartidos);// lo limpio 
			$("#listaPartidos").append(noHay_partidos);// lo limpio 
		}
	
		limpiarFormulario();
	}
	
	
	/***********************************************************************************************
	@Descripcion	Añadimos un nuevo objeto en el array
					
	***********************************************************************************************/
	function nuevoPartidoProgramado(idPartido, titulo,  eLocal, eVisitante, fecha2, fecha){
		datos_partidos.push({
			equipoLocal:eLocal,
			equipoVisitante:eVisitante,
			fechaPartido:fecha,
			fechaPartido2:fecha2,
			id:idPartido,
			tituloPartido:titulo,
			numRows:(datos_partidos.length+1)
			
		 });
		actualizar_listaPartido(); // ordeno el array con los nuevos datos		  
	}
	
	
	/***********************************************************************************************
	@Descripcion	Cuando pulsamos en uno de los botones de editar un partido programado, mostramos en los inputs 
						los datos que le corresponden.
						
	***********************************************************************************************/
	function editarPartido(id , titulo , eLocal ,  eVisitante ,  fecha){
		
		//Añadimos información a las cajas
		$("#idPartido").val(id) ;
		$("#tituloPartido").val(titulo) ;
		$("#equipo1").val(eLocal);
		$("#equipo2").val(eVisitante);
		$("#datetimepicker4").val(fecha+":00") ;
		// Estaria bien saber como marcar los datos en el calendario
		$("#addPartido").css("display", "none");
	  
		$("#editPartido").css("display", "block");	
		mensajes_fin('proximoP');
		isVisible('#editPartido',  textos.proximoPartido_PHP.editPartido.html, textos.proximoPartido_PHP.editPartido.title, 'boton');
	}
	
	function editarEvento(id , titulo , descripcion  ,  fecha){
		
		//Añadimos información a las cajas
		$("#idPartido").val(id) ;
		$("#tituloPartido").val(titulo) ;
		$("#descripcionEvento").val(descripcion);
		$("#datetimepicker4").val(fecha+":00") ;
		// Estaria bien saber como marcar los datos en el calendario
		$("#addPartido").css("display", "none");
		$("#editPartido").css("display", "block");	
		mensajes_fin('proximoP');
		isVisible('#editPartido',  textos.proximoPartido_PHP.editPartido.html, textos.proximoPartido_PHP.editPartido.title, 'boton');
	}
	
	
	/***********************************************************************************************
	@Descripcion	Guardamos los datos modificados en el formulario por consulta sql
					
	***********************************************************************************************/
	function updatePartido(){
		mensajes_fin("proximoP");
		
		// Cambiamos el formato para guardarlo en la bd
		var fecha = $("#datetimepicker4").val();
		var titulo= $("#tituloPartido").val()  ;
		var idPartido = $("#idPartido").val();
		
		if(dominio == "streamsports"){
			var eLocal = $("#equipo1").val();
			var eVisitante = $("#equipo2").val();
			var descripcion = "";
		}else{
			var descripcion = $("#descripcionEvento").val();
			var eLocal = "";
			var eVisitante = ""; 
		}
		
		// 2.  comprobar que esta todo bien
	//	if ((titulo != "") && (eLocal != "") && (eVisitante != "") &&  (fecha != "") ){
		if ((titulo != "") &&  (fecha != "") && ( ((eLocal != "") && (eVisitante != "") ) || ((descripcion != "")) ) ){			
			var fechaHora = convertir_fechaHora(fecha) ;
			// console.log("Nueva fecha: " + 	fechaHora);	
			$.ajax({
				async:true, 
				cache:false,
				type: 'GET',
				data: { 
					'subdominio' : datoUsuario[0].subdominio,
					'idUsuario' : datoUsuario[0].idUsuario,
					'tituloPartido' :titulo,
					'equipoLocal' : eLocal,
					'equipoVisitante' : eVisitante,
					'descripcion' : descripcion,
					'fechaHoraPartido' : fechaHora,
					'idPartido': idPartido,
					'tiempoDesfase': desfaseHoraria,
					'descripcion' : descripcion
				},
				url:  '/assets/io/proximoPartido/modificar.php',  // falta hacer la consulta
				success: function(response){
					var datos =  $.parseJSON(response);	
									
					if(datos[0].numResult == 0){
							$("#mensajes").html("No se ha podido realizar la modificación.");
							$("#mensajes").css("background-color", messageColor_danger);
						}else{			
							$("#mensajes").html(datos[0].smsResult);
							$("#mensajes").css("background-color", messageColor_success);
							listado_proximos_partidos('update');
						}
						mensajes_ini('proximoP');
						
						// Cambiamos los botones de guardar y añadir
						$("#editPartido").css("display", "none");	
						$("#addPartido").css("display", "block");						
				} // fin success
			});
		}else{
			// mostramos mensaje que faltan datos por rellenar.
			call_comprobar_datos();	
		}			
	}
	
	/***********************************************************************************************
	@Descripcion	Comprtueba todos los datos del formulario, para indicar cual se han dejado por rellenar.
						Dicha función la utilizamos cuando hacemos un 'insert' o un 'update'.
					
	***********************************************************************************************/
	function call_comprobar_datos(){
		comprobar_datos("equipo1");
		comprobar_datos("equipo2");
		comprobar_datos("datetimepicker4");
		comprobar_datos("tituloPartido");	

		if(inputVacio != 0){ // hay algun error
			mensajes_ini("proximoP");
			$("#mensajes").html("Comprueba que no te falte ningún dato por rellenar.");
			$("#mensajes").css("background-color", messageColor_success);
		}else{
			mensajes_fin("proximoP");
		}		
	}
	
	/***********************************************************************************************
	@Descripcion	Reseteamos el formulario dejando todos los inputs sin datos
					
	***********************************************************************************************/
	function limpiarFormulario (){
	//	console.log("limpiarFormulario");
		$("#idPartido").val("") ;
		$("#tituloPartido").val("");
		$("#equipo1").val("");
		$("#equipo2").val("");
		$("#datetimepicker4").val("");
		$("#descripcionEvento").val("");
	}
	

	
	/***********************************************************************************************
	@Descripcion	Comprueba que el input text no este	vacio, si esta vacio indicara por cambio de color del input
					
	***********************************************************************************************/	
	function comprobar_datos(nameDiv){
		
		if($("#"+nameDiv).val() == ""){
			$("#"+nameDiv).css("border" , "1px solid red");
			inputVacio++;
		}else{
			$("#"+nameDiv).css("border" , "1px solid #CCC");	
			inputVacio--;
		}		
	}
	

	/***********************************************************************************************
	@Descripcion	Eliminamos de la BBDD el partido programado escogido.	
					
	***********************************************************************************************/	
	function deleteEvento(idEvento){
		
		$.ajax({
			async:true, 
			cache:false,
			type: 'GET',
			data: { 
				'subdominio' : datoUsuario[0].subdominio,
				'idUsuario' : datoUsuario[0].idUsuario,				
				'idEvento' : idEvento				
			},
			url:  '/assets/io/proximoPartido/eliminar.php', 
			success: function(response){
				var datos =  $.parseJSON(response);	
						
				// Comprobamos que la consulta no ha dado error
				if(datos[0].numResult == 0){
					$("#mensajes").html(datos[0].smsResult);
					$("#mensajes").css("background-color", messageColor_danger);
				}else{			
					$("#mensajes").html(datos[0].smsResult);
					$("#mensajes").css("background-color", messageColor_success);
					listado_proximos_partidos('update');
				}	
				mensajes_ini('proximoP');
			} // fin success
		});		 
	}
	
	
	
	
/*****************************************************************************************************
BLOQUEAR IP
*****************************************************************************************************/
	
	/********************************************************************
	@Descripcion		Cuando el usuario hace algún evento ejecutamos el siguiente 
								codigo para que espere mientras termina de efectuarse la ejecución.
					
	********************************************************************/
	function loaderDatos(mensaje, queSoy){
		// console.log("loaderDatos");
		if(queSoy== 'pob'){
			$("#tabla_pob").append(cargando_pob);
		}else{
			$("#tabla_pro").append(cargando_pro);
		}
		
		$("#cargandoTabla_p_"+queSoy).html(mensaje);
	}
	
	
	/********************************************************************
	@Descripcion		Cuando ya se ha efectuado la ejecucion tenemos que dejar de
							mostrar el loader.png 
					
	********************************************************************/
	function loaderDatos_fin(queSoy){
	//	console.log("loaderDatos_fin: " + queSoy);
		mensajes_ini(queSoy);
		if( $("#cargandoTabla_"+queSoy).is(':visible') ){  //cargandoTabla_pob
			$( "#cargandoTabla_"+queSoy ).remove();
		}
	}
	
	
	/********************************************************************
	@Descripcion	Escondemos el div de los mensajes de quien sea...
					
	********************************************************************/
	function mensajes_fin(queSoy){
		if(queSoy != 'proximoP'){
			$( "#mensajes_"+queSoy ).css('display', 'none');
		}else{
			$( "#mensajes").css('display', 'none');
		}
	}
	
	
	/********************************************************************
	@Descripcion	Mostramos el div de los mensajes de quien sea...
					
	********************************************************************/
	function mensajes_ini(queSoy){	
		if(queSoy != 'proximoP'){
			$( "#mensajes_"+queSoy ).css('display', 'block');
		}else{
			$( "#mensajes").css('display', 'block');
		}
	}
	
	
	/********************************************************************
	@Descripcion	Limpiamos los mensajes y imagenes de cargando.. 
					
	********************************************************************/	
	function limpieza(queSoy){
		// console.log("limpieza");
		// finalizamos mensajes e imagen cargando de poblacion
		loaderDatos_fin('pob');
		mensajes_fin('pob');
		$("#q").val(''); 
		// finalizamos mensajes e imagen cargando de provincia
		loaderDatos_fin('pro');
		mensajes_fin('pro');
		$("#q2").val(''); 
		
		// Iniciamos el mensaje de quien sea.. 
		mensajes_ini(queSoy);
	}
		
		
		
	/********************************************************************
	@Descripcion		
	
	********************************************************************/
	function noHayDatos(queSoy){
		// console.log("noHayDatos " + queSoy);
		loaderDatos_fin(queSoy);
		if(queSoy == 'pob'){
			$("#tabla_pob").append(noHay_pob);
		}else{
			$("#tabla_pro").append(noHay_pro);
		}
		
		// Preguntamos si la tabla esta vacia, si esta vacia update 0, sino nada
		tablaVacia();		
	}
	
		// comprobamos si hay datos en la tabla para ese usuario, sino update users.geobloqueo=0
	function tablaVacia(){
		// console.log("tablaVacia");
		$.ajax({
			async:true, 
			cache:false,
			type: 'GET',
			data: { 'idUsuario' : datoUsuario[0].idUsuario,},
			url: '/assets/io/bloquearP/hayBloqueaciones.php', 
			success: function(response){}
		});		
	}
	
	/********************************************************************
	@Descripcion		
	
	********************************************************************/
	function pulsar(e) {
		tecla = (document.all) ? e.keyCode :e.which;
		return (tecla!=13);
	} 
	
	
	/*****************************************************************
	@Descripcion		Devuelve todas las poblaciones que hay en la tabla 'poblacion'
	@typeahead		
					href: "https://en.wikipedia.org/?title={{display}}",
					callback:
						onInit: null,
						onReady: null,      // -> New callback, when the Typeahead initial preparation is completed
						onSearch: null,     // -> New callback, when data is being fetched & analyzed to give search results
						onResult: null,
						onLayoutBuiltBefore: null,  // -> New callback, when the result HTML is build, modify it before it get showed
						onLayoutBuiltAfter: null,   // -> New callback, modify the dom right after the results gets inserted in the result container
						onClickBefore: null,// -> Improved feature, possibility to e.preventDefault() to prevent the Typeahead behaviors
						// -> New feature, happens after the default clicked behaviors has been executed
						onSendRequest: null,// -> New callback, gets called when the Ajax request(s) are sent
						onReceiveRequest: null,     // -> New callback, gets called when the Ajax request(s) are all received
						onSubmit: null					
	*****************************************************************/
	var dataPoblacion;
	function listaPoblaciones(){
		//console.log("listaPoblaciones");
		$.ajax({
			async:true, 
			cache:false,
			type: 'GET',
			accent: true,
			url:  '/assets/io/bloquearP/listas.php', 
			success: function(response){ 
				dataPoblacion =  $.parseJSON(response);	 
				// console.log(" POBLACIONES INPUTS SEARCH "); console.log(dataPoblacion);				
			},
			
			complete: function(){				
				// Mostramos resultados en input search poblaciones
				$('#q').typeahead({
					minLength: 1, 	// Numero de caracteres para empezar a buscar
					order: "asc",
					group: true, //true

					maxItemPerGroup: 10, // Numero maximo de resultados
					groupOrder: function () {
						var scope = this, 
						sortGroup = [];
						
						for (var i in this.result) {
							sortGroup.push({
								group: i,
								length: this.result[i].length
							});
						}	
			
						sortGroup.sort(
							scope.helper.sort(
								["length"],
								false, // false = desc, the most results on top
								function (a) {
									return a.toString().toUpperCase()
								}
							)
						);

						return $.map(sortGroup, function (val, i) {
							return val.group
						});
					},
						
					hint: true,   //Si la activamos a true solo muestra palabras que empiezan por lo que estamos escribiendo
					dropdownFilter:  false,	// todo		
					template: "{{display}} <small><em></em></small>",
					emptyTemplate: 'No se han encontrado resultados con ese nombre',
					source: {
						poblacion: { 	data: dataPoblacion.poblacion	},
						//provincia: {	data: dataPoblacion.provincia	},
						//	comunidad: {	data: dataPoblacion.comunidad	}
					},
					
					callback: {					
						onClickAfter: function (node, a, item, event) {	
							// console.log("item.display: " + item.display);
							add_bloquearPoblacion(item.display, 'pob'); //te mando el nombre de la poblacion a bloquear
						},
					},
					debug: true,
				});
				
				// Mostramos resultados en input search provincias
				$('#q2').typeahead({
					minLength: 1, 	// Numero de caracteres para empezar a buscar
					order: "asc",
					group: true, 
					maxItemPerGroup: 10, // Numero maximo de resultados
					groupOrder: function () {
						var scope = this, 
						sortGroup = [];
						
						for (var i in this.result) {
							sortGroup.push({
								group: i,
								length: this.result[i].length
							});
						}	
			
						sortGroup.sort(
							scope.helper.sort(
								["length"],
								false, // false = desc, the most results on top
								function (a) {
									return a.toString().toUpperCase()
								}
							)
						);

						return $.map(sortGroup, function (val, i) {
							return val.group
						});
					},
						
					hint: true,   //Si la activamos a true solo muestra palabras que empiezan por lo que estamos escribiendo
					dropdownFilter:  false,		
					template: "{{display}} <small><em></em></small>",
					emptyTemplate: 'No se han encontrado resultados con ese nombre.',
					source: { provincia: {	data: dataPoblacion.provincia	}, },
					callback: {					
						onClickAfter: function (node, a, item, event) {	
							//  console.log("item.display: " + item.display);
								add_bloquearPoblacion(item.display, 'pro'); //te mando el nombre de la poblacion a bloquear
						},
					},
					debug: true,
				});
			} // complete
		});	
	}

	/********************************************************************
	@Descripcion		
	
	********************************************************************/
	function actualizarArray(queSoy, op){
		//console.log("actualizarArray");
		$.ajax({
			async:true, 
			cache:false,
			type: 'GET',
			data: { 
				'subdominio' : datoUsuario[0].subdominio,
				'idUsuario' : datoUsuario[0].idUsuario,
				'queSoy' : queSoy
			},
			url:  '/assets/io/bloquearP/actualizarDatos.php', 
			success: function(response){ 
				if(queSoy == 'pob'){
					blockedPob =  $.parseJSON(response);
				console.log(queSoy+ ":"); 	console.log(blockedPob);
					// Consulta ha dado error o no
					if(blockedPob[0].numResult == 0){
						console.log(blockedPob[0].smsResult); // mensaje de error
					}else{
						if(blockedPob[0].numRows == 0){
							noHayDatos('pob'); // No hay datos
							
						}else{
							if(op == 'insert'){
								for(var i=0; i < blockedPob.length; i++ ){
									append_pbloqueada_table( blockedPob[i],  'old',   queSoy );
								} // fin for
							}
						} // fin else numRows
					} // fin else numResults
						
				}else{
					blockedPro =  $.parseJSON(response);
					
					// Consulta ha dado error o no
					if(blockedPro[0].numResult == 0){
						console.log(blockedPro[0].smsResult); // mensaje de error
					}else{
						if(blockedPro[0].numRows == 0){
							noHayDatos('pro'); // No hay datos
						}else{
							if(op == 'insert'){
								for(var i=0; i < blockedPro.length; i++ ){
									append_pbloqueada_table( blockedPro[i],  'old',  queSoy );
								} // fin for
							}
						} // fin else numRows
					} // fin else numResults
					
				}				
			} // fin succes 
		});
	}
	
	
	function append_pbloqueada_table(nameArray, op ,  queSoy){
		//  console.log("append_pbloqueada_table");
		//  console.log(nameArray); 		console.log(op); 		console.log(queSoy);
		
		loaderDatos_fin(queSoy);
		
		var  listaB ="";
		var paramEliminar = nameArray.id+", &#39;"+queSoy+"&#39;"; 	// &#39; son comillas
		
		if(queSoy == 'pob'){
			var nom = nameArray.poblacion +' ('+nameArray.provincia+')';		
			var title_info	= "Desbloquear población";
		}else{
			var nom = nameArray.provincia;	
			var title_info	= "Desbloquear provincia";			
		}
	
		// Eliminamos el 'No hay ... ' en caso que exista
		if( $("#tr_noHay_"+queSoy).is(':visible') ){
			$( "#tr_noHay_"+queSoy ).remove();
		}
		
	
		
		// Añado a la tabla
		listaB = "<tr id='"+queSoy+"_"+nameArray.id+"'  class='pblock_tr' >"; 
		listaB += "<td 	class='pblock_txt'> " + nom + " </td>" ;
		//listaB += "<td  class='pblock_img'>  <img src='http://"+location.hostname+"/assets/img/borrarP.png'  title='"+title_info+"'  onClick='eliminarP(" +paramEliminar+ ")' />  </td>";
		
		listaB += "<td  class='pblock_img'>";	
		listaB += "<a class='circular ui icon button tiny delete_lbl' href='#' title='"+title_info+"'  onclick='eliminarP("+paramEliminar+")'>";
		listaB += "<i class='icon delete'></i>";
		listaB += "</a>";
		listaB += "</td>";
		
		listaB += "</tr> "; 
		
		$("#tabla_"+queSoy).append(listaB);			
	}
	
	/*****************************************************************
	@Descripcion		añadimos a la ciudad a la tabla 'poblaciones bloquedas'
					
	*****************************************************************/	
	function add_bloquearPoblacion(pSeleccionada, queSoy){
		// console.log("add_bloquearPoblacion");
		loaderDatos("Añadiendo datos...", queSoy); // img cargando datos
				
		$.ajax({
			async:true, 
			cache:false,
			type: 'GET',
			data: { 
				'subdominio' : datoUsuario[0].subdominio,
				'idUsuario' : datoUsuario[0].idUsuario,
				'pSeleccionada' : pSeleccionada,
				'queSoy' : queSoy
			},
			url: '/assets/io/bloquearP/add_bloquear.php', 
			success: function(response){ 
				var datosP =  $.parseJSON(response);	// console.log(data);
				// console.log("Los datos de "+queSoy+"  bloqueada son los siguientes: "); console.log(datosP);
				
				// Consulta ha dado error o no
				if(datosP[0].numResult == 0){
						console.log(datosP[0].smsResult); // mensaje de error
				}else{
					// Existe o no 
					if(datosP[0].existe == 0){ //No existe, se ha insertado
						
						// añadimos a la tabla que corresponda
						if(queSoy == 'pob'){	
							blockedPob.push(datosP[0]);
							$("#q").val('');  // limpiamos el input text para que pueda seguir introduciendo más poblaciones	
						}else{
							blockedPro.push(datosP[0]);
							$("#q2").val('');  // limpiamos el input text para que pueda seguir introduciendo más provincias	
						}
						append_pbloqueada_table( datosP[0],  'new',  queSoy );
						var smsColor = messageColor_success;
						
					}else{ // Ya existia
						// La poblcion existia en la bd, por lo tanto mensaje de que ya esta insertada
						var smsColor = messageColor_danger;
						loaderDatos_fin(queSoy);					
					}
					

					limpieza(queSoy);
					$("#mensajes_"+queSoy).css("background-color", smsColor);
					$("#mensajes_"+queSoy).html(datosP[0].smsResult);
			
				}
			
			} // fin success
		});	
	}
	
	// Eliminar 1 
	
		/*****************************************************************
	@Descripcion		Eliminamos la poblacion seleccionada
					idSelect , id de la poblacion/provincia a eliminar	
					queSoy, 'pob' problacion /'pro' provincia
	*****************************************************************/		
	//function eliminarP(idSelect,queSoy,posicion){
	function eliminarP(idSelect,queSoy){ // vboy a pasar el array entero
	// console.log("eliminarP");
		//console.log("eliminarP " + idSelect +" , "+queSoy);
		loaderDatos("Eliminando datos...",queSoy); // img cargando datos
		
		$.ajax({
			async:true, 
			cache:false,
			type: 'GET',
			data: { 
				'idSelect' : idSelect,
				'subdominio' : datoUsuario[0].subdominio,
				'idUsuario' : datoUsuario[0].idUsuario,
				'queSoy' : queSoy
			},
			url:  '/assets/io/bloquearP/delete_one.php', 
			success: function(response){
				var datosDelete =  $.parseJSON(response);	
				// console.log("Los datos eliminados son los siguiente:");  console.log(datosDelete);
				
				loaderDatos_fin(queSoy);		
				
				// Consulta ha dado error o no
				if(datosDelete[0].numResult == 0){
						console.log(datosDelete[0].smsResult); // mensaje de error
				}else{
					// Miramos si la ha eliminado o no
					if(datosDelete[0].numRows == 0){ 
						 // No la ha eliminado
						var smsColor = messageColor_danger;					
					}else{
						
						// Si la ha eliminado
						$( "#"+queSoy+"_"+idSelect).remove(); //elimino de la tabla visualmente
						actualizarArray(queSoy, 'delete');
						var smsColor = messageColor_success;
					}
							
					limpieza(queSoy);	
					$("#mensajes_"+queSoy).css("background-color", smsColor);
					$("#mensajes_"+queSoy).html(datosDelete[0].smsResult);
				}
			}// fin success
		});			
	}
	
	/*****************************************************************
	@Descripcion		Eliminamos todas las poblacion 
	@param				opcion, sera 'pob' cuando sea una poblacion o 'pro' cuando sea una provincia				
	*****************************************************************/
	function eliminar_todo(queSoy){
		console.log("eliminar_todo");
		loaderDatos("Eliminando datos...", queSoy); // img cargando datos
		
		$.ajax({
			async:true, 
			cache:false,
			type: 'GET',
			data: { 
				'subdominio' : datoUsuario[0].subdominio,
				'idUsuario' : datoUsuario[0].idUsuario,
				'queSoy' : queSoy
			},
			url:  '/assets/io/bloquearP/delete_all.php', 
			success: function(response){
				loaderDatos_fin();
				var datosDeleteAll =  $.parseJSON(response);					
				// console.log("Los datos eliminados son los siguiente:");	console.log(datosDeleteAll);
				
				if(datosDeleteAll[0].numResult == 0){
						console.log(datosDeleteAll[0].smsResult); // mensaje de error
				}else{
					// Miramos si la ha eliminado o no
					if(datosDeleteAll[0].numRows == 0){
						var smsColor = messageColor_danger;	
					}else{
						var smsColor = messageColor_success;	
						
						//Que soy??
						if(queSoy == 'pob'){
							putArray_all(blockedPob, queSoy);
						}else{
							putArray_all(blockedPro, queSoy);					 
						}
						
						noHayDatos(queSoy); // Esto es para cuando termina de eliminarlo todo
					}
				}

				limpieza(queSoy);
				$("#mensajes_"+queSoy).css("background-colo", messageColor_danger);
				$("#mensajes_"+queSoy).html(datosDeleteAll[0].smsResult);
			}
		});	
	}
	
	

	// Elimino todos los rows de donde toque (pob or pro)
	function putArray_all(nameArray, queSoy){
		console.log("putArray_all");
		// Tendremos que eliminar todos los tr  eso implicara un for		
		for(var i=0; i < nameArray.length ; i++ ){						
			$( "#"+queSoy+"_"+nameArray[i].id ).remove(); // elimino todas las posiciones									
		}	
		nameArray.length=0;		
	}