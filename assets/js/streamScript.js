	/*****************************************************************
	@Descripcion		variables globales					
	
	*****************************************************************/
 		//Variables globales para los acentos 
	var min_a = "\u00e1";
	var min_e = "\u00e9";
	var min_i = "\u00ed";
	var min_o = "\u00f3";
	var min_u = "\u00fa";
	var min_n = "\u00f1";
	
	var may_a = "\u00c1";
	var may_e = "\u00c9";
	var may_i = "\u00cd";
	var may_o = "\u00d3";
	var may_u = "\u00da";
	var may_n = "\u00d1";
	
 	var noHay_pob_txt;
	var cargando_pob_txt ;
	var noHay_pro_txt;
	var noHay_partidos_txt;
	
	// var thPartidos ="<tr> <th colspan='3' id='titulo_proximosPartidos' class='tituloPB'>  Listado de los partidos por hacer:  </th> 	</tr>";
	
	var cargando_pro = "<tr id='cargandoTabla_pro'  class='pblock_tr' > <td colspan='2'  class='pblock_txt pblock_no'> <img src='https://"+location.hostname+"/assets/img/ajaxLoader.gif'> <p id='cargandoTabla_p_pro'> </p>  </td> </tr> ";
	// var d = new Date(); 
	var listaBloqueada_length;
	var simpleColor_info = '#6AA6D6';
	var messageColor_success = '#63CC9E';
	var messageColor_info = '#7BC5D3';
	var messageColor_warning = '#DFD271 ';
	var messageColor_danger = '#D15E5E';
	var datoUsuario;
	var datos_partidos; 
	var noRecargar_tab_bloqueo = 0;
	var noRecargar_tab_proximoPartido = 0;
	
	var hostname =  location.hostname;
	var pathname = location.pathname;
	var serverName = hostname.split('.');
	var subdominio = serverName[0];
	var dominio = serverName[1];
	var textos =  new Array(); 		
	
	var noHay_pob; 	
	var cargando_pob;
	// provincia 
	var noHay_pro ; 		
	// proximos partidos
	var noHay_partidos ;	
	var thPartidos;	
	var cargando_pro;
		
	$(document).ready(function() { 
	
	recogerImagenPortada();
		console.log(dominio);
		// Segun el dominio en el que estemos sera un texto u otro.
		switch(dominio){
			case 'streamevents':
			$("#tab_bloqueoIp a").text("Bloquear Poblaci"+min_o+"n");
			$("#tab_proximoPartido a").text("Pr"+min_o+"ximo Evento");
			$("#tab_configurarDatos a").text("Configurar datos");
			$("#tab_estadisticasRango a").text("Estad"+min_i+"stcas");
			// $("#tab_configurarDatos a").text("Configurar Datos");
			$("#cont_equipo1").css("display", "none");
			$("#cont_equipo2").css("display", "none");
			$("#cont_descripcionEvento").css("display", "block");
			
			
				textos = {
					// bloquear  poblaciones
					bloqueacionPoblacion_PHP:{},
					
					// proximoPartido.php
					proximoPartido_PHP:{ // nombre del archivo php al que hacemos referencia
						thPartidos: "Listado de los eventos por hacer: ",
						introTitulo_proximo:"Programaci"+min_o+"n pr"+min_o+"ximos eventos",
						txt01_proximo:"Para programar un 'pr"+min_o+"ximo evento' debes rellenar todos los campos y pulsar el bot"+min_o+"n 'A"+min_n+"adir evento'.",
						txt02_proximo:"Esta informaci"+min_o+"n es para que tus seguidores sepa cuando se emitira el pr"+min_o+"ximo evento.",
					
						tituloPartido: {
							lbl_html:"T"+min_i+"tulo evento:",
							lbl_title:"T"+min_i+"tulo evento",
							title:"Introduzca un t"+min_i+"tulo de evento",
							placeholder:"Introduce el tipo de evento, por ejemplo 'Fiesta cumplea"+min_n+"os 25'",
						}, 
						
						descripcionEvento: {
							lbl_html: "Descripci"+min_o+"n del evento:", 
							lbl_title: "Descripci"+min_o+"n del evento", 
							title:"Introduce una descripci"+min_o+"n para el evento.",
							placeholder:"Introduzca  una descripci"+min_o+"n para el evento",
						},
						
						// titulo_proximosPartidos:{},	
						
						calendario: {
							lbl_html: "Selecciona la fecha/hora del evento:",
							lbl_title: "",
							
							open_html: "Abrir",
							open_title: "Abrir calendario",
					
							close_html: "Cerrar ",
							close_title: "Cerrar calendario",
					
							reset_html: "Reset",
							reset_title: "Reiniciar calendario",
						},
				
						addPartido: {
							html: "A"+min_n+"adir Evento",
							title: "A"+min_n+"adir Pr"+min_o+"ximo Evento",
						},
						
						editPartido: {
							html: "Guardar Cambios",
							title: "Guardar los cambios realizados",		
						},
					},
					
					configuracion_PHP:{ // nombre del archivo php al que hacemos referencia
						noHay_pob: "No hay poblaciones bloqueadas", 
						noHay_partidos: "No hay eventos programados ", 		
						noHay_pro:"No hay provincias bloqueadas",
						
						tab_bloqueoIp: {
							html: "Bloqueo por geolocalizaci"+min_o+"n",
							title: "Pulsa para acceder al apartado de bloqueo por geolocalizaci"+min_o+"n",		
						},
						
						tab_proximoPartido: {
							html: "Pr"+min_o+"ximos Eventos",
							title: "Pulsa para acceder al apartado de pr"+min_o+"ximos eventos",		
						},
					},
					
					video_PHP:{
						eliminarP:{
							 title:"Eliminar evento",
						},
					},		
					
					videos_PHP:{
						eliminarP:{
							 title:"Eliminar evento", 
						},
						
						bestJugadas:{
							title:"Pulsa para ver los mejores momentos del evento",
						}
					},
				
					social_buttons_PHP:{
						compartirFB:{
							 title:"Comparte el evento en tu facebook", 
						},
						
						compartirTW:{
							 title:"Comparte el evento en tu twitter", 
						},
					
						descargarResumen:{
							 title:"Descargar resumen del evento", 
						},
						
						verEstadisticas:{
							 title:"Ver estad"+min_i+"sticas de la emisi"+min_o+"n",
						},
				
					}
				};	
				break;
			
			case 'streamsports':
				$("#tab_bloqueoIp a").text("Bloquear Poblaci"+min_o+"n");
				$("#tab_proximoPartido a").text("Pr"+min_o+"ximo Partido");
				$("#tab_configurarDatos a").text("Configurar datos");
				$("#tab_estadisticasRango a").text("Estad"+min_i+"stcas");
					// $("#tab_configurarDatos a").text("Configurar Datos");
				$("#cont_equipo1").css("display", "block");
				$("#cont_equipo2").css("display", "block");
				$("#cont_descripcionEvento").css("display", "none");
			
				textos = {
					// bloquear  poblaciones
					bloqueacionPoblacion_PHP:{
						
					},
					
					// proximoPartido.php
					proximoPartido_PHP:{ // nombre del archivo php al que hacemos referencia
						thPartidos: "Listado de los partidos por hacer: ",
					
						introTitulo_proximo:"Programaci"+min_o+"n pr"+min_o+"ximos partidos",
						txt01_proximo:"Para programar un 'pr"+min_o+"ximo partido' debes rellenar todos los campos y pulsar el bot"+min_o+"n 'A"+min_n+"adir partido'.",
						txt02_proximo:"Esta informaci"+min_o+"n es para que tu afici"+min_o+"n sepa cuando se emitir"+min_a+" el pr"+min_o+"ximo partido.",
					
						tituloPartido: {
							lbl_html:"T"+min_i+"tulo partido:",
							lbl_title:"T"+min_i+"tulo partido",
							title:"Introduzca un t"+min_i+"tulo de partido",
							placeholder:"Introduce el tipo de partido, por ejemplo 'Copa del rey'",
						}, 
						
						equipo1: {
							lbl_html: "Equipo Local:", 
							lbl_title: "Equipo Local", 
							title:"Introduce el nombre del equipo local.",
							placeholder:"Introduzca el nombre del equipo local",
						},
						
						equipo2: {
							lbl_html: "Equipo Visitante:", 
							lbl_title: "Equipo Visitante", 
							title:"Introduce el nombre del equipo visitante.",
							placeholder:"Introduzca el nombre del equipo visitante",
						},
					
						// titulo_proximosPartidos:{},
					
						calendario: {
							lbl_html: "Selecciona la fecha/hora del partido:",
							lbl_title: "",
							
							open_html: "Abrir",
							open_title: "Abrir calendario",
					
							close_html: "Cerrar ",
							close_title: "Cerrar calendario",
					
							reset_html: "Reset",
							reset_title: "Reiniciar calendario",
						},
				
						addPartido: {
							html: "A"+min_n+"adir Partido",
							title: "A"+min_n+"adir Pr"+min_o+"ximo Partido",
						},
						
						editPartido: {
							html: "Guardar Cambios",
							title: "Guardar los cambios realizados",		
						},
					},
					
					configuracion_PHP:{ // nombre del archivo php al que hacemos referencia
						noHay_pob: "No hay poblaciones bloqueadas", 
						noHay_partidos: "No hay partidos programados ", 		
						noHay_pro:"No hay provincias bloqueadas",
						
						tab_bloqueoIp: {
							html: "Bloqueo por geolocalizaci"+min_o+"n",
							title: "Pulsa para acceder al apartado de bloqueo por geolocalizaci"+min_o+"n",		
						},
						
						tab_proximoPartido: {
							html: "Pr"+min_o+"ximos partidos",
							title: "Pulsa para acceder al apartado de pr"+min_o+"ximos partidos",		
						},
					},
					
					video_PHP:{
						eliminarP:{
							 title:"Eliminar partido",
						},
					},		
					
					videos_PHP:{
						eliminarP:{
							 title:"Eliminar partido", 
						},
						
						bestJugadas:{
							title:"Pulsa para ver las mejores jugadas del partido",
						}
					},
				
					social_buttons_PHP:{
						compartirFB:{
							 title:"Comparte el partido en tu facebook", 
						},
						
						compartirTW:{
							 title:"Comparte el partido en tu twitter", 
						},
					
						descargarResumen:{
							 title:"Descargar resumen del partido", 
						},
						
						verEstadisticas:{
							 title:"Ver estad"+min_i+"sticas de la emisi"+min_o+"n",
						},
				
					}
				};	
				break;
		}	// fin switch
		
		
		
		loader_textos(pathname);
	});
	
	/***********************************************************************************************
	@Descripcion	A�adimos los textos donde esten visible
						
	***********************************************************************************************/	
	function loader_textos(pathname){
		
		if(pathname == '/configuracion'){
			loader_textos_bloquearPoblacion();
			

			var configuracionTXT = textos.configuracion_PHP;
			
			
			// configuracion_PHP
			isVisible('#bloqueoIp', configuracionTXT.tab_bloqueoIp.html, configuracionTXT.tab_bloqueoIp.title,	'input');
			isVisible('#proximoPartido', configuracionTXT.tab_proximoPartido.html, configuracionTXT.tab_proximoPartido.title, 'input');
					
		
		}
		else{
					
			// video_PHP
			var videoTXT = textos.video_PHP;	
			isVisible('#eliminarP', 	videoTXT.eliminarP.title,'' , 'a');
		
			// videos_php
			var videosTXT = textos.videos_PHP;	
			isVisible('.eliminarP', 	videosTXT.eliminarP.title, '' , 'a');
			isVisible('.bestJugadas', 	videosTXT.bestJugadas.title, '' , 'a');
		
		
			// Redes sociales
			var socialButtonsTXT = textos.social_buttons_PHP;	
			isVisible('.compartirFB', 	socialButtonsTXT.compartirFB.title, '' , 'a');
			isVisible('.compartirTW', 	socialButtonsTXT.compartirTW.title, '' , 'a');
			isVisible('.descargarResumen', 	socialButtonsTXT.descargarResumen.title, '' , 'a');
			isVisible('.verEstadisticas', 	socialButtonsTXT.verEstadisticas.title, '' , 'a');
		}
	}
	
	
	function loader_textos_proximoPartido(){
			var proximoPartidoTXT = textos.proximoPartido_PHP;
		// proximoPartido.php
			isVisible('#introTitulo_proximo' , proximoPartidoTXT.thPartidos, '' , 'text');
			isVisible('#introTitulo_proximo' , proximoPartidoTXT.introTitulo_proximo, '' , 'text');
			isVisible('#txt01_proximo' , proximoPartidoTXT.txt01_proximo, '', 'text');
			isVisible('#txt02_proximo' , proximoPartidoTXT.txt02_proximo,'',  'text');	
			
			isVisible('#tituloPartidoLBL', proximoPartidoTXT.tituloPartido.lbl_html,  proximoPartidoTXT.tituloPartido.lbl_title, 'etiqueta');			
			isVisible('#tituloPartido', proximoPartidoTXT.tituloPartido.placeholder, proximoPartidoTXT.tituloPartido.title, 'input');
			
			if(dominio == 'streamsports'){
				isVisible('#equipo1LBL', proximoPartidoTXT.equipo1.lbl_html, proximoPartidoTXT.equipo1.lbl_title, 'etiqueta');
				isVisible('#equipo1', proximoPartidoTXT.equipo1.placeholder, proximoPartidoTXT.equipo1.title, 'input');		
				
				isVisible('#equipo2LBL', proximoPartidoTXT.equipo2.lbl_html, proximoPartidoTXT.equipo2.lbl_title, 'etiqueta');		
				isVisible('#equipo2',	proximoPartidoTXT.equipo2.placeholder, proximoPartidoTXT.equipo2.title, 'input');	
			}else{		
				isVisible('#descripcionEventoLBL', proximoPartidoTXT.descripcionEvento.lbl_html, proximoPartidoTXT.descripcionEvento.lbl_title, 'etiqueta');		
				isVisible('#descripcionEvento',	proximoPartidoTXT.descripcionEvento.placeholder, proximoPartidoTXT.descripcionEvento.title, 'input');
			}
			
			isVisible('#fechaLBL', proximoPartidoTXT.calendario.lbl_html, proximoPartidoTXT.calendario.lbl_title, 'etiqueta');		
			isVisible('#open', proximoPartidoTXT.calendario.open_html,  proximoPartidoTXT.calendario.open_title, 'boton');
			isVisible('#close' , proximoPartidoTXT.calendario.close_html, proximoPartidoTXT.calendario.close_title, 'boton');		
			isVisible('#reset', proximoPartidoTXT.calendario.reset_html, 	proximoPartidoTXT.calendario.reset_title, 'boton');
			
			isVisible('#addPartido',	proximoPartidoTXT.addPartido.html, proximoPartidoTXT.addPartido.title, 'boton');		
			isVisible('#editPartido',  proximoPartidoTXT.editPartido.html, proximoPartidoTXT.editPartido.title, 'boton');	
		
				
			// proximos partidos
			noHay_partidos = "<tr id='tr_noHay_partido'  class='pblock_tr'  > <td colspan='3'  class='pblock_txt pblock_no'> "+ textos.configuracion_PHP.noHay_partidos +" </td> </tr> " ;
			thPartidos ="<tr> <th colspan='3' id='tituloPB_pob2' class='tituloPB'> "+ textos.proximoPartido_PHP.thPartidos +" </th> 	</tr>";	
			// alert(textos.proximoPartido_PHP.thPartidos);
	}
	
	
	function loader_textos_bloquearPoblacion(){
		// poblacion
		noHay_pob = "<tr id='tr_noHay_pob'  class='pblock_tr'  > <td colspan='2'  class='pblock_txt pblock_no'> " + textos.configuracion_PHP.noHay_pob +" </td> </tr> "; 	
		cargando_pob = "<tr id='cargandoTabla_pob'  class='pblock_tr' > <td colspan='2'  class='pblock_txt pblock_no'> <img src='https://"+ hostname +"/assets/img/ajaxLoader.gif'> <p id='cargandoTabla_p_pob'> </p>  </td> </tr> ";
		
		// provincia 
			noHay_pro = "<tr id='tr_noHay_pro'  class='pblock_tr'  > <td colspan='2'  class='pblock_txt pblock_no'>"+ textos.configuracion_PHP.noHay_pro +"</td> </tr> "; 			
			cargando_pro = "<tr id='cargandoTabla_pro'  class='pblock_tr' > <td colspan='2'  class='pblock_txt pblock_no'> <img src='https://"+ hostname +"/assets/img/ajaxLoader.gif'> <p id='cargandoTabla_p_pro'> </p>  </td> </tr> ";
		
		var bloqueacionPoblacionTXT = textos.bloqueacionPoblacion_PHP;
		/*
		Hay que implementarla
		*/
		
	}
	
	function isVisible(nomDiv, textDiv, textDiv2,  opcion ){
		 // console.log("isVisible(" + nomDiv + " , " + textDiv + " , " + textDiv2 + " , " + opcion+ ")");
		 
		if(textDiv2 == "") textDiv2 = textDiv;
	
		if ($(nomDiv).is(":visible")){
			
			switch(opcion){
				case 'text': 
					$(nomDiv).html(textDiv); 		
					break;
				
				case 'input':
					$(nomDiv).attr({
						placeholder: textDiv,
						'data-original-title': textDiv2
					});				
					break;
				
				case 'boton': 
					$(nomDiv)
						.attr("title", textDiv2)
						.html(textDiv)					
					break;
				
				
				case 'etiqueta': 
					$(nomDiv)
						.attr("title", textDiv2)
						.html(textDiv)						
					break;
			
				case 'a': 				
					$(nomDiv).attr("title", textDiv2);					
					break;
				
				
				case 'selectOption': 
					break;
					
				
				
			} // fin switch
		} // fin if		
	} // fin funcion
	
	
	// tiempo de desfase
	function desfaseHoraria (){
		var desfase_aux = new Date().getTimezoneOffset();
		
		// si es un numero negativo se suma, si es un numero positivo se resta
		if(desfase_aux < 0 ) {
			return desfase = Math.abs(desfase_aux);		  
		}else if(desfase_aux > 0 ){
			return desfase = -desfase_aux;
		}else{
			return desfase = 0;
		}
	}
	
		/***********************************************************************************************
	@Descripcion	Cambiamos el formato de la fecha para guardarlo en BBDD
	@param			Fecha a modificar
	@Nota			Faltaria comprobar que la fecha es mayor igual a la acutual
	***********************************************************************************************/	
	function convertir_fechaHora(param){
			
		var dateString = param,
		dateParts = dateString.split(' '),
		timeParts = dateParts[1].split(':'),
		date;   
		dateParts = dateParts[0].split('-');
			
		// Este es el formato que tiene en la bbdd
		return dateParts[2]+"-"+dateParts[1]+"-"+dateParts[0]+" "+timeParts[0]+":"+timeParts[1]+":"+timeParts[2];
		
	}
	
	
	/*
	 Sacamos la imagen de la portada
	*/
		
	function recogerImagenPortada(){
		
		$.ajax({
			async:true, 
			cache:false,
			type: 'GET',
			//data: {},
			url: '/assets/io/recogerImgPortada.php', 
			success: function(response){
				console.log("response: " + response);
				if(response){
					var urlSrc = response;
				}
				else{
					//var urlSrc = "../img/logoDefault.jpg";					
					var urlSrc = "http://test.streamsports.es:8080/assets/img/logoDefault.jpg";					
				}
				console.log("urlSrc: " + urlSrc);
				$("#imgPortadaActual").html("<img src='"+urlSrc+"'  height='200' width='200' >");	
				$('#header').css('background-image', 'url(' + urlSrc + ')');
			
			
			} // fin success					
		});	
	
	}
	
	
	// Falta poner el filtro de si tipoGrabacion != 10 no se pueda descargar.
	function download_video(idGrabacion){	
		$.ajax({
			async:true, 
            cache:false,
			type: 'GET',
			url: '/assets/io/descargarPartidoCompleto.php',
			data: { 'idGrabacion' : idGrabacion },
			success: function(response){
				var datos = $.parseJSON(response);			
				
				// Activo la grabacion
				window.open('/assets/io/descargarPartidoCompleto2.php?idUsuario='+datos[0].idUsuario+'&archivo='+datos[0].archivo+'&dominio='+datos[0].dominio,  '_blank');			
			} // fin success					
		});		
	}
	