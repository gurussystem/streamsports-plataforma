<?php


//$client = new Client();
$dades = array();




function estadisticas($dominio, $idSeleccionado){
	$estado = array();
	$usuarios = array();
	$contador = 0;
	
	// echo "<br> (sacarArrayUsuario) dominio: " . $dominio . ", idSeleccionado: " . $idSeleccionado;
	
	# Conectamos con la BD
	 include 'conexion.php';
	 
	##############################################
	# Datos de los usuarios que se han conectado - SQL - array
	##############################################
	# Ejecutamos la consulta 
	if($dominio == 'streamsports')
		$sql = " call stats_streamsports(?)";
	else
		$sql = " call stats_streamevents(?)";
	
	$stmt = $mysqli->prepare($sql);
	$stmt->bind_param("i",  $idSeleccionado);	
	$stmt->execute(); 
	$result = $stmt->get_result();		

	# Si no hay datos paramos.
	if($result->num_rows <= 0){
		// $estado = array('proceso' => 0, 'mensaje' => 'No hay datos para hacer estadisticas.'); 
		//return json_encode($dades);
		//die();
	}else{
		// $estado = array('proceso' => 1, 'mensaje' => 'Si hay datos para hacer estadisticas.'); 
	
		# Evaluamos si ha ido todo bien o habido algun fallo
		if($stmt->errno){
			echo "<br> error: ". $stmt->errno;
		}	
		
		# Usuarios conectados 
		$sumaDuracionConexion  = 0;
		while($row=$result->fetch_assoc()) {
			//echo '<pre>'; print_r($row); echo '</pre>';  
				
			# Sumamos el total de la duracion de las conexiones por usuario
			$sumaDuracionConexion = $sumaDuracionConexion + $row["duracionConexion"];
			
			# rellenamos el array que utilizaremos en este php
			
			$usuarios[] = array(
				"idConexion"=>$row["idConexion"], 
				"idUser"=>$row["idUser"],  
				"idUserSession"=>$row["idUserSession"], 
				"hostUser"=>$row["hostUser"], 
				"titulo"=>$row["titulo"],	
				"horaPartido_inicio"=>$row["horaPartido_inicio"],
				"horaPartido_fin"=>$row["horaPartido_fin"], 	
				"fechaPartido"=>$row["fechaPartido"],
				"fechaPartidoI"=>$row["fechaPartidoI"],
				"fechaTitulo"=>$row["fechaTitulo"],
				"fechaHoraConexion_inicio"=>$row["fechaHoraConexion_inicio"], 
				"fechaHoraConexion_fin"=>$row["fechaHoraConexion_fin"], 
				"duracionConexion"=>$row["duracionConexion"], 
				"duracionPartido"=>$row["duracionPartido"], 
				"horaConexion_inicio"=>$row["horaConexion_inicio"], 
				"horaConexion_fin"=>$row["horaConexion_fin"], 
				"fechaConexion"=>$row["fechaConexion"] ,	
				"cont_pc"=>$row["cont_pc"], 	
				"espectadores"=>$row["espectadores"] ,				
				"ip"=>$row['ip'],
				"eLocal"=>$row["eLocal"],	
				"eVisitante"=>$row["eVisitante"],
				"numConexionesTotales"=>$result->num_rows,
				"sumaDuracionConexion"=>$sumaDuracionConexion  // El ultimo registro del array tiene el total
				
			);
			
			//	echo '<br> usuarios <pre>'; print_r($usuarios); echo '</pre>';
		}  #fin while
		$stmt->close();
		$dades['usuarios'] =  $usuarios;
		$dades['titulo'] = $usuarios[0]['titulo']; // esto borrar
		$dades['eLocal'] =  $usuarios[0]['eLocal']; // esto borrar
		$dades['eVisitante'] =  $usuarios[0]['eVisitante']; // esto borrar
		$dades['espectadores'] = $usuarios[0]["espectadores"]; // esto borrar
		$numConexionesTotales = $usuarios[0]["numConexionesTotales"];
		
		##############################################
		# Calculo de los porcentajes para el rosco 	
		##############################################		
		$numEspectadores= $usuarios[0]["espectadores"];		
		$cont_pc = $usuarios[0]["cont_pc"];
		$con_pc_porcentaje = round( ( $cont_pc  * 100 ) / $numEspectadores);	
		if ( $con_pc_porcentaje == 100 ) {
			$con_pc_porcentaje = 99;
			$con_app_porcentaje=01;
		}else{
			$con_app_porcentaje = round( ( ($numEspectadores - $cont_pc)  * 100 ) / $numEspectadores);
		}
		// echo "<br> porcentaje pc: " . $con_pc_porcentaje . " <br> porcentaje app: " . $con_app_porcentaje; 
		
		# Asignamos al array que devolvemos 
		$dades['numOrdenadores'] = $con_pc_porcentaje ;
		$dades['numMoviles'] =  $con_app_porcentaje;
		
		
		##############################################
		# Traduciendo fecha del titulo		
		##############################################		
		$fechaTituloParts = explode(" ",$usuarios[0]["fechaTitulo"]); //0 dia //2 mes
		// echo '<pre>'; print_r($fechaTituloParts); echo '</pre>'; 
		
		$fechaTitulo_dia = diaSemana_fecha($fechaTituloParts[0]);
		$fechaTitulo_mes = mes_fecha($fechaTituloParts[2]);
		$fechaTitulo = $fechaTitulo_dia . " " .$fechaTituloParts[1] . " de " . $fechaTitulo_mes . " " . $fechaTituloParts[3]; 
		// echo "<br> fechaTitulo: ". $fechaTitulo;
		$dades['fechaTitulo'] = $fechaTitulo;
		
		##################################################
		# Duracion partido (sacamos el numero de minutos que ha durado el partido)
		##################################################
		$duracionPartido = round($usuarios[0]["duracionPartido"]/60);
		$dades['minutosTotal'] =  $duracionPartido ;
		// echo "<br>duracion partido mint: " . $duracionPartido . "minutos";
		
		##################################################
		# Array duración del partido
		##################################################
		// echo "<br> hora inicio partido: " . $usuarios[0]["horaPartido_inicio"];
		$tiempoParts = explode(":",$usuarios[0]["horaPartido_inicio"]);
				
		$horas = $tiempoParts[0] ;
		$minutos = $tiempoParts[1];
		$segundos = $tiempoParts[2];
		$contadorMinutos = $minutos;
		// echo "<pre>"; print_r($tiempoParts); echo "</pre>";
		//echo "<br>h:m:s ".  $horas . " : " . $minutos . " : " .  $segundos . " contador ( ". $contadorMinutos . ")";
		# Sacamos el array de los tiempos por minutos 18:20, 18:21....
		for($h=0; $h<=$duracionPartido; $h++){
		/*	if($h!=0){
				if ($minutos <= 59){
					$horas = $horas ;
						
					if($minutos != 59) {					
						if($minutos < 9)
							$minutos = '0'.($minutos+1);
						else
							$minutos = $minutos+1;
						
					}else{	
						$horas = $horas+1 ; 
						$minutos = '00';
					}				
				}	// fin if 'minutos <= 59'
			} // fin if
					
			//$varTiempo = $horas.":".$minutos;
			$tiemposPartido[$h] = $varTiempo; 
			*/
			// $tiemposPartido_comparar[$h] = $usuarios[0]["fechaPartido"]. " ".$horas.":".$minutos. ":00";
			$tiemposPartido_comparar[$h] = strtotime($usuarios[0]["fechaPartidoI"])+60*$h;
			//echo "<br> varTiempo : " .$varTiempo ;
	//		echo "<br>antes- fechaPartidoI : " .$usuarios[0]["fechaPartidoI"] ;
		//	echo "<br> tiemposPartido_comparar : " .$tiemposPartido_comparar[$h];
		
		# Creamos el array de tiempo de la zona europea para mostrar por pantalla
			//date_default_timezone_set("Europe/Madrid");
			
			// al tiempo que saco de la bd le sumo la zona horaria 
			// $local =date('r', strtotime($tiemposPartido_comparar[$h]) + date('Z', strtotime($tiemposPartido_comparar[$h])) );
			//$local =date('r', $tiemposPartido_comparar[$h] + date('Z', $tiemposPartido_comparar[$h]) );
			$local =date('r', $tiemposPartido_comparar[$h] );
			//echo "<br> local_: " . $local;
			$localParts = explode(" ",$local);
			// echo '<pre>'; print_r($localParts); echo '</pre>'; 
			
			# Solo quiero la hora y el minutos
			$localParts2 = explode(":",$localParts[4]);
			$tiemposPartido_utc[$h] = $localParts2[0].":".$localParts2[1];				
			$contadorMinutos = $contadorMinutos + 1;			
		} //fin for
		$dades['minPartido'] = $tiemposPartido_utc;	

		// echo '<pre>'; print_r($tiemposPartido); echo '</pre>'; 
		// echo '<pre>'; print_r($tiemposPartido_comparar); echo '</pre>'; 
		// echo '<pre>'; print_r($tiemposPartido_utc); echo '</pre>'; 
		

		##################################################
		# Comparamos tiempo con usuarios , en cada tiempo cuantos usurios se han conectado????
		#Tiempo
		##################################################
		for($t=0; $t<=$duracionPartido; $t++){
			// echo "<br> t =" . $t . "<br>";
			//if($t >=0 &&  $t <=50){ // Borrar condicion luego
				$tiempoP = $tiemposPartido_comparar[$t]; // tiempo partido x minutos
					
				
						//echo "<br> tiempoP: ". $tiempoP;
				for($u=0; $u<=$numConexionesTotales-1; $u++){
					// echo "<br> u =" . $u . "<br>";
					$fechaHoraConexion_inicio = strtotime($usuarios[$u]["fechaHoraConexion_inicio"]);
					//echo "<br> fechaHoraConexion_inicio: ". $fechaHoraConexion_inicio;
					$fechaHoraConexion_fin = strtotime($usuarios[$u]["fechaHoraConexion_fin"]);				
					//echo "<br> fechaHoraConexion_fin: ". $fechaHoraConexion_fin;
					if( ($fechaHoraConexion_inicio < $tiempoP) && ($tiempoP < $fechaHoraConexion_fin) ){
						
						$contador  =  $contador + 1; 	
					}//fin if	
				} // fin for u
				
				//	if(empty($contador)){$contador = 0;}			
				$contadorUsuarios[] = $contador;
				$contador = 0;
			//}
	}//fin for t
	$dades['estado'] = $estado;
		$dades['contUsuarios'] = $contadorUsuarios;
		//	echo '<pre> users'; print_r($contadorUsuarios); echo '</pre>'; 
		
	##################################################	
	# Calculo de Tiempo medio de visualizacion ( suma duracion  / espectadores)
	##################################################
	$mediaVisualizacion = $sumaDuracionConexion /  $numEspectadores ;
	$dades['tmpMediaVisualizaion'] = gmdate("i:s", $mediaVisualizacion);
	
	
	##################################################
	# Calculo de viewersHours = Total espectadores * tiempo medio de visualizacion
	##################################################
	$viewersHours = (($mediaVisualizacion * $numEspectadores)/(60*60)) ;
	$dades['viewersHours'] =round($viewersHours,2);
	
	################################################### 
	# Ciudades
	# Consultamos primero en la bd 'stats_ciudades' sino estan los datos consultamos al servicio 
	##################################################  
	$ciudades = array();
	$posicionCiudad=0;
	$contandoConexionesMarisa =0;
	$a = 0;
	for($j = 0; $j<= count($usuarios) - 1; $j++){
		// echo '<br> '.$j .' - '.$usuarios[$j]['ip'];
		$stmt = $mysqli->prepare("select * from   configstream.stats_ciudades where  ip = ?");
		$stmt->bind_param("s", $usuarios[$j]['ip']);
		$stmt->execute();
		$result2 = $stmt->get_result();	
		// echo "<br> conexion: " .  $result2->num_rows;

		if($result2->num_rows != 0){
			while($row2=$result2->fetch_assoc()) { 
				$cadena = $row2['ciudad'];
				$usuarios[$j]['ciudad'] =   $row2['ciudad']; // utf8_encode($row2['ciudad']);
			}
			$stmt->close();
			
		}else{	
			$stmt->close();
			// echo "<br> conexion cero";
			$ip = $usuarios[$j]['ip'];
			// echo "<br> conexion cero $ip";
			$datosMaxMind = recogerDatosMaxMind($ip);
			// echo "<pre>"; print_r($datosMaxMind); echo "</pre>";
			# Una vez tengo el array  hacemos el insert
			// echo "<br>encontrado: ".$datosMaxMind['enontrado'];
				
			if($datosMaxMind['enontrado'] == 1){
				# Comprobar que existe el codigo postal
				 $stmt = $mysqli->prepare(" SELECT	pob.poblacion,     pro.provincia,    c.comunidad FROM 
					configstream.poblacion as pob
					inner join configstream.provincia as pro on (pob.idprovincia = pro.idprovincia) 
					inner join configstream.comunidad as c on (c.idcomunidad = pro.idcomunidad) where pob.postal = ? ");
				$stmt->bind_param("i", $datosMaxMind['cp']);
				$stmt->execute();
				$result3 = $stmt->get_result();	
				$row3=$result3->fetch_assoc();
					
				
				if(empty($datosMaxMind['ciudad']))
					$datosMaxMind['ciudad'] = $row3['poblacion']; //utf8_encode($row3['poblacion']);
					
				if(empty($datosMaxMind['prov']))
					$datosMaxMind['prov'] = $row3['provincia']; // utf8_encode($row3['provincia']);
					
				if(empty($datosMaxMind['comunidad_autonoma']))
					$datosMaxMind['comunidad_autonoma'] = $row3['comunidad_autonoma']; // utf8_encode($row3['comunidad_autonoma']);
						
			} // fin encontrado = 1; 
			else{
				$datosMaxMind['ciudad'] = "Resto";
				$datosMaxMind['prov'] = "Resto";
				$datosMaxMind['cp'] = 00000;
				$datosMaxMind['comunidad_autonoma'] = "Resto";
				$datosMaxMind['pais_code'] = "Resto";
				$datosMaxMind['pais'] = "Resto";
				$datosMaxMind['continente_code'] = "Resto";
				$datosMaxMind['continente'] = "Resto";
				$datosMaxMind['lat'] = 0;
				$datosMaxMind['lng'] = 0;
				$datosMaxMind['ip'] = $ip;
			}
			//$stmt->close();
		
			$usuarios[$j]['ciudad'] =  $datosMaxMind['ciudad'];
			try{
				$stmt = $mysqli->prepare("INSERT INTO configstream.stats_ciudades(provincia, ciudad, comunidad_autonoma, pais_code, pais, continente_code, continente, codigo_postal, lat, lng, ip) VALUES (?, ?, ?, ? , ?, ?, ?, ?, ?, ?, ?)");			
			}catch (mysqli_sql_exception $e) { 
				echo $e->getMessage(); 
				echo "<br>den error: ".  $mysqli->error;	
			} 

			$stmt->bind_param("sssssssssss",$datosMaxMind['prov'], $datosMaxMind['ciudad'] , $datosMaxMind['comunidad_autonoma'], $datosMaxMind['pais_code']  , $datosMaxMind['pais'] , $datosMaxMind['continente_code']  , $datosMaxMind['continente'] , $datosMaxMind['cp'], $datosMaxMind['lat'] , $datosMaxMind['lng'], $datosMaxMind['ip']);
			$stmt->execute();
			$stmt->close();
		}//else max mind
					
		
		# Controlamos por userId de la conexion, como contamos los espectadores
		if( !isset($idUsuariosConexion[$usuarios[$j]['idUserSession']])  ){	 // userId 	
			//echo "<br> !isset if " . $j ;
			$contandoConexionesMarisa ++;
					
			$idUsuariosConexion[$usuarios[$j]['idUserSession']] = 1;
			$idUsuariosConexion[$j]['posicion'] = $a;
					
			if( !empty($usuarios[$j]['ciudad']) ) // sino esta vacio su nombre 
						
				$idUsuariosConexion[$j]["ciudad"] = $usuarios[$j]['ciudad'];		
			else
				$idUsuariosConexion[$j]["ciudad"] = 'Resto';						
			
			//echo "<br> ciudad: ". $idUsuariosConexion[$j]['ciudad'];		
			# Sumamos para pasar a la siguiente ciudad
			$a++;
		}
		else{
			//echo "<br> !isset else" . $j ;
			$idUsuariosConexion[$usuarios[$j]['idUserSession']]  ++;	
			$idUsuariosConexion[$j]['posicion'] = $a;
					$idUsuariosConexion[$j]["ciudad"] = 'Nula';
		}			
	}//for
	//echo '<pre> '; print_r($idUsuariosConexion ); echo '</pre>';  
		
	# Hago el recuento de ciudades con el array idUsuariosConexion
	for($m = 0; $m<= count($usuarios) - 1;  $m++){
		if($idUsuariosConexion[$m]["ciudad"] != 'Nula'){
			if( !isset($ciudades[$idUsuariosConexion[$m]["ciudad"]]) )				
				$ciudades[$idUsuariosConexion[$m]["ciudad"]] = 1;
			else
				$ciudades[$idUsuariosConexion[$m]["ciudad"]]  ++;
		}
	}
	$dades['espectadores_aux'] = $contandoConexionesMarisa;
	$dades['listaCiudades'] = $ciudades; 
		
	//	echo "contandoConexionesMarisa: " . $contandoConexionesMarisa;
	//echo '<pre>'; print_r($idUsuariosConexion); echo '</pre>';  
	//	echo '<pre> ciudad: '; print_r($ciudades ); echo '</pre>';  
		
	
	##################################################
	# DEVOLUCION DE ARRAY
	##################################################	
	return $dades;
	//echo '<br> dades <pre>'; print_r($dades); echo '</pre>';
	// $mysqli->close();
	}
}



function  selectArrayFechas($dominio, $idUsuario,$fechaI, $fechaF ){
	#	echo "<br> selectArrayFechas <br>";
	
	# Conectamos con la BD
	include 'conexion.php';
	$datesArray  = array();
		
	# saco array con las fechas
	if($dominio == 'streamsports')
		$sql = "SELECT distinct DATE_FORMAT(fechaIni,'%Y-%m-%d')  as fecha FROM streamsports.partidos WHERE idUsuario = ? AND  fechaIni BETWEEN ? AND ? ORDER  BY fechaIni ASC ";
	else
		$sql = "SELECT distinct DATE_FORMAT(fechaIni,'%Y-%m-%d')  as fecha FROM streamevents.eventos WHERE idUsuario = ? AND  fechaIni BETWEEN ? AND ? ORDER  BY fechaIni ASC   ";
	
	# Ejecutamos la consulta 
	$stmt = $mysqli->prepare($sql); 
	$stmt->bind_param("iss",  $idUsuario,$fechaI, $fechaF);
	$stmt->execute(); 
	$result = $stmt->get_result();	
	
	if($result){
		while($row=$result->fetch_assoc()) {
			# echo '<pre>'; print_r($row); echo '</pre>';  			
			
			# Separa el dia el mes y el año porque la libreria de graficos del google me lo exige.
			$fechaPorPartes = explode("-",$row["fecha"]); //0 dia //2 mes
			$dia=$fechaPorPartes[2];
			$mes=$fechaPorPartes[1];
			$year=$fechaPorPartes[0];
			
		
			$datesArray[] = array(
				"fechaEntera"=>$row['fecha'],
				"year"=>$year,				
				"mes"=>$mes,				
				"dia"=>$dia,				
				"proceso"=>1,				
			);
		
		
		} # fin while
	}
	else{
		$datesArray[] = array(
			"proceso"=>0,				
			"smsResult"=>"No se han encontrado fechas"				
		);
	}
	$stmt->close();
	$mysqli->close();
	# echo '<pre>'; print_r($datesArray); echo '</pre>';  
	return $datesArray;
}
	
	
function espectadoresEvento($dominio, $idUsuario, $idSeleccionado, $fechaIni){
	#  echo "<br> espectadoresEvento <br>";
	
	# Conectamos con la BD
	include 'conexion.php';
		
	$datesArray  = array();
		
	if($dominio == 'streamsports')
		$sql = "SELECT 
						COUNT(distinct s.userId) AS espectadores , 
						DATE_FORMAT(p.fechaIni,'%Y-%m-%d')  as fecha
					FROM 
						streamsports.users AS u
						LEFT JOIN streamsports.stats AS s ON (s.host = u.host)
						LEFT JOIN streamsports.partidos AS p ON (u.id = p.idUsuario)
					WHERE 
						p.idPartido = ?
					AND(
						(p.fechaIni BETWEEN s.tiempo AND s.tiempoFin) 
						OR  (p.fechaFin BETWEEN s.tiempo AND s.tiempoFin) 
						OR (s.tiempo>=p.fechaIni and s.tiempoFin<=p.fechaFin)
					)";
		
	else
		# esto no hay datos y hay que comprobarlo
		$sql = "SELECT COUNT(distinct s.userId) as espectadores 	, DATE_FORMAT(e.fechaIni,'%Y-%m-%d')  as fecha
		FROM 
			streamevents.users AS u
			LEFT JOIN streamevents.stats AS s ON (s.host = u.host)
			LEFT JOIN streamevents.eventos AS p ON (u.id = e.idUsuario)
		WHERE 
			e.idEvento = ?
			AND(
				(e.fechaIni BETWEEN s.tiempo AND s.tiempoFin) 
				OR (e.fechaFin BETWEEN s.tiempo AND s.tiempoFin)
				OR (s.tiempo>=e.fechaIni and s.tiempoFin<=e.fechaFin)
            )";
		$stmt = $mysqli->prepare($sql); 
		$stmt->bind_param("i",  $idSeleccionado);
		$stmt->execute(); 
		$result = $stmt->get_result();	
		$row=$result->fetch_assoc();
		$stmt->close();
		
		if($result){
	
			$datesArray[] = array(
				"proceso"=>1,
				"numEspectadores"=>$row['espectadores'],
				"fecha"=>$fechaIni,				
			);
			
		
		} else{
			$datesArray[] = array(
				"numResult"=>0,
				"smsResult"=>'La consulta se ejecuto correctamente, pero no se han encontrado resultados',
				
			);
			
			
		}
	
		$mysqli->close();
		// echo '<pre>'; print_r($datesArray); echo '</pre>';  
		return $datesArray;
	}// fin funcion

?>