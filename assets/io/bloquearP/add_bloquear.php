<?php
header('Content-Type: text/html; charset=UTF-8'); 

	# Conectamos 
	include '../conexion.php';
	# echo '<br>' .$subdominio_php. '<br>' ;
	
	# Compruebo los parametros que envio
	require_once '../funciones.php';
	$subdominio = comprobarParametros ('subdominio');
	$idUsuario = comprobarParametros ('idUsuario');
	$queSoy = comprobarParametros ('queSoy');	
	$pSeleccionada = comprobarParametros ('pSeleccionada');
	
	# echo '<pre>'; print_r($_GET); echo '</pre>';  echo '<pre>'; print_r($_POST); echo '</pre>';   echo '<pre>'; print_r($_REQUEST); echo '</pre>';  
	# echo '<br> idUsuario:' .$idUsuario . '<br> pSeleccionada:' .$pSeleccionada.  '<br> queSoy:' .$queSoy.  '<br>'.$subdominio.' == '.$subdominio_php;
	
	# Comprobamos que el dominio enviado es el mismo
	if($subdominio == $subdominio_php ){
		#  Depende de si es poblacion o provincia llamamos a una funcion u otra
		# Preguntamos si no existe en 'poblacion_bloqueada' para ese usuario.
		# Si no existe la añadimos 
		if($queSoy == 'pob'){	
			$stmt = $mysqli->prepare("call add_bloqueada_poblacion(?,?) ");
		}else{
			$stmt = $mysqli->prepare("call add_bloqueada_provincia(?,?) ");		
		}
		$stmt->bind_param("is",  $idUsuario, $pSeleccionada);	
		$stmt->execute(); 
		$result = $stmt->get_result();	
		      
		# echo '<pre>'; print_r($row); echo '</pre>';
		if($stmt->errno){	
			$mensaje =  "<br> error: ". $stmt->errno; 
			$id='-1';
			$existe='-1';
		}else{
			if ($row = $result->fetch_assoc()) {  # hay resultados
				
				# Principio del mensaje que enviaremos
				if($queSoy == 'pob'){	
					$mensaje_aux = "La población  " . $row['poblacion'] . " (" . $row['provincia'] . ") ";
				}else{
					$mensaje_aux = "La provincia  " . $row['provincia']  ;
				}				
				# Estaba bloqueada o no
				if(isset($row['idUser'])){	// Ya existe, esta bloqueada
					# Que soy???
					$existe = 1;
					$mensaje = $mensaje_aux . " ya está bloqueada.";
				}else{ // no existe, no estaba bloqueada
					$mensaje = $mensaje_aux . " se ha bloqueado correctamente.";
					$existe = 0;
				}
			}	
			$id = $row['id'];		
		}
			
		# Montamos el array que vamos a enviar
		$data[] = array(
			"id"=>$id,
			"provincia"=>$row['provincia'],
			"poblacion"=>$row['poblacion'],
			"smsResult"=>$mensaje,
			"numResult"=>1,	// ha ido bien		
			"existe"=> $existe			
		);	
		
		$stmt->close();
		echo json_encode($data);
		$mysqli->close();
	}
	else{
		
		# indicamos que hay algun error
		$data[] = array(
			"subdominio"=>$subdominio, 
			"smsResult"=>'ERROR, se ha producido algún error al intentar conseguir la información del cliente. Ponte en contacto con el administrador.',
			"numResult"=>0 
		);	
		
		echo json_encode($data);		
	}
?>