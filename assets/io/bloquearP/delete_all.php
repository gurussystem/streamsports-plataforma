<?php
# para probar --> http://cornella.pruebasmari/userWhat.php?hostUser="cornella.streamgps.com"

	# Conectamos 
	include '../conexion.php';
	# echo '<br>' .$subdominio_php. '<br>'  ;
	
	require_once '../funciones.php';
	$subdominio = comprobarParametros ('subdominio');
	$idUsuario = comprobarParametros ('idUsuario');
	$queSoy = comprobarParametros ('queSoy');
	# echo '<pre>'; print_r($_GET); echo '</pre>'; echo '<pre>'; print_r($_POST); echo '</pre>'; echo '<pre>'; print_r($_REQUEST); echo '</pre>';  
	
	if($subdominio == $subdominio_php ){
	# echo "<br>".$subdominio."==". $subdominio_php."<br>";
		if($queSoy == 'pob'){
			# echo "<br>soy poblacion<br>";
			
			# Eliminamos del listado la poblacion seleccionada
			$stmt = $mysqli->prepare(" call delete_all_poblacion(?) ");
			$stmt->bind_param("i",  $idUsuario );	
			$stmt->execute(); 

			$affected_rows_delete =  mysqli_affected_rows($mysqli);
			if($affected_rows_delete != 0){
				$mensaje = 'Se han desbloqueado correctamente todas las poblaciones.';
			}else{
				$mensaje = 'No se han podido desbloqueado todas las poblaciones, si persiste el problema pongase en contacto con el administrador.  ';
			}
			
		}else{
			
			//echo "<br>soy provincia<br>";
			# Eliminamos del listado las provincias seleccionada
			$stmt = $mysqli->prepare("call delete_all_provincia(?)");
			$stmt->bind_param("i",  $idUsuario );	
			$stmt->execute(); 
			
			$affected_rows_delete =  mysqli_affected_rows($mysqli);
			//echo "<br> affected_rows_delete: " .$affected_rows_delete;
			if($affected_rows_delete != 0){
				$mensaje = 'Se han desbloqueado correctamente todas las provincias.';
			}else{
				$mensaje = 'No se han podido desbloqueado todas las provincias, si persiste el problema pongase en contacto con el administrador.  ';
			}
			
		}
	
		
		$data[] = array(
			"numRows"=>$affected_rows_delete , // si es difrenet de 0 es que se ha eliminado la poblacion
			"smsResult"=>$mensaje,
			"queSoy"=>$queSoy,
			"numResult"=>1
		);
		
		$stmt->close();	
		echo json_encode($data);
		$mysqli->close();
	}
	else{
		# Indicamos que hay algun error
		$data[] = array(
			"subdominio"=>$subdominio, 
			"smsResult"=>'ERROR, se ha producido algún error al intentar conseguir la información del cliente. Ponte en contacto con el administrador.',
			"queSoy"=>$queSoy,
			"numResult"=>0
		);	
		
		echo json_encode($data);	
	}
?>