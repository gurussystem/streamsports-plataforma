<?php
	# Conectamos 
	include '../conexion.php';
	# echo "<br> ($host , $user , $pass , $basedatos)";	
	require_once '../funciones.php';
	
	#Recogemos los parametros
	$subdominio = comprobarParametros ('subdominio');
	$idUsuario = comprobarParametros ('idUsuario');
	$queSoy = comprobarParametros ('queSoy');
	# echo '<pre>'; print_r($_GET); echo '</pre>';  echo '<pre>'; print_r($_POST); echo '</pre>';   echo '<pre>'; print_r($_REQUEST); echo '</pre>';  
	# echo '<br> pSeleccionada:' .$pSeleccionada;
	# echo '<br>'.$subdominio.' == '.$subdominio_php;
	
	if($subdominio == $subdominio_php ){
		
		if($queSoy == 'pob'){
			$stmt = $mysqli->prepare("call actualizar_poblaciones(?) ");
		}else{
			$stmt = $mysqli->prepare("call actualizar_provincias(?) ");
		}
		
		$stmt->bind_param("i",  $idUsuario);	
		$stmt->execute(); 
		$result = $stmt->get_result();	
		
		# hay resultados o no
		while($row = $result->fetch_assoc()) { 
			# echo '<br>  <pre>'; print_r($row); echo '</pre>';
			if($stmt->errno){	
				$mensaje =  "<br> error: ". $stmt->errno; 
			}else{
				$data[] = array(
					"id"=>$row['id'],
					"provincia"=>$row['provincia'],
					"poblacion"=>$row['poblacion'],
					"numResult"=>1,	// ha ido bien					
					"numRows"=>$result->num_rows ,	// ha ido bien					
				);
			}
		}
		
		# Controlamos que siempre enviemos datos, porque sino cuando un array este vacio dara problemas
		if(empty($data)){			
			// echo "array sin datos";
			$data[] = array(
				"smsResult"=>'No hay datos',
				"numResult"=>1,
				"numRows"=>0 
			);
		}
		$stmt->close();
		echo json_encode($data);
		$mysqli->close();				
		
		
		}else{		
			$data[] = array(
				"subdominio"=>$subdominio, 
				"smsResult"=>'ERROR, se ha producido algún error al intentar conseguir la información del cliente. Ponte en contacto con el administrador.',
				"numResult"=>0,
				"numRows"=>0 
			);
		}
?>