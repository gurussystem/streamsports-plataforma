<?php
	# Conectamos 
	include '../conexion.php';	
	require_once '../funciones.php';
	
	#Recogemos los parametros
	$subdominio = comprobarParametros ('subdominio');
	$idUsuario = comprobarParametros ('idUsuario');
	 # echo '<pre>'; print_r($_GET); echo '</pre>';  echo '<pre>'; print_r($_POST); echo '</pre>';   echo '<pre>'; print_r($_REQUEST); echo '</pre>';  
	
	
	if($subdominio == $subdominio_php ){
		
		# Ejecutamos la consulta 
		$stmt = $mysqli->prepare("call listaBloqueada(?) "); 
		$stmt->bind_param("i",  $idUsuario);
		$stmt->execute(); 
		$result = $stmt->get_result();	

		# Evaluamos is ha ido todo bien o habido algun fallo
		if($stmt->errno){
			echo "<br> error: ". $stmt->errno;
		}	
		
		# Controlamos si hay resultados o no
		if($result->num_rows != 0){
			while($row=$result->fetch_assoc()) {
				// echo '<pre>'; print_r($row); echo '</pre>';  		
						
				$data[] = array(
					"id"=>$row["id"], 
					"poblacion"=>$row["poblacion"],  // uncode por los acentos
					"provincia"=>$row["provincia"], 
					// "fecha_creacion"=>$row["fecha_creacion"], 
					// "idUser"=>$row["idUser"],
					"idUser"=>$idUsuario,
					"numRows"=>$result->num_rows,
					"smsResult"=>'La consulta se ejecuto correctamente',
					"numResult"=>1
				);
			}//fin while
		}else {
			$data[] = array(
					"numRows"=>0,
					"smsResult"=>'La consulta se ejecuto correctamente, pero no se han encontrado resultados',
					"numResult"=>1
				);
			
		}
		
		
		$stmt->close();
		echo json_encode($data);
		$mysqli->close();
		
	}else{
		// indicamos que hay algun error
		$data[] = array(
			"subdominio"=>$subdominio, 
			"smsResult"=>'ERROR, se ha producido algún error al intentar conseguir la información del cliente. Ponte en contacto con el administrador.',
			"numResult"=>0
		);		
		
		echo json_encode($data);
	}
	
	
?>