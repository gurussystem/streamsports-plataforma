<?php
# para probar --> http://cornella.pruebasmari/userWhat.php?hostUser="cornella.streamgps.com"

	# Conectamos 
	include '../conexion.php';
	# echo '<br>' .$subdominio_php. '<br>'  ;
	
	# Compruebo los parametros que envio
	require_once '../funciones.php';
	$subdominio = comprobarParametros ('subdominio');
	$idUsuario = comprobarParametros ('idUsuario');
	$idSelect = comprobarParametros ('idSelect');
	$queSoy = comprobarParametros ('queSoy');	
	# echo '<pre>'; print_r($_GET); echo '</pre>'; echo '<pre>'; print_r($_POST); echo '</pre>'; echo '<pre>'; print_r($_REQUEST); echo '</pre>';  
	
	if($subdominio == $subdominio_php ){
		# Recojo datos
		
		$stmt = $mysqli->prepare("call info_poblacion (?,?) ");
		$stmt->bind_param("ii",  $idSelect ,$idUsuario);	
		$stmt->execute(); 
		$result = $stmt->get_result();	
		$row = $result->fetch_assoc();
		$poblacion = $row['poblacion'];
		$provincia = $row['provincia'];
		$stmt->close();	
		
		# Eliminamos		
		$stmt = $mysqli->prepare("call delete_one_aux (?,?) ");
		$stmt->bind_param("ii",  $idSelect ,$idUsuario);	
		$stmt->execute(); 
		
	
		# Montamos arrays segun los resultados
		if($stmt->errno){	
	
			$data[] = array(
				"numRows"=>0, # si es difrenet de 0 es que se ha eliminado la poblacion
				"smsResult"=> " Error (". $stmt->errno.") , No se ha podido desbloquear.",
				"numResult"=>0
			);
			
		}
		else{
			if($queSoy == 'pob'){
				$mensaje = "La población " .  $poblacion . " (".$provincia .") se ha desbloqueado correctamente";
			}else{ 
				$mensaje = "La provincia " .  $provincia . "  se ha desbloqueado correctamente";
			}
				
			$data[] = array(
				"numRows"=>1, // si es difrenet de 0 es que se ha eliminado la poblacion
				"smsResult"=> $mensaje,
				"numResult"=>1
			);			
		}
		$stmt->close();	
		echo json_encode($data);
		$mysqli->close();
	}
	else{
		# indicamos que hay algun error
		$data[] = array(
			"subdominio"=>$subdominio, 
			"smsResult"=>'ERROR, se ha producido algún error al intentar conseguir la información del cliente. Ponte en contacto con el administrador.',
			"numResult"=>0
		);	
		
		echo json_encode($data);		
	}
	
?>