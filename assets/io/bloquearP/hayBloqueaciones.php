<?php 
	# Conectamos 
	include '../conexion.php';
	// echo '<br>' .$subdominio_php. '<br>'  ;
	require_once '../funciones.php';
	
	$idUsuario = comprobarParametros ('idUsuario');
	# echo '<pre>'; print_r($_GET); echo '</pre>';  echo '<pre>'; print_r($_POST); echo '</pre>';   echo '<pre>'; print_r($_REQUEST); echo '</pre>';  

	# Comprobar que la poblacion para ese idUsuario no existe
	$stmt = $mysqli->prepare("call hayBloqueaciones(?) ");
	$stmt->bind_param("i",  $idUsuario);	
	$stmt->execute(); 
	$result = $stmt->get_result();
	// $numRows = $result->num_rows ;
	$row = $result->fetch_assoc();
	$numRows = $row['numRegistros'];
	$stmt->close();	
		
		
	if($numRows == 0){ 
		$stmt = $mysqli->prepare("call desactivar_geobloqueo(?) ");
		$stmt->bind_param("i",  $idUsuario);	
		$stmt->execute(); 
		$stmt->close();			
	}		
?>