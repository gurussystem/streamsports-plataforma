<?php
	//Conectamos 
	include '../conexion.php';
	
	// Definicion de arrays
	$poblacion = array();
	$provincia = array();
	$comunidad = array();
	$data = array();
	
	//Ejecutamos la consulta 
	//$stmt = $mysqli->prepare("Select * from poblacion "); 
	$stmt = $mysqli->prepare(" call listaPoblaciones() "); // listado completo de poblaciones con su provincia, comunidad...	
	$stmt->execute(); 
	$result = $stmt->get_result();	

	//Evaluamos is ha ido todo bien o habido algun fallo
	if($stmt->errno){
		echo "<br> error: ". $stmt->errno;
	}	
	
	/* usuarios conectados */
	$rowTotales = $result->num_rows;
	while($row=$result->fetch_assoc()) {
	//	 echo '<pre>'; print_r($row); echo '</pre>';  
	//rellenamos el array que utilizaremos en este php
		/*
		$data[] = array(
				"idpoblacion"=>$row["idpoblacion"], 
				"poblacion"=>$row["poblacion"], 
				"poblacionseo"=>$row["poblacionseo"], 
				"provincia"=>$row["provincia"], 
				"provinciaseo"=>$row["provinciaseo"], 
				"provincia3"=>$row["provincia3"], 
				"idcomunidad"=>$row["idcomunidad"], 
				"comunidad"=>$row["comunidad"], 
				"postal"=>$row["postal"], 
				"latitud"=>$row["latitud"], 
				"longitud"=>$row["longitud"]					
			);
		*/
		
		//$poblacion[] = $row["poblacion"] . '  <span class="peque_txt">('.  $row["provincia"].')</span>' ;					
		//$poblacion[] = $row["poblacion"] . ' ('.  $row["provincia"].')' ;					
		 $poblacion[] = $row["poblacion"] ;					
		}//fin while
		$stmt->close();
		// echo '<pre>'; print_r($poblacion); echo '</pre>'; 
		
		//////////////////////////
		// select provincias 	
		//////////////////////////
		$stmt = $mysqli->prepare("call listaProvincias()"); 
		$stmt->execute(); 
		$result = $stmt->get_result();	

		//Evaluamos is ha ido todo bien o habido algun fallo
		if($stmt->errno){
			echo "<br> error: ". $stmt->errno;
		}	
		
		/* usuarios conectados */
		$rowTotales = $result->num_rows;
		while($row=$result->fetch_assoc()) {
			//	 echo '<pre>'; print_r($row); echo '</pre>';  
			$provincia[] =  $row["provincia"] ;				
		}//fin while
		$stmt->close();
		// echo '<pre>'; print_r($provincia); echo '</pre>'; 
		
		//////////////////////////
		// select comunidad 	
		//////////////////////////
		$stmt = $mysqli->prepare("call listaComunidades() ");
		$stmt->execute(); 
		$result = $stmt->get_result();	

		//Evaluamos is ha ido todo bien o habido algun fallo
		if($stmt->errno){
			echo "<br> error: ". $stmt->errno;
		}	
		
		/* usuarios conectados */
		$rowTotales = $result->num_rows;
		while($row=$result->fetch_assoc()) {
			//	 echo '<pre>'; print_r($row); echo '</pre>';  
			$comunidad[] =  $row["comunidad"] ;				
		}//fin while
			$stmt->close();
			// echo '<pre>'; print_r($comunidad); echo '</pre>'; 
			
		$stmt->close();
		
		$data['poblacion'] = $poblacion;
		$data['provincia'] = $provincia;
		$data['comunidad'] = $comunidad;
		
		
		//echo '<pre>'; print_r($data); echo '</pre>'; 
		echo json_encode($data);
	
	$mysqli->close();
?>