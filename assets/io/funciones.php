<?php
	function comprobarParametros ($param){
		
		if(isset($_GET[$param])){ 	
			return ($_GET[$param]);
		
		}else if(isset($_POST[$param])) {
			return ($_POST[$param]);    
		
		}else if(isset($_REQUEST[$param])) {
			return ($_REQUEST[$param]);    
		
		}
	}
	
	/*******************************************************************
	* Sumo o resto el desfase horario según en que parte del mundo estemos
	*******************************************************************/ 
	function desfaseHorario($fecha, $tDesfase, $opcion, $operacion){			
		// echo "<br> desfaseHorario( " . $fecha . " ," .  $tDesfase . " , " . $opcion  . " , " .  $operacion . " ) ";
		
		// convierto la fecha en timestamp
		$dtime = DateTime::createFromFormat("Y-m-d H:i:s", $fecha , new DateTimeZone("UTC"));
		$fecha_timestamp = $dtime->getTimestamp();
		// $var = DateTime::createFromFormat('Y-m-d H:i:s', $fecha)->getTimestamp();
		// Calculo en milisegundos los minutos
		$milisegundos_desfase = $tDesfase * 60;
		
		// Sumo los milisegundos a la fecha
		if($operacion == 'suma'){
			$fecha_timestamp_new = $fecha_timestamp + $milisegundos_desfase; 
		}else{
			$fecha_timestamp_new = $fecha_timestamp - $milisegundos_desfase; 
		}
		
		// Convierto al formato normal y devuelvo el resultado		
		switch($opcion){
			case 'entera':
				$fecha_result = gmdate('Y-m-d H:i:s',$fecha_timestamp_new);
				break;
				
			case 'hora':
				$fecha_result = gmdate('H:i:s',$fecha_timestamp_new);
				break;
				
			case 'fecha':
				$fecha_result = gmdate('Y-m-d',$fecha_timestamp_new);
				break;	
				
		}
	
		return $fecha_result;
	}
	
	
	function tiempoQueFaltaParaComenzar($fechaPartido, $fActual , $tiempoDesfase, $operacion){
		
		// convierto la fecha en timestamp
		$dtime_partido = DateTime::createFromFormat("Y-m-d H:i:s", $fechaPartido , new DateTimeZone("UTC"));
		$timestamp_partido = $dtime_partido->getTimestamp();
		
		//convierto en timestamp la fechaActual añadiendole el desfase horario
		$dtime_fActual = DateTime::createFromFormat("Y-m-d H:i:s", $fActual , new DateTimeZone("UTC"));
		$timestamp_fActual_aux = $dtime_fActual->getTimestamp();
		
		// Calculo en milisegundos los minutos
		$milisegundos_desfase = $tiempoDesfase * 60;
		
		// Sumo los milisegundos a la fecha
		if($operacion == 'suma'){
			$timestamp_fActual = $timestamp_fActual_aux + $milisegundos_desfase; 
		}else{
			$timestamp_fActual = $timestamp_fActual_aux - $milisegundos_desfase; 
		}
		
		return ceil(($timestamp_partido - $timestamp_fActual)/60);
		
	}
	

	/*******************************************************************
	* Compruebo que la fecha es correcta
	*******************************************************************/
	 function comprobar_fechaHora($fecha, $hora, $opcion, $desfase){
		// echo "<br>***********************";	echo "<br>comprobar_fechaHora( " . $fecha . " , " .$hora . " , " . $opcion . " , " . $desfase. ")";
		
		// Los dos vacios
		if( empty($fecha) && empty($hora) ){ 
			return '';
			
		}	else  if( !empty($fecha) && empty($hora)){  // hay fecha pero no hora
			// Dependiendo del input desde o hasta el valor sera diferente
			if($opcion == 'desde'){
				return  $fecha." ".explorarFormato("00:00:00", $desfase, $opcion); 
			}else{				
				return  $fecha." ".explorarFormato("23:59:59", $desfase, $opcion);
			}
			
		} else{ // Estan llenos los dos			
			$resultado = $fecha." ".explorarFormato($hora, $desfase, $opcion);
			return  desfaseHorario($resultado, $desfase, "entera", "resta");
			
		}
	} 
 
	/*******************************************************************
	* Compruebo que el formato de la fecha es correcta
	*******************************************************************/ 
	function explorarFormato($hora, $desfase, $opcion){
		// echo "<br> explorarFormato (" . $hora  . " , " .   $desfase . " , " . $opcion . ")";
	
		if(empty($hora)){
			if($opcion == 'desde'){
				return  "00:00:00";
			}else{				
				return  "23:59:59";
			}
			
		}else{
	
			//list($h, $m, $s) = explode(":", $hora);
			list($h, $m) = explode(":", $hora);
			 // echo '<br> (antes) hora: '.$h.'  minutos: '.$m.' segundos: '.$s.'<br>';
			
			$hora_h = tiempoIsNulo($h, $opcion, "hora");
			$hora_m = tiempoIsNulo($m, $opcion, "minutos");
			// $hora_s = tiempoIsNulo($s, $opcion, "segundos");		
			return  $hora_h.":". $hora_m.":00";
			}
		

	}
	
	/*******************************************************************
	* Compruebo que el formato de la fecha es correcta
	*******************************************************************/ 
	function tiempoIsNulo($tiempo, $opcion, $opcion2){
		// echo "<br> tiempoIsNulo (" . $tiempo  . " , " .   $opcion . ")";
		if(empty($tiempo)){ // si tiempo esta vacio
				
			if($opcion == 'desde') {
				return "00"; 
			}else{ // opcion = hasta
				if ($opcion2 == 'hora'){	
					return  "23";
				}else{
					return  "59";
				}
			}
			
		}else{ // campo lleno
			return $tiempo; 
		}
	}
	
	/*******************************************************************
	* Compruebo que el formato de la fecha es correcta
	*******************************************************************/ 
	function comprobar_fechas($fDesde, $fHasta, $tablaCampo){
		// echo "<br> comprobar_fechas (" . $fDesde  . " , " .   $fHasta . " , " . $tablaCampo .")";
	
		// Los dos vacios
		if( empty($fDesde) && empty($fHasta) ){ 
			return  "";
		}else if( !empty($fDesde) && empty($fHasta) ){ // desde esta lleno y hasta vacio
				$fechaHora_actual = date("Y-m-d") ." ". date("H:m:s"); 
				// echo "<br> fechaHora_actual:   " , $fechaHora_actual ;
				//return  " AND  grabacions.horaini BETWEEN TIMESTAMP('".$fDesde."')   and  TIMESTAMP('".$fechaHora_actual ."') " ;
				return  " AND  ".$tablaCampo." BETWEEN TIMESTAMP('".$fDesde."')   and  TIMESTAMP('".$fechaHora_actual ."') " ;
		}else 
			if( !empty($fDesde) && !empty($fHasta) ){ // los dos estan llenos	
			//	return  " AND grabacions.horaini BETWEEN TIMESTAMP('".$fDesde."')   and  TIMESTAMP('".$fHasta."') ";
				return  " AND ".$tablaCampo." BETWEEN TIMESTAMP('".$fDesde."')   and  TIMESTAMP('".$fHasta."') ";
		}else{ // fecha desde vacia y fecha hasta llena
			//tenemos que enviar un mensaje al usuario indicando que necesitamos que introduzca un desde 
			return  "";
		}

	}

	/*******************************************************************
	* Funcion para Sacamos el id del cliente y lo devolvemos
	*******************************************************************/
	function return_idCliente(){
		# Conectamos con la base de datos
		include 'conexion.php';
		
		# consulta
		if($dominio == 'streamsports')
			$sql = "SELECT id FROM streamsports.users where user=? ";
		else
			$sql = "SELECT id FROM streamevents.users where user=? ";
		
		# Ejecutamos la consulta
		$stmt = $mysqli->prepare($sql);
		$stmt->bind_param("s",  $subdominio);	
		$stmt->execute(); 
		// $stmt->bind_result($idCliente);
		$result = $stmt->get_result();	
		$row = $result->fetch_assoc();	
		$stmt->close();
		$mysqli->close();
		
		# Devolvemos el id del cliente
		return $row['id']; 
	}
	
	/*******************************************************************
	* Devolvemos el dominio o el subdominio, según lo que contenga el parametro $opcion	
	*******************************************************************/
	function sacar_sub_dominio($opcion){
		$serverName = explode(".", $_SERVER['SERVER_NAME']);
		if($opcion == 'dominio')
			return $serverName[1]; // dominio
		else	
			return $serverName[0]; // subdominio 
	}	

	
	function limpiarPalabra($cadena){
	/*
		$search  = array('á', 'Ã', 'é', 'í', 'ó', 'ú',  'Á', 'É', 'Í', 'Ó', 'Ú' , 'à', 'à', 'ì', 'ò', 'ù',  'À', 'È', 'Ì', 'Ò', 'Ù'  );
		$replace = array('a', 'a', 'e', 'i', 'o', 'u' , 'A', 'E', 'I', 'O', 'U',  'a', 'e', 'i', 'o', 'u',  'A', 'E', 'I', 'O', 'U' );
		$subject =  utf8_encode($row2['ciudad']);
		$usuarios[$j]['ciudad'] = str_replace($search, $replace, $subject);
	*/
	 $url = strtolower($cadena);
	 //Reemplazamos caracteres especiales latinos
	 /*
	 $find = array('â','ê','î','ô','û','ã','õ');
	 $repl = array('a','e','i','o','u','a','o');
	 $resultado = str_replace($find, $repl, $url);
*/
	 
		//esultado = str_replace($search, $replace, $cadena);
		return $resultado;	
	}
	
	function diaSemana_fecha($param){
		switch($param){
			 case 'Monday' :  		$dia = "Lunes "; break;			
			 case 'Tuesday':		$dia = "Martes "; break;	
			 case 'Wednesday':	$dia = "Miércoles "; break;	
			 case 'Thursday':		$dia = "Jueves "; break;	
			 case 'Friday':			$dia = "Viernes "; break;	
			 case 'Saturnady':		$dia = "Sábado "; break;	
			 case 'Sunday':			$dia = "Domingo "; break;	
			 default:					$dia = ""; break;	
		}
		
		return $dia;
	}
	
	
	function mes_fecha($param){
		switch($param){
			case 'January' :  		$mes = "Enereo "; break;			
			case 'February':		$mes = "Febrero "; break;	
			case 'March':				$mes = "Marzo "; break;	
			case 'April':				$mes = "Abril "; break;	
			case 'May':				$mes = "Mayo "; break;	
			case 'June':				$mes = "Junio "; break;	
			case 'July':				$mes = "Julio "; break;	
			case 'August':			$mes = "Agosto "; break;	
			case 'September':		$mes = "Septiembre "; break;	
			case 'October':			$mes = "Octubre "; break;	
			case 'November':		$mes = "Noviembre "; break;	
			case 'December':		$mes = "Diciembre "; break;	
			default:					$mes = ""; break;	
		}
		return $mes;		
	}
	
 /**
     * recogerDatosMaxMind
     * @param String $ip del usuario web
     **/
	 use GeoIp2\WebService\Client;
     function recogerDatosMaxMind($ip){ 
		
			//echo "<br> function recogerDatosMaxMind (".$ip.") <br>" ;
        $result=array();
        $geoIpClient = new Client(105705, 'JgM6k64wGiRb');
        $record = $geoIpClient->city($ip);
        
		$ciudad = "";
		if(isset($record->city->names['es'])){
			$ciudad =  $record->city->names['es'];
		}else{
			if(isset($record->city->names['en'])){
				$ciudad =  $record->city->names['en'];
			}		
		}
		
		$comunidad_autonoma = "";
		if(isset($record->subdivisions[0])){
			if(isset($record->subdivisions[0]->names['es'])){			
				$comunidad_autonoma =  $record->subdivisions[0]->names['es'];
			}
		}
		
		$pais_code = $record->country->isoCode;
		$pais =  $record->country->names['es'];
		$continente_code = $record->continent->code;
		$continente = $record->continent->names['es'] ;
		$lat = $record->location->latitude;
		$lng = $record->location->longitude;		
        $find = array('ÃƒÂ±', 'Ã', 'Ã¡', 'Ã©', 'Ã­','ï¿½','Ã³','Ãº','n~','ÃƒÂ¡','Ã±','n~','Ã‘','Ãš', 'â','ê','î','ô','û','ã','õ', 'Ã©');
        $repl = array(  'á', 'á',  'é', 'í','í','ó','ú','ñ','ñ','ñ','ñ','Ñ','Ú', 'a','e','i','o','u','a','o','e');
       // $ciudad =  str_replace($find, $repl, utf8_decode($ciudad) ); 	
        $provincia="";
         if(isset($record->subdivisions[1])){
            $provincia = $record->subdivisions[1]->names['es'];
        }
        $codigo_postal="";
        if(isset($record->postal)){
            $codigo_postal =$record->postal->code;
        }
		
        if(empty($ciudad) && empty($provincia) & empty($codigo_postal)){
            $result['enontrado']=0;
        }else{
            $result['enontrado']=1;
            $result['ciudad']=$ciudad;
            $result['prov']=$provincia;
            $result['cp']=$codigo_postal;
            $result['comunidad_autonoma']=$comunidad_autonoma;
            $result['pais_code']=$pais_code;
            $result['pais']=$pais;
            $result['continente_code']=$continente_code;
            $result['continente']=$continente;
            $result['lat']=$lat;
            $result['lng']=$lng;	
            $result['ip']=$ip;	
            $result['mensaje']="Datos recuperados";	
			$result['proceso']=1;	
        }
        $geoIpClient=null;
		 // echo 'resultado: <pre>'; print_r($result); echo '</pre>'; 	
        return $result;
    }

	
	
?>