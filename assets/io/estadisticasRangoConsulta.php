<?php
	header("Content-Type: text/html;charset=utf-8");
	require_once '../libs_internas/maxmind/vendor/autoload.php';
	use GeoIp2\WebService\Client;
	$geoIpClient = new Client(105705, 'JgM6k64wGiRb');
	
	# Conectamos con la BD
	include 'conexion.php';
	
	# Compruebo los parametros que envio
	require_once 'funciones.php';
	require_once 'funcionesEstadisticas.php';
	
	$fechaI = comprobarParametros('fechaI');
	$fechaF = comprobarParametros('fechaF');
	#$fechaI  = '2016-01-01';
	#$fechaF  = '2016-09-22';
	# echo '<pre>'; print_r($_GET); echo '</pre>';  echo '<pre>'; print_r($_POST); echo '</pre>';  
	$idUsuario = return_idCliente();
	# echo "<br> $idUsuario";	
	$dominio = sacar_sub_dominio('dominio');
	# echo "<br> $dominio";	
	
	# Definicion de arrays
	$data = array();
	$datos = array();
	
	# Consulta para sacar todos los idPartido - idEvento que hayan entre las dos fechas seleccionadas.
	if($dominio == 'streamsports')
		$sql = "SELECT idPartido as id , DATE_FORMAT(fechaIni,'%Y-%m-%d')  as fechaIni, titulo FROM streamsports.partidos WHERE idUsuario = ? AND  fechaIni BETWEEN ? AND ? ORDER  BY fechaIni ASC ";
	else
		$sql = "SELECT idEvento as id , DATE_FORMAT(fechaIni,'%Y-%m-%d')  as fechaIni, titulo FROM streamevents.eventos WHERE idUsuario = ? AND  fechaIni BETWEEN ? AND ? ORDER  BY fechaIni ASC   ";
	
	# Ejecutamos la consulta 
	$stmt = $mysqli->prepare($sql); 
	$stmt->bind_param("iss",  $idUsuario,$fechaI, $fechaF);
	$stmt->execute(); 
	$result = $stmt->get_result();	

	# Evaluamos is ha ido todo bien o habido algun fallo
	if($stmt->errno){
		echo "<br> error: ". $stmt->errno;
	}	
		
	# Controlamos si hay resultados o no
	if($result->num_rows != 0){
		while($row=$result->fetch_assoc()) {
			//  echo '<pre>'; print_r($row); echo '</pre>';  		
				
			$data[] = array(
				"id"=>$row["id"], 
				"fechaIni"=>$row["fechaIni"], 
				"dominio"=> $dominio, 			
				"subdominio"=> sacar_sub_dominio('subdominio') , 				
				"idUsuario"=> $idUsuario, 
				"fechaI"=>$fechaI,
				"fechaF"=>$fechaF	,  
				"numRows"=>$result->num_rows,
				"smsResult"=>'La consulta se ejecuto correctamente',
				"numResult"=>1,
				"nomLabel"=>"Partido " . $row['id'] ,
				"titulo"=> $row['titulo'] 
			);
		}//fin while
	}else {
		$data[] = array(
			"numRows"=>0,
			"smsResult"=>'La consulta se ejecuto correctamente, pero no se han encontrado resultados',
			"numResult"=>0
		);
	}
	
	# echo '<pre>'; print_r($data); echo '</pre>';  			
	$stmt->close();

	$numResultado = count($data);
	# $mysqli->close();
	
	# Controlamos si hay datos o no 
	if(	$data[0]['numRows'] != 0 ){
		# Sacamos array de fechas
		$fechas  = array();
		$fechas = selectArrayFechas($dominio, $idUsuario,$fechaI, $fechaF );
		$datos['fechas'] = $fechas;

		
		# sacamos array espectadores
		$datosEsp = array();
		$contEspectadorFecha = 0;
		$espectadores = array();
			
		for($i=0; $i<$numResultado; $i++){
			# echo '<br> PARTIDOS <pre>'; print_r($data[$i]); echo '</pre>';  	
			$idSeleccionado = $data[$i]['id'];
			$datosEsp[$i] = espectadoresEvento($dominio, $idUsuario, $idSeleccionado, $data[$i]['fechaIni'] );
			
			$data[$i]['numEspectadores'] = $datosEsp[$i][0]['numEspectadores'];
		} // fin for
		$datos['datosEsp'] = $datosEsp;
		
		
		# echo '<pre>'; print_r($datosEsp); echo '</pre>';  		
		
		# Recorro el array de fechas
		for($j=0; $j<count($fechas); $j++){ 	
			#  Recorro el array de espectadores
			for($i=0; $i<count($datosEsp); $i++){ 
				
				# Compruebo que la fecha actual es la misma que la de los espectadores y la sumo en un contador 
				if($fechas[$j]['fechaEntera'] ==  $datosEsp[$i][0]['fecha'] ){
					$contEspectadorFecha  = $contEspectadorFecha  + $datosEsp[$i][0]['numEspectadores']; 			
				}
			} // fin for 'i'
			
			# Termino de sumar para la misma fecha y lo añado en el array	
			array_push($espectadores, $contEspectadorFecha);	
			$contEspectadorFecha = 0; // inicializamos			
				
		} // fin for 'j'
		
		$mysqli->close();
		#  echo '<pre>'; print_r($data); echo '</pre>';  		
		$datos['espectadores'] = $espectadores;	
		$datos['proceso'] = 1;
		$datos['datos'] = $data;
	}
	else{
		// echo "<br> NO HAY datosEsp <br>";
		$datos['proceso'] = 0;
	}
	
	echo json_encode($datos); 
	
?>