<?php
header('Content-Type: text/html; charset=UTF-8'); 

	# Conectamos 
	include '../conexion.php';
	
	# Compruebo los parametros que envio
	require_once '../funciones.php';
	$subdominio = comprobarParametros ('subdominio');
	$idUsuario = comprobarParametros ('idUsuario');
	$idPartido = comprobarParametros ('idPartido');
	$tituloPartido = comprobarParametros ('tituloPartido');
	$descripcion = comprobarParametros ('descripcion');	
	$equipoLocal = comprobarParametros ('equipoLocal');
	$equipoVisitante = comprobarParametros ('equipoVisitante');
	$fechaHoraPartido_aux = comprobarParametros ('fechaHoraPartido');	
	$fechaHoraPartido = DateTime::createFromFormat('Y-m-d H:i:s',$fechaHoraPartido_aux)->getTimestamp();
	# echo '<pre>'; print_r($_GET); echo '</pre>';  echo '<pre>'; print_r($_POST); echo '</pre>';  
	
	$tiempoDesfase = comprobarParametros('tiempoDesfase');
	$fechaUtc_aux = desfaseHorario($fechaHoraPartido_aux, $tiempoDesfase, 'entera', 'resta');
	$fechaUtc = DateTime::createFromFormat('Y-m-d H:i:s',$fechaUtc_aux)->getTimestamp();

	# echo "<br> subdominio: " . $subdominio;
	# echo "<br> dominio: " . $dominio;
	# echo "<br> fechaUtc: " . $fechaUtc;
	# echo "<br> fechaHoraPartido: " . $fechaHoraPartido;
	
	if($subdominio == $subdominio_php ){
		
		if($dominio == 'streamsports'){
			//$stmt = $mysqli->prepare("call update_proximoPartido(?, ?, ? , ? , ?, from_unixtime(?), from_unixtime(?) )");
			$stmt = $mysqli->prepare("call update_proximoPartido(?, ?, ? , ? , ?, ?, ? )");
			//$stmt->bind_param("iisssii",  $idPartido, $idUsuario, $equipoLocal, $equipoVisitante,  $tituloPartido, $fechaUtc, $fechaHoraPartido);	
			$stmt->bind_param("iisssss",  $idPartido, $idUsuario, $equipoLocal, $equipoVisitante,  $tituloPartido, $fechaHoraPartido_aux, $fechaHoraPartido_aux);	
		}else{		
			//$stmt = $mysqli->prepare("call update_proximoEvento(?, ?, ?, ?, from_unixtime(?), from_unixtime(?) )");
			$stmt = $mysqli->prepare("call update_proximoEvento(?, ?, ?, ?, ?, ? )");
		//	$stmt->bind_param("iissii",  $idPartido, $idUsuario, $descripcion, $tituloPartido , $fechaUtc, $fechaHoraPartido);	
			$stmt->bind_param("iisssss",  $idPartido, $idUsuario, $descripcion, $tituloPartido , $fechaHoraPartido_aux, $fechaHoraPartido_aux);	
		}
		$stmt->execute(); 
		//$result = $stmt->get_result();	
	//	$row = $result->fetch_assoc();	
		
		$data[] = array(	
			"smsResult"=>"Los datos  se han modificado correctamente.",
			"numResult"=>1,
			"fechaHoraPartido_aux"=>$fechaHoraPartido_aux,
			"fechaHoraPartido"=>$fechaHoraPartido,
			"fechaUtc_aux"=>$fechaUtc_aux,
			"fechaUtc"=>$fechaUtc
		);
		
		
		$stmt->close();
		echo json_encode($data);
		$mysqli->close();
		
		
		
	}else{
		
		// indicamos que hay algun error
		$data[] = array(
			"subdominio"=>$subdominio, 
			"smsResult"=>'ERROR, se ha producido algún error al intentar conseguir la información del cliente. Ponte en contacto con el administrador.',
			"numResult"=>0 
		);	
		
		echo json_encode($data);		
	}
	
?>