<?php
header('Content-Type: text/html; charset=UTF-8'); 

	# Conectamos 
	include '../conexion.php';
	// echo '<br>' .$subdominio_php. '<br>'  ;
	
	# Compruebo los parametros que envio
	require_once '../funciones.php';
	$subdominio = comprobarParametros ('subdominio');
	$idUsuario = comprobarParametros ('idUsuario');
	//echo '<pre>'; print_r($_GET); echo '</pre>';  echo '<pre>'; print_r($_POST); echo '</pre>';   echo '<pre>'; print_r($_REQUEST); echo /'</pre>';  
	
	# echo "<br> dominio: $dominio";
	if($subdominio == $subdominio_php ){
		# depende de dominio sera una consulta diferente
		if($dominio == 'streamsports')
			$sql = "call select_proximoPartido(?) ";
		else
			$sql = "call select_proximoEvento(?)";
	
		$stmt = $mysqli->prepare($sql);
		$stmt->bind_param("i",  $idUsuario);	
		$stmt->execute(); 
		$affected_rows_insert =  mysqli_affected_rows($mysqli);	
		$result = $stmt->get_result();	
		
		 // hay resultados o no
		//if($stmt->errno){	
		if($result->num_rows==0){
			$mensaje =  "<br> error: ". $stmt->errno; 
			$data[] = array(
				"numResult"=>1,	// ha ido bien					
				"smsResult"=>"No hay datos", 	// ha ido bien		
				"numRows"=>0	// ha ido bien					
			);	
				
		}
		else{
			while($row = $result->fetch_assoc()) { 	
				if($dominio == 'streamsports'){
					$data[] = array(
						"id"=>$row['id'],
						"equipoLocal"=>$row['equipoLocal'],
						"equipoVisitante"=>$row['equipoVisitante'],
						"fechaPartido"=>$row['fechaPartido'],
						"fechaPartido2"=>date('d-m-Y H:i', strtotime($row['fechaPartido'])),
						"tituloPartido"=>$row['tituloPartido'],	
						"numResult"=>1,	// ha ido bien					
						"numRows"=>$result->num_rows 	// ha ido bien					
					);	
			}else{
				$data[] = array(	
						"id"=>$row['id'],
						"descripcion"=>$row['descripcion'],
						"fechaEvento"=>$row['fechaEvento'],
						"fechaEvento2"=>date('d-m-Y H:i', strtotime($row['fechaEvento'])),
						"titulo"=>$row['titulo'],	
						"numResult"=>1,	// ha ido bien					
						"numRows"=>$result->num_rows 	// ha ido bien					
					);	
			}
				
			}// fin while			
		} 
		
		$stmt->close();
		echo json_encode($data);
		$mysqli->close();
		

	}else{
		
		# Indicamos que hay algun error
		$data[] = array(
			"subdominio"=>$subdominio, 
			"smsResult"=>'ERROR, se ha producido algún error al intentar conseguir la información del cliente. Ponte en contacto con el administrador.',
			"numResult"=>0 
		);	
		
		echo json_encode($data);		
	}
	
?>