<?php
header('Content-Type: text/html; charset=UTF-8'); 

	# Conectamos 
	include '../conexion.php';
	# echo '<br>' .$subdominio_php. '<br>'  ;
	
	# Compruebo los parametros que envio
	require_once '../funciones.php';
	$subdominio = comprobarParametros ('subdominio');
	$idUsuario = comprobarParametros ('idUsuario');
	$tituloPartido = comprobarParametros ('tituloPartido');
	$equipoLocal = comprobarParametros ('equipoLocal');
	$equipoVisitante = comprobarParametros ('equipoVisitante');
	$fechaHoraPartido_aux = comprobarParametros ('fechaHoraPartido');	
	$descripcion = comprobarParametros ('descripcion');	
	$fechaHoraPartido = DateTime::createFromFormat('Y-m-d H:i:s',$fechaHoraPartido_aux)->getTimestamp();
	// $fechaHoraPartido = DateTime::createFromFormat('Y-m-d H:i:s',$fechaHoraPartido_aux)->getTimestamp();
	$tiempoDesfase = comprobarParametros('tiempoDesfase');
	$fechaUtc_aux = desfaseHorario($fechaHoraPartido_aux, $tiempoDesfase, 'entera', 'resta');
	$fechaUtc = DateTime::createFromFormat('Y-m-d H:i:s',$fechaUtc_aux)->getTimestamp();
	
	# echo '<pre>'; print_r($_GET); echo '</pre>';  echo '<pre>'; print_r($_POST); echo '</pre>';  
	# echo "<br> dominio: " . $dominio;
	# echo "<br> subdominio: " . $subdominio;
	# echo "<br> subdominio_php: " . $subdominio_php;
	
	if($subdominio == $subdominio_php ){
		# echo "<br>subdominio - dominio: " . $dominio;
		
		if($dominio == 'streamsports'){
			# echo "<br> dominio streamsports: " . $dominio;		
			$stmt = $mysqli->prepare("call add_proximoPartido(?, ?, ?, ?, ?, ?) ");
			// $stmt = $mysqli->prepare("call add_proximoPartido(?, ?, ?, from_unixtime(?), from_unixtime(?), ?) ");
			//$stmt->bind_param("issiis",  $idUsuario, $equipoLocal,$equipoVisitante ,$fechaHoraPartido ,  $fechaHoraPartido, $tituloPartido );
			$stmt->bind_param("isssss",  $idUsuario, $equipoLocal,$equipoVisitante ,$fechaHoraPartido_aux ,  $fechaHoraPartido_aux, $tituloPartido );

		}else{
			# echo "<br> dominio streameventos: " . $dominio;
			//$stmt = $mysqli->prepare("call add_proximoEvento(?, ?,  from_unixtime(?), from_unixtime(?), ?) ");
			$stmt = $mysqli->prepare("call add_proximoEvento(?, ?,  ?, ?, ?) ");
			//$stmt->bind_param("isiis",  $idUsuario, $descripcion, $fechaUtc ,  $fechaHoraPartido, $tituloPartido );	
			$stmt->bind_param("issss",  $idUsuario, $descripcion, $fechaHoraPartido_aux ,  $fechaHoraPartido_aux, $tituloPartido );	
		}
		$stmt->execute(); 
		$result = $stmt->get_result();	
		$row = $result->fetch_assoc();	
	
		# Array de confirmación de la consulta
		$data[] = array(	
			"smsResult"=>"Los datos se han insertado correctamente.",
			"numResult"=>1,
			"idPartido" =>$row['id'],
			"fechaHoraPartido_aux"=>$fechaHoraPartido_aux,
			"fechaHoraPartido"=>$fechaHoraPartido,
			"fechaUtc_aux"=>$fechaUtc_aux,
			"fechaUtc"=>$fechaUtc
		);
		
		$stmt->close();
		echo json_encode($data);
		$mysqli->close();
		
	}else{
		
		# indicamos que hay algun error
		$data[] = array(
			"dominio"=>$dominio, 
			"subdominio"=>$subdominio, 
			"smsResult"=>'ERROR, se ha producido algún error al intentar conseguir la información del cliente. Ponte en contacto con el administrador.',
			"numResult"=>0 
		);			
		echo json_encode($data);
		
	}
?>