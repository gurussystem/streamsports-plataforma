<?php
header('Content-Type: text/html; charset=UTF-8'); 

	//Conectamos 
	include '../conexion.php';
	// echo '<br>' .$subdominio_php. '<br>'  ;
	
	//Compruebo los parametros que envio
	require_once '../funciones.php';
	$idUsuario = comprobarParametros ('idUsuario');
	$tiempoDesfase = comprobarParametros ('tiempoDesfase');
	//echo '<pre>'; print_r($_GET); echo '</pre>';  echo '<pre>'; print_r($_POST); echo '</pre>';   echo '<pre>'; print_r($_REQUEST); echo /'</pre>';  
			
	$stmt = $mysqli->prepare("call info_proximo_partido(?) ");
	$stmt->bind_param("i",  $idUsuario);	
	$stmt->execute(); 
	$affected_rows_insert =  mysqli_affected_rows($mysqli);	
	$result = $stmt->get_result();	
		
	
	//if($stmt->errno){	
	if($result->num_rows==0){
		$mensaje =  "<br> error: ". $stmt->errno; 
		$data[] = array(
			"numResult"=>0,	// ha ido mal					
			"smsResult"=>"No hay datos", 	// ha ido bien		
			"numRows"=>0	// ha ido bien		
		);	
				
	}
	else{
		if($result->num_rows != 0){
			while($row = $result->fetch_assoc()) { 
					
				$data[] = array(
					"equipoLocal"=>$row['equipoLocal'],
					"equipoVisitante"=>$row['equipoVisitante'],
					"fechaHoraPartido"=>$row['fechaPartido'],
					"fechaLocalPartido"=>desfaseHorario($row['fechaLocalPartido'], 0, 'fecha', 'suma'),		
					"fechaPartido"=>desfaseHorario($row['fechaPartido'], $tiempoDesfase, 'fecha', 'suma'),		
					"HoraPartido"=>desfaseHorario($row['fechaPartido'], 0, 'hora', 'suma'),			
					"fechaPartido2"=>date('d-m-Y H:i', strtotime($row['fechaLocalPartido'])),
					"tituloPartido"=>$row['tituloPartido'],	
					//"fActual"=>$row['fActual'],	
					"fechaHoraActual"=>desfaseHorario($row['fActual'], $tiempoDesfase, 'entera', 'suma'),			
					"fechaActual"=>desfaseHorario($row['fActual'], $tiempoDesfase, 'fecha', 'suma'),			
					"horaActual"=>desfaseHorario($row['fActual'], $tiempoDesfase, 'hora', 'suma'),			
					"duracionPartidoUTC"=>desfaseHorario($row['fechaPartido'], 120, 'hora', 'suma'),			
					"duracionPartidoLocal"=>desfaseHorario($row['fechaPartido'], (120+$tiempoDesfase), 'hora', 'suma'),			
					"numResult"=>1,	// ha ido bien					
					"numRows"=>$result->num_rows	,	
					"tiempoQueFalta"=>tiempoQueFaltaParaComenzar($row['fechaLocalPartido'],$row['fActual'] , $tiempoDesfase, 'suma')		
					
				);	
			} // fin while			
		
		}else{
			$data[] = array(
				"numResult"=>1,	// ha ido bien					
				"smsResult"=>"No hay datos", 	// ha ido bien		
				"numRows"=>1	// ha ido bien		
			);	
		} // fin else
	} 
		
	$stmt->close();
	//echo "<pre>";print_r($data);echo "<pre>";
	echo json_encode($data); 
	$mysqli->close();
	
?>