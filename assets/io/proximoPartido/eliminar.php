<?php
header('Content-Type: text/html; charset=UTF-8'); 

	//Conectamos 
	include '../conexion.php';
	// echo '<br>' .$subdominio_php. '<br>'  ;
	
	//Compruebo los parametros que envio
	require_once '../funciones.php';
	$subdominio = comprobarParametros ('subdominio');
	$idUsuario = comprobarParametros ('idUsuario');
	$idEvento = comprobarParametros ('idEvento');
	//echo '<pre>'; print_r($_GET); echo '</pre>';  echo '<pre>'; print_r($_POST); echo '</pre>';   echo '<pre>'; print_r($_REQUEST); echo /'</pre>';  
	
	if($subdominio == $subdominio_php ){
		
		if($dominio == 'streamsports'){
			$stmt = $mysqli->prepare("call delete_proximoPartido(?, ?) ");
		}else{
			$stmt = $mysqli->prepare("call delete_proximoEvento(?, ?) ");
		}
		$stmt->bind_param("ii",  $idUsuario, $idEvento);	
		$stmt->execute(); 
		$affected_rows_insert =  mysqli_affected_rows($mysqli);	
		
		if($stmt->errno){	
			$mensaje =  "<br> error: ". $stmt->errno; 
			$data[] = array(
				"numResult"=>0,	// ha ido bien					
				"smsResult"=>$mensaje 	// ha ido bien					
			);		
		}else{
			$data[] = array(
				"numResult"=>$affected_rows_insert,	// ha ido bien					
				"smsResult"=>"Los datos se han eliminado correctamente." 	// ha ido bien				
			);		
		}
			
		$stmt->close();
		echo json_encode($data);
		$mysqli->close();
	
		
		
	}else{
		
		// indicamos que hay algun error
		$data[] = array(
			"subdominio"=>$subdominio, 
			"smsResult"=>'ERROR, se ha producido algún error al intentar conseguir la información del cliente. Ponte en contacto con el administrador.',
			"numResult"=>0 
		);	
		
		echo json_encode($data);		
	}
	
?>