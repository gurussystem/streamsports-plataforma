<?php
	# Conectamos con la base de datos
	include 'conexion.php';
		
	# Librerias y llamadas... 
	require_once("funciones.php");	
	//require '.././libs_internas/aws/aws-autoloader.php';
	//use Aws\S3\S3Client;
		
	# Recogemos datos (pasaremos el id de la empresa)
	$idGrabacion = comprobarParametros ('idGrabacion');	
	$dominio = sacar_sub_dominio("dominio");
	$subdominio = sacar_sub_dominio("subdominio");
	$data = array();
	
	# tenemos que hacer una consulta donde sacar los datos como  $idCliente y $archivo
	if($dominio == 'streamsports')
		$sql = "SELECT archivo, idUsuario, tipoGrabacion FROM streamsports.grabaciones  WHERE idGrabacion=?";
	else
		$sql = "SELECT archivo, idUsuario, tipoGrabacion FROM streamevents.grabaciones  WHERE idGrabacion=?";
		
	$stmt = $mysqli->prepare($sql);	
	$stmt->bind_param("i", $idGrabacion); 
	$stmt->execute(); 	
	$stmt->bind_result( $archivo, $idUsuario, $tipoGrabacion);
	$stmt->fetch();
	
	$data[] = array(			
			"dominio"=> $dominio, 			
			"subdominio"=> $subdominio, 			
			"archivo"=> $archivo, 			
			"idUsuario"=> $idUsuario, 
			"tipoGrabacion"=>$tipoGrabacion	
	);

	echo json_encode($data); 
	
	$stmt->close();
?>