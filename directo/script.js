/*****************************************************************
	@Descripcion		Variables globales
*****************************************************************/
	var s;
  
	

	var tempo=0;
	var timer;  
	var randtoken=null;
	var etok=null;
	var etokm="";
	var loadTime=0;
	var player;
	var duracionPublicidad = 20000;
	var duracionStopPublicidad = 10000;
	var topOverlay =0;
	var controlbar_H = 32; 
	var posicionPublicidad = 0; 
	var playPublicidad;
	var stopPublicidad;
	
	
	//Variables globales para los acentos 
	var min_a = "\u00e1";
	var min_e = "\u00e9";
	var min_i = "\u00ed";
	var min_o = "&#243;";
	var min_u = "\u00fa";
	var min_n = "\u00f1";
	
	var may_a = "\u00c1";
	var may_e = "\u00c9";
	var may_i = "\u00cd";
	var may_o = "\u00d3";
	var may_u = "\u00da";
	var may_n = "\u00d1";
  
  

	
	
/*****************************************************************
	@Descripcion		
		
*****************************************************************/
	function S4() {
		return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
	}


/*****************************************************************
	@Descripcion		
		
*****************************************************************/
	if (typeof Cookies.get('s') != 'undefined'){
		s=Cookies.get('s');
	}else{
		generateUid = function (separator) {
			var delim = separator || "-";
			return (S4() + S4() + delim + S4() + delim + S4() + delim + S4() + delim + S4() + S4() + S4());
		};
		s=generateUid();
		Cookies.set('s', s);
	}
	
	
/*****************************************************************
	@Descripcion		
	
*****************************************************************/ 
	function vTimer() {
		if(player.getState()=="playing"){
			tempo=tempo+60;
			loadS("playing",tempo,randtoken);
		}else{
			clearInterval(vTimer);
		}
	}
	
	
	
/*****************************************************************
	@Descripcion		
		
*****************************************************************/ 
	function detectmob() { 
		var check = false;
		(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
		return check;
	}
	
/*****************************************************************
	@Descripcion		
		
*****************************************************************/ 
	function loadS(evento,tiempo,token,info) {
		var xmlhttp;
		if (typeof token === 'undefined') { token = 0; }
		if (typeof info === 'undefined') { info = "-"; }
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		}
		xmlhttp.open("GET", encodeURI("https://apisports.streamgps.com/rest/v1/s?host="+location.hostname+"&s="+s+"&event="+evento+"&tipo="+navigator.userAgent+"&eventTime="+tiempo+"&token="+token+"&info="+info), true);
		xmlhttp.send();
	}

	
	
/************************************************************************************************************
 Resize pantalla
************************************************************************************************************/
	var screenW_aux;
	var screenH_aux;

/*******************************************************************************
	@Descripcion: 	Redimension de los elementos se encarga del tama�o de todos los elementos
		
********************************************************************************/		
	function pantallaResize(){	
		
		var screenW = jQuery(window).width(); // Tama�o width ventana del navegador (anchura)
		var screenH = jQuery(window).height(); // Tama�o height ventana del navegador (altura)
		//var topOverlay = $("#overlay").css("top");
		//numOverlayTop = topOverlay.slice(0, -2);
			
		if((screenW_aux != screenW) || (screenH_aux != screenH)  ){ //|| (numOverlayTop <100)
			
			screenW_aux = screenW;
			screenH_aux = screenH;
					
			//Medidas del visor
			var visorW = $("#playertdGAEmcmjOwn").width();
			var visorH = $("#playertdGAEmcmjOwn").height();	
			
			//La medida del contenedor de publicidad sera igual que la del visor  (height)
			// $("#overlay").css("width", visorW);
								 
			//Top del contenedor del banner publicitario, sera resta height (visor - boxBanner - 7px)
			//var img_H = $("#img_publi").height();
			//var topBanner	= ( visorH - (img_H + controlbar_H ) )+ 5; 					
			// $("#overlay").css("top", numOverlayTop+"px");
		}
	}//fin funcion 'pantallaResize'

			
/*******************************************************************************
	@Descripcion: Calcular aspection ration y lo redondeamos hacia arriba
	
********************************************************************************/		
	function aspectRatio(w,h){
		if(w != 0){		
			return Math.ceil( w / (16/9) );	// Resultado height
		}else{				
			return Math.ceil( h * (16/9) ); // Resultado width
		}
	}

	
/*******************************************************************************
	@Descripcion:
		
********************************************************************************/	
	function resize_div_aspectRatio(nombreDiv, calcularW, calcularH){
		if(calcularW == 1){ //Calculo el width
			return aspectRatio(0,$(nombreDiv).height()); 
		}else{//Calculo el height
			return  aspectRatio($(nombreDiv).width(),0); 
		}	
	}

	
/*****************************************************************
		@Descripcion		Funcion para detectar si es un movil o un pc
		
*****************************************************************/
	function detectarMovilPC(){
		var isMobile = {
			Android: function() {
				return navigator.userAgent.match(/Android/i);
			},
			BlackBerry: function() {
				return navigator.userAgent.match(/BlackBerry/i);
			},
			iOS: function() {
				return navigator.userAgent.match(/iPhone|iPad|iPod/i);
			},
			Opera: function() {
				return navigator.userAgent.match(/Opera Mini/i);
			},
			Windows: function() {
				return navigator.userAgent.match(/IEMobile/i);
			},
			any: function() {
				return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
			}
		};
		
		
		if( isMobile.any() ){ 
			// alert('Mobile')
			return 1; // es mvl
		}else {
			return 0; // es pc
			// alert('eres pc');
		};
	}
	
/*****************************************************************
		@Descripcion		Comprobamos si la ip esta en la bd stats_ciudades
		
*****************************************************************/	
	
	function comprobar_ip(ipUsuario){
		// console.log("(comprobar_ip)  ipUsuario: " + ipUsuario);
		$.ajax({
			async:true, 
			cache:false,
			type: 'GET',
			data: { 			
				'ipUsuario' :ipUsuario, 			
			},
			url:  'io/stats_ciudades_ip.php', 
			success: function(response){
				// console.log("Miramos si esta en la bd stats_ciudades ");	
			// 	console.log(response);
				var datosPoblacion =  $.parseJSON(response);	
				//datosPoblacion = datosPoblacion_aux;
				
				if(datosPoblacion[0].numResult != 0){ 
					// Esta en la tabla, tenemos datos (poblacion, provincia....)
					// console.log("Esta mi poblaci�n bloqueada?? " + datosPoblacion[0].poblacion + " (" + datosPoblacion[0].provincia + ")");
					estaMiPoblacionBloqueada(datosPoblacion[0].poblacion, datosPoblacion[0].provincia);
				}else{
					console.log("Ha ocurrido alg�n error... ");					
				}
			
			} // fin success
		});
		
	}
	
/*****************************************************************
		@Descripcion		Comprobamos si la poblacion (provincia) esta bloqueada
		
*****************************************************************/		
	function estaMiPoblacionBloqueada(poblacion, provincia){
			$.ajax({
			async:true, 
			cache:false,
			type: 'GET',
			data: { 			
				'namePoblacion' :poblacion, 			
				'nameProvincia' :provincia, 			
			},
			url:  'io/miPoblacionEstaBloqueda.php', 
			success: function(response){
				// console.log("Miramos si esta en la bd stats_ciudades ");	
			// 	console.log(response);
				estaBloqueda =  $.parseJSON(response);	
	
				if(estaBloqueda[0].numResult != 0){ 
					if(estaBloqueda[0].numRows == 1){  
						console.log("Si esta bloqueada");
						$("#player_no").css("display","block");
						$("#player_no p").html("Emisi"+min_o+"n no esta disponible desde tu poblaci"+min_o+"n  "+ poblacion + " (" + provincia + ")" );
					}else{
						 console.log("No esta bloqueada");
						$("#player_no").css("display","none");
						loadPlayer();	
					}
				
				}else{
					console.log("Ha ocurrido algun error... ");					
				}
			
			} // fin success
		});
		
	}
	
	
	
	 /*****************************************************************
		@Descripcion		
		
	*****************************************************************/ 
	function initGeolocation() {
		//alert('initGeolocation');
		if (navigator && navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(successCallback, errorCallback);
        } else {
            console.log('Geolocation is not supported');
        }
	}
 
  	/*****************************************************************
		@Descripcion		
		
	*****************************************************************/ 
	function errorCallback() {
		//alert('errorCallback');
		// console.log('not permision');
		$("#player_no").css("display","block");
		$("#player_no p").html("Si quiere ver la emisi"+min_o+"n es necesario estar geolocalizado. Disculpe las molestias." );
	}
 
  	/*****************************************************************
		@Descripcion		
		
	*****************************************************************/ 
	function successCallback(pos) {
	//	alert('successCallback');
		//alert(pos);
		// console.log("entre en successCallback");
		// console.log(pos);
		$("#player_no").css("display","none");
		$("#player_no p").html("");
		
		geocoder = new google.maps.Geocoder();
		var latlng = new google.maps.LatLng(pos.coords.latitude,pos.coords.longitude);
			geocoder.geocode({'latLng': latlng}, function(results, status) {
						
				if (status == google.maps.GeocoderStatus.OK) {
					//Check result 0
					var result = results[0];
					//look for locality tag and administrative_area_level_1
					var city = "";
					var state = "";
					//console.log("GeocoderStatus.OK");
					for(var i=0, len=result.address_components.length; i<len; i++) {
						var ac = result.address_components[i];
						if(ac.types.indexOf("locality") >= 0) city = ac.long_name;
						if(ac.types.indexOf("administrative_area_level_2") >= 0) state = ac.long_name;
					}
				// 	console.log("city:"+city+" prov: "+state);
					//only report if we got Good Stuff
					if(city != '' && state != '') {
						//console.log("init LoadDOC");
					
						// loadDoc(city,state); // recoge del movil
						 estaMiPoblacionBloqueada(city, state);
						
					}else{
						$("#player_no").css("display","block");
						$("#player_no p").html("Lo sentimos, pero no encontramos tu localizaci"+min_o+"n.");
					}
				} 
		});
    }
	
/************************************************************************************************************
publicidad
************************************************************************************************************/
 
	
 /*****************************************************************
	@Descripcion		funcion que muestra la publicidad cada x tiempo.
	
*****************************************************************/ 
	function  mostrarPublicidad(){		
	// console.log('mostrarPublicidad');
	// console.log(ads);
//	if(posicionPublicidad == undefined){ posicionPublicidad=0;}
		for(i=0; i < ads.length; i++){		
		// 	console.log(ads.length);
		// 	console.log(posicionPublicidad+ "==" + i);
			if(posicionPublicidad == i){			
				$("#img_publi").attr('src' , ads[posicionPublicidad].url); 			
				stopPublicidad = setTimeout(esconderPublicidad , duracionPublicidad);
				//$("#overlay").css("display", "block");				
			}
		}	
	}
	
	
/*******************************************************************************
	@Descripcion:  Esconde el banner publicitario
	
********************************************************************************/
	function esconderPublicidad(){
		// console.log("Entre en esconderPublicidad " + posicionPublicidad + "==" +  totalPubli);
		
		clearInterval(stopPublicidad);
		$("#overlay").css("display", "none");
		
		if(posicionPublicidad == totalPubli ){
			posicionPublicidad = 0;
		}else{
			posicionPublicidad = posicionPublicidad + 1;
		}
			
		//playPublicidad = setTimeout(mostrarPublicidad , duracionStopPublicidad);
	}
	
	
	
	
	
	/*
	function init(){
		// console.log("la ip del cliente es: " + ipUsuario);
		// console.log("EMPEZAMOS init() ");
			
		$.ajax({
			async:true, 
			cache:false,
			type: 'GET',
			// data: { 				'namePoblacion' :city, 				'nameProvincia' : prov			},
			url:  'io/hayPoblacionesBloqueadas.php', 
			success: function(response){
				console.log("Miramos si hay poblaciones bloqueadas ");	
				console.log(response);
				hayPoblacionesBloqueadas =  $.parseJSON(response);	
				
				// Si hay poblaciones bloqueadas, miraremos si es un movil o un pc, sino jwplayer directamente
				if(hayPoblacionesBloqueadas == true){
					 esMovil = detectarMovilPC();
					 if(esMovil == 1){ // es movil
							console.log("Soy movil ");
							alert("eres mvkl");
							$("#player_no").css("display","block");
							$("#player_no p").html("Necesitamos  permiso para geolocalizar su posici"+min_o+"n" );
							initGeolocation();
					 }else{ // es pc
						 console.log("Soy pc, ahora mirare la ip");
						 if(ipUsuario != ""){
							console.log("Tengo la ip: " + ipUsuario + " esta en la bd stats_ciudades");
							//para puebas si estoy en localhost el maxmind no me lo recoge
							if(ipUsuario == '127.0.0.1') ipUsuario ='88.87.197.114';
							// if(ipUsuario == '88.87.197.114') ipUsuario ='87.218.191.173';							
							comprobar_ip(ipUsuario);
						 }else{
							 console.log("No tengo ip, ahora que?? ");							 
						 }
					 }
						
				}else{
					// Si no hay poblaciones bloqueadas llamamos directamente al jwplayer
					$("#player_no").css("display","none");
					loadPlayer(); // y fin, no hay que hacer nada m�s
				}
			
			} // fin success
		});
	}
	*/