<?php
/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author StreamSports
 * 
 */
require_once dirname(__DIR__) . '/assets/libs_internas/maxmind/vendor/autoload.php';
use GeoIp2\WebService\Client; //(No me funciona)
class DbHandler {
	//echo "<br> entre en clase DbHandler <br>";
    private $conn;
    function __construct() {
		// echo "<br>entre en el construct<br>";
		// echo '<br> ' . dirname(__FILE__) . '/DbConnect.php';
		require_once dirname(__FILE__) . '/DbConnect.php';
		
		// echo "<br> entre en ruta DbConnect <br>";
       
		
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
        //$this->$geoIpClient = new Client(105705, 'JgM6k64wGiRb');
        
    }

    /**
     * recogerDatosPlayer
     * @param String $host subdominio asociado al cliente streamsports
     **/
    public function recogerDatosPlayer($host){
		// echo "<br> function recogerDatosPlayer <br>";
		$host_aux = $host.'.%';
        if($stmt=$this->conn->prepare('call recogerDatosPlayer(?)')){
            $stmt->bind_param('s', $host_aux);
            if ($stmt->execute()) {
                $result = $stmt->get_result();
                $data = $result->fetch_assoc();
                $stmt->close();
				// echo 'resultado: <pre>'; print_r($data); echo '</pre>'; 	
                return $data;
            }
        }
        $stmt->close();
    }

    /**
     * ipInDb
     * @param String $ip del usuario web
     **/
    public function ipInDb($ip){
           //  echo "<br> function ipInDb (".$ip.") <br>" ;
            $result=null;
            $stmt=$this->conn->prepare('call ipInDb(?)');
            $stmt->bind_param('s',$ip); 
            $stmt->execute();
            $result = $stmt->get_result();
            $data = $result->fetch_assoc();
            $stmt->close();
			// echo 'resultado: <pre>'; print_r($data); echo '</pre>'; 	
            return $data;
            
    }

    /**
     * recogerDatosMaxMind
     * @param String $ip del usuario web
     **/
    public function recogerDatosMaxMind($ip){
			// echo "<br> function recogerDatosMaxMind (".$ip.") <br>" ;
        $result=array();
        $geoIpClient = new Client(105705, 'JgM6k64wGiRb');
        $record = $geoIpClient->city($ip);
        $ciudad =  $record->city->names['es'];
		if(isset($record->subdivisions[0])){
			$comunidad_autonoma =  $record->subdivisions[0]->names['es'];
		}
		$pais_code = $record->country->isoCode;
		$pais =  $record->country->names['es'];
		$continente_code = $record->continent->code;
		$continente = $record->continent->names['es'] ;
		$lat = $record->location->latitude;
		$lng = $record->location->longitude;		
        $find = array('ÃƒÂ±', 'Ã', 'Ã¡', 'Ã©', 'Ã­','ï¿½','Ã³','Ãº','n~','ÃƒÂ¡','Ã±','n~','Ã‘','Ãš', 'â','ê','î','ô','û','ã','õ', 'Ã©');
        $repl = array(  'á', 'á',  'é', 'í','í','ó','ú','ñ','ñ','ñ','ñ','Ñ','Ú', 'a','e','i','o','u','a','o','e');
       // $ciudad =  str_replace($find, $repl, utf8_decode($ciudad) ); 	
        $provincia=null;
         if(isset($record->subdivisions[1])){
            $provincia = $record->subdivisions[1]->names['es'];
        }
        $codigo_postal=null;
        if(isset($record->postal)){
            $codigo_postal =$record->postal->code;
        }
		
        if(empty($ciudad) && empty($provincia) & empty($codigo_postal)){
            $result['enontrado']=0;
        }else{
            $result['enontrado']=1;
            $result['ciudad']=$ciudad;
            $result['prov']=$provincia;
            $result['cp']=$codigo_postal;
            $result['comunidad_autonoma']=$comunidad_autonoma;
            $result['pais_code']=$pais_code;
            $result['pais']=$pais;
            $result['continente_code']=$continente_code;
            $result['continente']=$continente;
            $result['lat']=$lat;
            $result['lng']=$lng;	
            $result['ip']=$ip;	
        }
        $geoIpClient=null;
		/**/
		//pongo por poner poque no me funiona el use ...
		 /*
		 $result['enontrado']=1;
         $result['ciudad']="BCN";
         $result['prov']="BCN";
         $result['cp']="08930";
		*/
		// echo 'resultado: <pre>'; print_r($result); echo '</pre>'; 	
        return $result;
    }

    /**
     * poblacionBloqueda
     * @param Array $datosPoblacion -> 'ciudad','prov','cp'
     * @param $idUsuario int identificador de usuario.
     *TODO: Esta función se tiene modificar para detectar provincia tambien.
     **/
    public function poblacionBloqueda($datosPoblacion,$idUsuario){
		// echo "funcion poblacionBloqueda (". $idUsuario.")";
		// echo 'resultado: <pre>'; print_r($datosPoblacion); echo '</pre>'; 	
        $result=0;
        if(isset($idUsuario)){
            $ciudad=$datosPoblacion['ciudad'].'%';
            //$stmt=$this->conn->prepare('SELECT id FROM poblacion_bloqueada where idUsuario=? and poblacion like ?');
            $stmt=$this->conn->prepare('call poblacionBloqueda(?,?)');
            $stmt->bind_param('is',$idUsuario,$ciudad); 
            $stmt->execute();
            $stmt->store_result();
            if($stmt->num_rows==0){
                $result=1;
            }else{
				$result=0;
			}
            $stmt->close();
        }
       //  echo 'resultado: <pre>'; print_r($result); echo '</pre>'; 	
        return $result;
    }

    /**
     * recogerDatosPubli
     * @param Integer $id table users
     */
    public function recogerDatosPubli($idUsuario){
		// echo "recogerDatosPubli (".$idUsuario.")";
        if($stmt=$this->conn->prepare('call recogerDatosPubli(?)')){
            $stmt->bind_param('i', $idUsuario);
            if ($stmt->execute()) {
                $result=array();
                $result = $stmt->get_result();
				// Hay que controlar cuando no hay publicidad
                $results=null;
                while($data = $result->fetch_assoc()){
                    $results[]=$data;
                }
				
				if($results==null){
					$results[]=array("titulo"=>"transparente","url"=>"http://tv2.streamsports.es/directo/img/publi0.png");
				}
				
                $stmt->close();
				//  echo 'resultado: <pre>'; print_r($result); echo '</pre>'; 	
                return $results;
            }
            $stmt->close();
        }
    }

    /**
     * saveIpDb
     * @param los campos a grabar en stats_ciudades
     */
    // public function saveIpDb($ciudad,$ip,$provincia,$comunidad_autonoma,$pais_code,$pais,$continente_code,$continente,$codigo_postal,$lat,$lng){
    public function saveIpDb($datosMaxMind){
		// echo "insertar datos: ";
		//echo 'resultado: <pre>'; print_r($datosMaxMind); echo '</pre>'; 	
        $stmt = $this->conn->prepare("call saveIpDb (?, ?, ?, ? , ?, ?, ?, ?, ?, ?, ?)");            
        $stmt->bind_param("sssssssssss",$datosMaxMind['prov'], $datosMaxMind['ciudad'], $datosMaxMind['comunidad_autonoma'], $datosMaxMind['pais_code'] , $datosMaxMind['pais'], $datosMaxMind['continente_code'], $datosMaxMind['continente'], $datosMaxMind['cp'], $datosMaxMind['lat'], $datosMaxMind['lng'], $datosMaxMind['ip']);
        $stmt->execute();
		// echo'<br>'. $stmt->num_rows;
        $stmt->close();            

    }
    private function arrayCopy( array $array ) {
            $result = array();
            foreach( $array as $key => $val ) {
                if( is_array( $val ) ) {
                    $result[$key] = arrayCopy( $val );
                } elseif ( is_object( $val ) ) {
                    $result[$key] = clone $val;
                } else {
                    $result[$key] = $val;
                }
            }
            return $result;
    }
}
?>