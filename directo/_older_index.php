<meta http-equiv="Content-Type" content="text/html"; charset="utf-8"/> 
<meta name="description" content="description">
<meta name="author" content="Streamgps">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<title>En Directo TV</title>
<link id="favicon" rel="shortcut icon" type="image/png" href="../assets/img/favicon.ico" />
<link href='style.css' rel='stylesheet' type='text/css'>
<script  type="text/javascript" src="https://code.jquery.com/jquery.js" ></script>
<script  type="text/javascript" src="jwplayer/jwplayer.js"></script>
<script  type="text/javascript">jwplayer.key="/0b8ypTK4fChtQcZ8gHP0F1pkzHm/BgfA/96JvTYAuw=";</script>
<script	type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script> 
<script  type="text/javascript" src="js.cookie.js"></script>		
<script  type="text/javascript" src="script.js"></script>
<?php 
	include 'query.php';
?>

<script  type="text/javascript">

/*****************************************************************
	@Descripcion		array de php a js / datos basicos
*****************************************************************/

	var info=<?php echo json_encode($datos);?>;
	// console.log(info); // comprobamos los valores del array
	
    var ads=<?php echo json_encode($datosPubli);?>;
	// console.log(ads); // comprobamos los valores del array
 	
	var totalPubli = (ads.length-1); 
	// console.log( "medida totalPubli: " +  totalPubli);   
	
	var ipUsuario=<?php echo json_encode($ipUsuario);?>;
	// console.log("ipUsuario: "+ ipUsuario);
	
	var esMovil;
	
	
	 /*****************************************************************
	@Descripcion		Primera ejecucion
	
****************************************************************/
	jQuery(document).ready(function() {
		$("#overlay").css("display", "block");
		var myVar = setInterval(pantallaResize, 50);
		initIndex();
	});
	

 	/*****************************************************************
		@Descripcion		
		
		Primero comprobamos si hay alguna poblacion bloqueada para el subdominio.
		Si -> Tendremos que saber si es un movil o un pc , necesitaremos una ip o una 'poblacion (provincia)'.
		No-> 
		
	*****************************************************************/ 
	function initIndex(){
		// console.log("la ip del cliente es: " + ipUsuario);
		// console.log("EMPEZAMOS init() ");
			
		$.ajax({
			async:true, 
			cache:false,
			type: 'GET',
			// data: { 				'namePoblacion' :city, 				'nameProvincia' : prov			},
			url:  'io/hayPoblacionesBloqueadas.php', 
			success: function(response){
				// console.log("Miramos si hay poblaciones bloqueadas ");	
				// console.log(response);
				hayPoblacionesBloqueadas =  $.parseJSON(response);	
				
				// Si hay poblaciones bloqueadas, miraremos si es un movil o un pc, sino jwplayer directamente
				if(hayPoblacionesBloqueadas == true){
					 esMovil = detectarMovilPC();
					 if(esMovil == 1){ // es movil
							// console.log("Soy movil ");
							alert("eres mvkl");
							$("#player_no").css("display","block");
							$("#player_no p").html("Necesitamos  permiso para geolocalizar su posici"+min_o+"n" );
							initGeolocation();
					 }else{ // es pc
						//  console.log("Soy pc, ahora mirare la ip");
						 if(ipUsuario != ""){
							// console.log("Tengo la ip: " + ipUsuario + " esta en la bd stats_ciudades");
							//para puebas si estoy en localhost el maxmind no me lo recoge
							if(ipUsuario == '127.0.0.1') ipUsuario ='88.87.197.114';
							// if(ipUsuario == '88.87.197.114') ipUsuario ='87.218.191.173';							
							comprobar_ip(ipUsuario);
						 }else{
							console.log("No tengo ip, ahora que?? ");							 
						 }
					 }
						
				}else{
					// Si no hay poblaciones bloqueadas llamamos directamente al jwplayer
					$("#player_no").css("display","none");
					loadPlayer(); // y fin, no hay que hacer nada más
				}
			
			} // fin success
		});
	}
	
	
	/*****************************************************************
		@Descripcion		
		
	*****************************************************************/ 
	function loadPlayer(){
		mostrarPublicidad();
		// console.log(" loadPlayer() ");
		// console.log(" loadPlayer("+info+") ");
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();

		if(dd<10) {
			dd='0'+dd
		} 

		if(mm<10) {
			mm='0'+mm
		} 
		
		today = yyyy+mm+dd;
		document.title =info[0].title;
		
		

		player=jwplayer('playertdGAEmcmjOwn');
		var poster="";
		fileRtsp=info[0].fileRtsp;
		titlePlayer=info[0].title;
		logoPlayer=info[0].logoPlayer;
		autoStartPlayer=info[0].autostart;
		logoPosition=info[0].logoPosition;
		_mediaId=location.hostname;
 
		if(detectmob()){
			fileHls=info[0].fileHlsMob;
		}else{
			fileHls=info[0].fileHls;
		}
		
		if(poster){
			poster=info[0].poster;
		}
			
		
		if(location.hostname=="tv.streamgps.com"){
			fileRtsp="rtsp://54.154.222.51:1935/livecentral/streamgps";
			titlePlayer="StreamGPS";
			logoPlayer="Logo-StreamGPS_mosca_2.png";
			autoStartPlayer="true";
			logoPosition="top-right";
		
			if(detectmob()){
				fileHls="http://54.154.222.51:1935/livecentral/ngrp:streamgps_mobile/playlist.m3u8";
			}else{
				fileHls="http://54.154.222.51:1935/livecentral/ngrp:streamgps_all/playlist.m3u8";
			}
		}

				
		player.setup({
			playlist: [{
				sources: [
					{ file: fileHls },
					{ file: fileRtsp }
				],				
				image:poster
			}],
			title: titlePlayer,
			width: '100%',
			stretching:"exactfit",
			aspectratio: '16:9',
			skin: 'glow',
			autostart: autoStartPlayer,
			androidhls:'true',
			logo: {
				file: logoPlayer,
				position:logoPosition
			},
			ga: {} 
		});
		
			
		// jwPlayer evento resize
		player.on('resize',function(event) {			
			var screenW = jQuery(window).width();  // Tamaño width ventana del navegador (anchura)
			var screenH = jQuery(window).height(); // Tamaño height ventana del navegador (altura)
			topOverlay = $("#overlay").css("top");
			numOverlayTop = topOverlay.slice(0, -2);  
      
			screenW_aux = screenW;
			screenH_aux = screenH;
          
			//Medidas del visor
			var visorW = $("#playertdGAEmcmjOwn").width();
			var visorH = $("#playertdGAEmcmjOwn").height(); 
			                 
			//Top del contenedor del banner publicitario, sera resta height (visor - boxBanner - 7px)
			var img_H = $("#img_publi").height(); 
			var topBanner = ( visorH - (img_H + controlbar_H ) )+ 5;          
			$("#overlay").css("top", topBanner+"px");
		});

		
		// jwPlayer evento error
		player.on('error',function(event) {  
			if(etokm!=event.message){
				etokm=event.message;
				etok = S4()+S4();
				loadS("error",0,etok,event.message);
			}
			
			var timeout=setTimeout(function(){
				player.load({file:  fileHls});
				player.play(); 
			},15000);
		});
		
		// jwPlayer evento 
		player.on('play', function(event){
			tempo=0;
			randtoken=S4()+S4();
			
			if (loadTime!=0){
				loadS("play",0,randtoken,loadTime);
				loadTime=0;
			};
 
			timer = setInterval(vTimer, 60000);
			mostrarPublicidad();  
 
		});

		// jwPlayer evento  
		player.on('firstFrame', function(event){
			loadTime=event.loadTime;
			tempo=30;
			loadS("play",tempo,randtoken,event.loadTime);
		});
	
		// jwPlayer evento  
		player.on('visualQuality', function(event){
			loadS("visualQuality",0,0,event.label);
		});

		// jwPlayer evento 
		player.on('buffer', function(event){
			clearInterval(vTimer);
			
			if(event.oldstate=="playing"){
				loadS("playing",tempo,randtoken);
			}
  
			tempo=0;
			randtoken=0;
			// console.log("buffer video:"+event.reason);
  
			if(event.reason!="loading"){
				loadS("buffer",0,0,event.reason);
			}
		});

		
		// jwPlayer evento  
		player.on('bufferChange', function(event){  
			if(event.buffer==0){
				loadS("buffering",0,0);
			}
		});

		
		// jwPlayer evento 
		player.on('fullscreen', function(event){
			if(event.fullscreen==true){
				// console.log("IN fullscreen");
				var overlay= document.getElementById('cont_publicidad');
				var video= document.getElementById('playertdGAEmcmjOwn'); //v
				
				video.addEventListener('progress', function() {
					var show= video.currentTime>=5 && video.currentTime<10;
					overlay.style.visibility= show? 'visible' : 'visible';
				}, false);
				
				var img_H = $("#img_publi").height(); 
				subirMarginTopOverlay=player.getSafeRegion().height-img_H-controlbar_H; 				
				var screenH = jQuery(window).height();
			}else{
				  //$("#overlay").css("marginTop", "-120px"); // margin-top: 638px;
			}
		});
			
			
		// jwPlayer evento
		player.on('pause', function(event){
			clearInterval(stopPublicidad);//paro la publicidad
			clearInterval(vTimer);
			
			if(event.oldstate=="playing"){
				loadS("playing",tempo,randtoken);
			}
  
			tempo=0;
			randtoken=0;
			loadS("pause",0);
		});

		
		// jwPlayer evento 
		player.on('idle', function(event){
			clearInterval(vTimer);
			
			if(event.oldstate=="playing"){
				loadS("playing",tempo,randtoken);
			}
  
			tempo=0;
			randtoken=0;
				
		});

}


 
</script>

	<div id="result"></div>
	<div id='playertdGAEmcmjOwn'  style="display:block;" ></div>
	<div id='player_no'  style="display:none;" > <p> </p> </div>
	<div id="overlay" >
		<section id="cont_publi">
			<a id="a_publi" href="#">
				<img id="img_publi" src="img/publi0.png" class="">
			</a>
		</section>
	</div>
	<br>	