<link href='style.css' rel='stylesheet' type='text/css'>
<link id="favicon" rel="shortcut icon" type="image/png" href="../assets/img/favicon.ico" />
<script  type="text/javascript" src="https://code.jquery.com/jquery.js" ></script>
<script  type="text/javascript" src="jwplayer/jwplayer.js"></script>
<script  type="text/javascript">jwplayer.key="/0b8ypTK4fChtQcZ8gHP0F1pkzHm/BgfA/96JvTYAuw=";</script>
<script  type="text/javascript" src="js.cookie.js"></script>
<script  type="text/javascript" src="script.js"></script>
<?php 
	include 'query.php';
?>
	
<script  type="text/javascript">
/*****************************************************************
	@Descripcion		array de php a js
*****************************************************************/
    var info=<?php echo json_encode($datos);?>;
	// console.log(info); // comprobamos los valores del array
	
    var ads=<?php echo json_encode($datosPubli);?>;
	// console.log(ads); // comprobamos los valores del array
 	var totalPubli = (ads.length-1); 
	// console.log( "medida totalPubli: " +  totalPubli);   
	 
	 var ipUsuario=<?php echo json_encode($ipUsuario);?>;
	//  console.log("ipUsuario: "+ ipUsuario);
	
 	

	   
 /*****************************************************************
	@Descripcion		Primera ejecucion
	
****************************************************************/
	jQuery(document).ready(function() {
		$("#overlay").css("display", "none");
		var myVar = setInterval(pantallaResize, 50);
		initIndexMobile();
	});
	
 /*****************************************************************
	@Descripcion		
	
*****************************************************************/ 
	function initIndexMobile(){

		if ( info[0].fileHlsAdmin != '') {
			$("#player_no").css("display","none");	
			loadPlayer();
		}else{
			$("#player_no").css("display","block");
			$("#player_no p").html("Emisi"+min_o+"n no disponible desde tu poblaci"+min_o+"n  " );
		}		
	}


/*****************************************************************
	@Descripcion		
		
*****************************************************************/ 

	function loadPlayer(){
				// mostrarPublicidad();
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();

		if(dd<10) {
			dd='0'+dd
		} 

		if(mm<10) {
			mm='0'+mm
		} 
		
		today = yyyy+mm+dd;
		document.title =info[0].title;
	

		player=jwplayer('playertdGAEmcmjOwn');
		_mediaId=location.hostname;
		
		player.setup({
			playlist: [{
				sources: [{ file:  info[0].fileHlsAdmin},],				
				image:info[0].poster
			}],
			title: info[0].title,
			width: '100%',
			stretching:"exactfit",
			aspectratio: '16:9',
			skin: 'glow',
			autostart: info[0].autostart,
			androidhls:'true',
			logo: {
				file: info[0].logoPlayer,
				position:info[0].logoPosition
			},
			ga: {} 
		});
	
		
		// jwPlayer evento resize 
		player.on('resize',function(event) {			
			var screenW = jQuery(window).width();  // Tamaño width ventana del navegador (anchura)
			var screenH = jQuery(window).height(); // Tamaño height ventana del navegador (altura)
			topOverlay = $("#overlay").css("top");
			numOverlayTop = topOverlay.slice(0, -2);  
      
			screenW_aux = screenW;
			screenH_aux = screenH;
          
			//Medidas del visor
			var visorW = $("#playertdGAEmcmjOwn").width();
			var visorH = $("#playertdGAEmcmjOwn").height(); 
			                 
			//Top del contenedor del banner publicitario, sera resta height (visor - boxBanner - 7px)
			var img_H = $("#img_publi").height(); 
			var topBanner = ( visorH - (img_H + controlbar_H ) )+ 5;          
			$("#overlay").css("top", topBanner+"px");
		});

		
		// jwPlayer evento error
		player.on('error',function(event) {  
			if(etokm!=event.message){
				etokm=event.message;
				etok = S4()+S4();
				loadS("error",0,etok,event.message);
			}
			
			var timeout=setTimeout(function(){
				player.load({file:  info[0].fileHlsAdmin});
				player.play(); 
			},15000);
		});

		
		// jwPlayer evento 
		player.on('play', function(event){
			tempo=0;
			randtoken=S4()+S4();
			
			if (loadTime!=0){
				loadS("play",0,randtoken,loadTime);
				loadTime=0;
			};
 
			timer = setInterval(vTimer, 60000);
			// mostrarPublicidad();  
 
		});

		// jwPlayer evento  
		player.on('firstFrame', function(event){
			loadTime=event.loadTime;
			tempo=30;
			loadS("play",tempo,randtoken,event.loadTime);
		});

		
		// jwPlayer evento  
		player.on('visualQuality', function(event){
			loadS("visualQuality",0,0,event.label);
		});

		
		// jwPlayer evento 
		player.on('buffer', function(event){
			clearInterval(vTimer);
			
			if(event.oldstate=="playing"){
				loadS("playing",tempo,randtoken);
			}
  
			tempo=0;
			randtoken=0;
			// console.log("buffer video:"+event.reason);
  
			if(event.reason!="loading"){
				loadS("buffer",0,0,event.reason);
			}
		});

		
		/* jwPlayer evento  */
		player.on('bufferChange', function(event){  
			if(event.buffer==0){
				loadS("buffering",0,0);
			}
		});

		
		// jwPlayer evento 
		player.on('fullscreen', function(event){
			if(event.fullscreen==true){
				// console.log("IN fullscreen");
				var overlay= document.getElementById('cont_publicidad');
				var video= document.getElementById('playertdGAEmcmjOwn'); //v
				
				video.addEventListener('progress', function() {
					var show= video.currentTime>=5 && video.currentTime<10;
					overlay.style.visibility= show? 'visible' : 'visible';
				}, false);
				
				var img_H = $("#img_publi").height(); 
				subirMarginTopOverlay=player.getSafeRegion().height-img_H-controlbar_H; 				
				var screenH = jQuery(window).height();
			}else{
				 $("#overlay").css("marginTop", "-120px"); // margin-top: 638px;
			}
		});
			
			
		// jwPlayer evento
		player.on('pause', function(event){
			clearInterval(stopPublicidad);//paro la publicidad
			clearInterval(vTimer);
			
			if(event.oldstate=="playing"){
				loadS("playing",tempo,randtoken);
			}
  
			tempo=0;
			randtoken=0;
			loadS("pause",0);
		});

		
		/* jwPlayer evento  */
		player.on('idle', function(event){
			clearInterval(vTimer);
			
			if(event.oldstate=="playing"){
				loadS("playing",tempo,randtoken);
			}
  
			tempo=0;
			randtoken=0;
				
		});
	}

		
		
	</script>
	
	<div id='playertdGAEmcmjOwn'  style="display:block;" ></div>
	<div id='player_no'  style="display:none;" > <p> </p> </div>
	
	<div id="overlay" style="display:none;"  >
		<section id="cont_publi">
			<a id="a_publi" href="#">
				<img id="img_publi" src="img/publi0.png" class="">
			</a>
		</section>
	
	</div>
	<br>	