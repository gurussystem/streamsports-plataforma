<?php
	header('Content-Type: text/html; charset=UTF-8');
	// echo "<br> entre en index_ebav2.php <br>a";
	$mivarPhp = "Asignado en PHP";
	// echo "<br> ". $mivarPhp ;

	include_once dirname(__FILE__) . '/DbHandler.php';
	// echo "<br>  sali de DbHandler";

	$db = new DbHandler();
	// echo "<br>  db";

	//recogemos datos usuario
	$arraydom=split('[.]', $_SERVER['HTTP_HOST']);
	$subDominio=$arraydom[0];
	// echo "<br>  subDominio: ". $subDominio;

	$datosPlayer=$db->recogerDatosPlayer($subDominio);

	$datosPubli='';
	$permtirVerPartido=1;
	$ipCliente=getRealIP();

	//tiene geobloqueo activado y la ip no es proxy?
	if($datosPlayer['geoBloqueo'] && $ipCliente!='unknown'){
		//si:
		$datosDb=$db->ipInDb($ipCliente);
		//la ip esta en la bd?
		if($datosDb===null){
			//no:
			//buscamos en servicio maxmind
			$datosMaxMind=$db->recogerDatosMaxMind($ipCliente);
			//ciudad/provincia/CP están vacios?
			if($datosMaxMind['enontrado']==0){
				//si:
				//No podemos decidir aquí. tenemos que hacer gelocalización en javascript.
				$permtirVerPartido=-1;
			}else{
				//no:
				$db->saveIpDb($datosMaxMind);
				$permtirVerPartido=$db->poblacionBloqueda($datosMaxMind,$datosPlayer['id']);
			}
		}else{
			//si:
			$permtirVerPartido=$db->poblacionBloqueda($datosDb,$datosPlayer['id']);
		}
	} // fin if


	if($permtirVerPartido!=0){
		//recoger datos publi:
		$datosPubli=$db->recogerDatosPubli($datosPlayer['id']);
	}


	/*
	Esta funcion peta desde pc por lo menos
	*/
	function getRealIP(){
		// echo "<br> getRealIP <br>";
		// echo "<pre> " ; print_r($_SERVER); echo "</pre>";

		if( isset($_SERVER['HTTP_X_FORWARDED_FOR']) )  { //No existe 'HTTP_X_FORWARDED_FOR'
			// echo 'Hay proxy';
			$client_ip = ( !empty($_SERVER['REMOTE_ADDR']) ) ? $_SERVER['REMOTE_ADDR'] : ( ( !empty($_ENV['REMOTE_ADDR']) ) ? $_ENV['REMOTE_ADDR'] :  "unknown" );

			// los proxys van añadiendo al final de esta cabecera
			// las direcciones ip que van "ocultando". Para localizar la ip real
			// del usuario se comienza a mirar por el principio hasta encontrar
			// una dirección ip que no sea del rango privado. En caso de no
			// encontrarse ninguna se toma como valor el REMOTE_ADDR
			$entries = preg_split('/[, ]/', $_SERVER['HTTP_X_FORWARDED_FOR']);

			reset($entries);
			while (list(, $entry) = each($entries)) {
				$entry = trim($entry);
				if ( preg_match("/^([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/", $entry, $ip_list) ) {
					// http://www.faqs.org/rfcs/rfc1918.html
					$private_ip = array(
					  '/^0\./',
					  '/^127\.0\.0\.1/',
					  '/^192\.168\..*/',
					  '/^172\.((1[6-9])|(2[0-9])|(3[0-1]))\..*/',
					  '/^10\..*/'
					);

					$found_ip = preg_replace($private_ip, $client_ip, $ip_list[1]);

					if ($client_ip != $found_ip){
						$client_ip = $found_ip;
						break;
					}
				}
			} // fin while
		}else{

			// echo 'No hay proxy';
			$client_ip = ( !empty($_SERVER['REMOTE_ADDR']) ) ? $_SERVER['REMOTE_ADDR'] : ( ( !empty($_ENV['REMOTE_ADDR']) ) ? $_ENV['REMOTE_ADDR'] : "unknown" );
		}

		//	echo "<br> client_ip: ". $client_ip ."<br>";
		return $client_ip;

	} // fin funcion getRealIP
?>

<meta http-equiv="Content-Type" content="text/html"; charset="utf-8"/>
<meta name="description" content="description">
<meta name="author" content="Streamgps">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<title>En Directo TV</title>
<link href='style.css' rel='stylesheet' type='text/css'>
<link href='styleVisor.css' rel='stylesheet' type='text/css'>
<script  type="text/javascript" src="https://code.jquery.com/jquery.js" ></script>
<script  type="text/javascript" src="jwplayer/jwplayer.js"></script>
<script  type="text/javascript">jwplayer.key="/0b8ypTK4fChtQcZ8gHP0F1pkzHm/BgfA/96JvTYAuw=";</script>
<script  type="text/javascript" src="js.cookie.js"></script>
<script  type="text/javascript" src="script.js"></script>

<script>

	var permtirVerPartido = '<?php echo $permtirVerPartido; ?>';
	// console.log("permtirVerPartido: " + permtirVerPartido);

	var info_objeto = '<?php echo json_encode($datosPlayer) ?>'; //usamos json_encode porque es un array
	// console.log("ARRAY INFORMACION"); console.log(info_objeto); // comprobamos los valores del array

	//Convertimos de json_ a array
	var info = $.parseJSON(info_objeto);
	// console.log(info);

	var ads_objeto = '<?php echo json_encode($datosPubli) ?>'; //usamos json_encode porque es un array
	// console.log("ARRAY PUBLICIDAD"); console.log(ads); // comprobamos los valores del array

	//Convertimos de json_ a array
	var ads = $.parseJSON(ads_objeto);

	var totalPubli = (ads.length-1);
	// console.log( "medida totalPubli: " +  totalPubli);


	// Primera ejecucion
	jQuery(document).ready(function() {
		$("#overlay").css("display", "none"); // block
		var myVar = setInterval(pantallaResize, 50);
		initIndex();
	});

	/*
		@Descripcion

		Primero comprobamos si hay alguna poblacion bloqueada para el subdominio.
		Si -> Tendremos que saber si es un movil o un pc , necesitaremos una ip o una 'poblacion (provincia)'.
		No->
	*/

	function initIndex(){
		// console.log("EMPEZAMOS initIndex() ");
		//permtirVerPartido=-1;
		//  console.log("tengo que entrar en el caso : " + permtirVerPartido);
		switch(permtirVerPartido){
			case '1': //mostrar  player con datos
				// console.log("estoy en caso 1");
				comprobacionesPreviasAlPartido();

				if(estadoPartido_live == 1){
					loadPlayer();
					$("#cont_infoProximPartit").css("display", "none");
				}else{
					datos_proximoPartido();
				}

				break;

			case '0': //mostrar texto "Esta emisión no está disponible desde su localización."
				// console.log("estoy en caso 0");
				$("#player_no").css("display","block");
				$("#cont_infoProximPartit").css("display","none");
				$("#player_no p").html("Lo sentimos, tu población tiene restringida la emisión del partido para tu localidad. " );
				break;

			case '-1': // ( caso -1) hacemos que en el body init se active la geolocalización de javscript.
				// console.log("estoy en caso -1");
				$("#player_no").css("display","block");
				$("#player_no p").html("Necesitamos  permiso para geolocalizar su posici"+min_o+"n" );
				initGeolocation();
				break;

		} // fin switch

		// console.log("fin switch");
	} // fin funcion initIndex



	// Si el live me devuelve 1 cargo el play directamente, sino compruebo la hora
	function comprobacionesPreviasAlPartido(){

		$.ajax({
			async:true,
			cache:false,
			type: 'GET',
			data: {
				 'idUsuario' : info.id
			},
			url:  'https://'+location.hostname+'/assets/io/proximoPartido/estadoPartido.php',
			success: function(response){
				estadoPartido =  $.parseJSON(response);
				// console.log("partido live: " + estadoPartido[0].live);
				// console.log("numResult: " + estadoPartido[0].numResult);
				estadoPartido_live =  estadoPartido[0].live;

			} // fin success
		});

	}	// fin comprobacionesPreviasAlPartido

	/*
		@Descripcion	visor jwplayer

	*/
	function loadPlayer(){
		// mostrarPublicidad();
		// console.log(" loadPlayer() ");
		// console.log(info);

		// console.log(" loadPlayer("+info+") ");
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();

		if(dd<10) {
			dd='0'+dd;
		}

		if(mm<10) {
			mm='0'+mm;
		}

		today = yyyy+mm+dd;
		// console.log("today: " + today)
		document.title =info.title;
		player=jwplayer('playertdGAEmcmjOwn');
		// _mediaId=location.hostname; // estp para que es??

		// esto lo pongo directo??
		if(detectmob()){
			fileHls=info.fileHlsMob;
		}else{
			fileHls=info.fileHls;
		}

		// console.log("fileHls: " + info.fileHls)	;

		player.setup({
			playlist: [{
				sources: [
					{ file: fileHls },
					{ file: info.fileRtsp }
				],
				image:info.poster
			}],
			title: info.title,
			width: '100%',
			stretching:"exactfit",
			aspectratio: '16:9',
			skin: 'glow',
			autostart: info.autostart,
			androidhls:'true',
			logo: {
				file: info.logoPlayer,
				position:info.logoPosition
			},
			ga: {}
		});


		// jwPlayer evento resize
		player.on('resize',function(event) {
		// console.log("player reize"); console.log(event);
			var screenW = jQuery(window).width();  // Tamaño width ventana del navegador (anchura)
			var screenH = jQuery(window).height(); // Tamaño height ventana del navegador (altura)
			topOverlay = $("#overlay").css("top");
			numOverlayTop = topOverlay.slice(0, -2);

			screenW_aux = screenW;
			screenH_aux = screenH;

			//Medidas del visor
			var visorW = $("#playertdGAEmcmjOwn").width();
			var visorH = $("#playertdGAEmcmjOwn").height();

			//Top del contenedor del banner publicitario, sera resta height (visor - boxBanner - 7px)
			var img_H = $("#img_publi").height();
			var topBanner = ( visorH - (img_H + controlbar_H ) )+ 5;
			$("#overlay").css("top", topBanner+"px");
		});


		// jwPlayer evento error
		player.on('error',function(event) {
			// console.log("player error"); console.log(event);
			if(etokm!=event.message){
				etokm=event.message;
				etok = S4()+S4();
				loadS("error",0,etok,event.message);
				mostrarDatos_proximoPartido();
			}

			var timeout=setTimeout(function(){
				player.load({file:  fileHls});
				player.play();
			},15000);
		});

		// jwPlayer evento
		player.on('play', function(event){
		//	console.log("player play"); console.log(event);
			tempo=0;
			randtoken=S4()+S4();

			if (loadTime!=0){
				loadS("play",0,randtoken,loadTime);
				loadTime=0;
			};
			esconderDatos_proximoPartido();

			timer = setInterval(vTimer, 60000);
			// mostrarPublicidad();

		});

		// jwPlayer evento
		player.on('firstFrame', function(event){
		//	console.log("player firstFrame: "); console.log(event);
			loadTime=event.loadTime;
			tempo=30;
			loadS("play",tempo,randtoken,event.loadTime);
		});

		// jwPlayer evento
		player.on('visualQuality', function(event){
			// console.log("player visualQuality"); console.log(event);
			//loadS("visualQuality",0,0,event.label);
		});

		// jwPlayer evento
		player.on('buffer', function(event){
			// console.log("player buffer :"); console.log(event);
			clearInterval(vTimer);

			if(event.oldstate=="playing"){
				loadS("playing",tempo,randtoken);
			}

			tempo=0;
			randtoken=0;
			// console.log("buffer video:"+event.reason);

			if(event.reason!="loading"){
				//loadS("buffer",0,0,event.reason);
			}
		});


		// jwPlayer evento
		player.on('bufferChange', function(event){
		// console.log("player bufferChange"); console.log(event);
			if(event.buffer==0){
				//loadS("buffering",0,0);
			}
		});


		// jwPlayer evento
		player.on('fullscreen', function(event){
			if(event.fullscreen==true){
				// console.log("IN fullscreen");
				var overlay= document.getElementById('cont_publicidad');
				var video= document.getElementById('playertdGAEmcmjOwn'); //v

				video.addEventListener('progress', function() {
					var show= video.currentTime>=5 && video.currentTime<10;
					overlay.style.visibility= show? 'visible' : 'visible';
				}, false);

				// var img_H = $("#img_publi").height();
				subirMarginTopOverlay=player.getSafeRegion().height-img_H-controlbar_H;
				var screenH = jQuery(window).height();
			}else{
				  $("#overlay").css("marginTop", "-120px"); // margin-top: 638px;
			}
		});


		// jwPlayer evento
		player.on('pause', function(event){
			// console.log("player pause"); console.log(event);
			clearInterval(stopPublicidad);//paro la publicidad
			clearInterval(vTimer);

			if(event.oldstate=="playing"){
				loadS("playing",tempo,randtoken);
			}

			tempo=0;
			randtoken=0;
			loadS("pause",0);
		});


		// jwPlayer evento
		player.on('complete', function(event){
		//	console.log("player complete"); console.log(event);
		});


		// jwPlayer evento
		player.on('playlistComplete', function(event){
		//	console.log("player playlistComplete"); console.log(event);
		});


		// jwPlayer evento
		player.on('idle', function(event){
		//	console.log("player idle"); console.log(event);
			clearInterval(vTimer);

			if(event.oldstate=="playing"){
				loadS("playing",tempo,randtoken);
			}

			tempo=0;
			randtoken=0;

		});


		mostrarDatos_proximoPartido();

}


	function mostrarDatos_proximoPartido(){
		//	console.log("MOSTRAR datos proximo partido");
		$("#infoProximPartit").css("display", "block");
	}

	function esconderDatos_proximoPartido(){
		//	console.log("ESCONDER datos proximo partido");
		$("#infoProximPartit").css("display", "none");
	}


	// tiempo de desfase
	function desfaseHoraria (){
		var desfase_aux = new Date().getTimezoneOffset();

		// si es un numero negativo se suma, si es un numero positivo se resta
		if(desfase_aux < 0 ) {
			return desfase = Math.abs(desfase_aux);
		}else if(desfase_aux > 0 ){
			return desfase = -desfase_aux;
		}else{
			return desfase = 0;
		}
	}

	var infoPartido;
	var estadoPartido_live ;
	function datos_proximoPartido(){
		var tiempoDesfase = desfaseHoraria();

		$.ajax({
			async:true,
			cache:false,
			type: 'GET',
			data: {
				'idUsuario' : info.id,
				'tiempoDesfase' : tiempoDesfase
			},
			url:  'https://'+location.hostname+'/assets/io/proximoPartido/infoPartido.php',
			success: function(response){
				infoPartido =  $.parseJSON(response);
				// console.log(infoPartido);
				//	console.log("numResult: " + infoPartido[0].numResult);

				// Comprobamos que la consulta no ha dado error
				if(infoPartido[0].numRows >= 0){ // No hay datos
					$("#infoProximPartit").css("display", "block");
					if(infoPartido[0].tiempoQueFalta <=15){
						 comprobacionesPreviasAlPartido();
					}

					// console.log("estadoPartido_live (menos 10 mint) : " + estadoPartido_live);
					if(estadoPartido_live == 1){
						loadPlayer();
						$("#cont_infoProximPartit").css("display", "none");
					}else{
						if(infoPartido[0].numRows == 0){
							$("#infoProximPartit").html("No hay emisiones programadas.");
						}else{
							$("#cont_infoProximPartit").css("display", "block");
							$("#infoProximPartit").css("display", "block");

							frasePartido = "<div id='titulo'>Próxima Emisión: </div><br>" + infoPartido[0].tituloPartido + ": " + infoPartido[0].equipoLocal + " - " + infoPartido[0].equipoVisitante + " (" + infoPartido[0].fechaPartido2 + ") <div id='cont_verPartido'> <button id='verPartido' onclick='verPartit()' title='Pulsa para ver el partido, si ha empezado claro!!! ' style='display:none;'> Ver partido </button> </div> <br> <div id='estadop'> </div>";

							$("#infoProximPartit").html(frasePartido);
							if(infoPartido[0].tiempoQueFalta <=15){
								$("#verPartido").css("display", "block");
							}
						}
					}
				}
			} // fin success
		});
	}

	function verPartit(){
		$("#estadop").css("display", "none");

		$.ajax({
			async:true,
			cache:false,
			type: 'GET',
			data: {
				 'idUsuario' : info.id
			},
			url:  'https://'+location.hostname+'/assets/io/proximoPartido/estadoPartido.php',
			success: function(response){
				estadoPartido =  $.parseJSON(response);
				//	console.log(estadoPartido[0].live);

				estadoPartido_live =  estadoPartido[0].live;
				//	console.log("estadoPartido_live (ver partido) : " + estadoPartido_live);
				if(estadoPartido_live ==1){
					loadPlayer();
					$("#cont_infoProximPartit").css("display", "none");
					jwplayer('playertdGAEmcmjOwn').play();
				}else{
					$("#estadop").css("display", "block");
					$("#estadop").html(" Sin Emisión actualmente, intente más tarde.");
				}
			} // fin success
		});
	}

</script>


	<div id="result"></div>
	<div  id="cont_infoProximPartit">
		<div id="infoProximPartit"> </div>

	</div>
	<div id='playertdGAEmcmjOwn'  style="display:block;" ></div>
	<div id='player_no'  style="display:none;" > <p> </p> </div>


	<div id="overlay" style="display:none;" >
		<section id="cont_publi">
			<a id="a_publi" href="#">
				<img id="img_publi" src="img/publi0.png" class="">
			</a>
		</section>
	</div>

	<br>
