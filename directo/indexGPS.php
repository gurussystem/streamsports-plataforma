<?php 
header('Content-Type: text/html; charset=UTF-8'); 
// echo "<br> entre en index_ebav2.php <br>a";
$mivarPhp = "Asignado en PHP";
// echo "<br> ". $mivarPhp ;

include_once dirname(__FILE__) . '/DbHandler.php';

// echo "<br>  sali de DbHandler";

$db = new DbHandler();

// echo "<br>  db";

//recogemos datos usuario
$arraydom=split('[.]', $_SERVER['HTTP_HOST']);
$subDominio=$arraydom[0];
// echo "<br>  subDominio: ". $subDominio;

$datosPlayer=$db->recogerDatosPlayer($subDominio);

$datosPubli='';
$permtirVerPartido=1;
$ipCliente=getRealIP();

//tiene geobloqueo activado y la ip no es proxy?
if($datosPlayer['geoBloqueo'] && $ipCliente!='unknown'){
	//si: 
	$datosDb=$db->ipInDb($ipCliente);
	//la ip esta en la bd?
	if($datosDb===null){
		//no:
		//buscamos en servicio maxmind
		$datosMaxMind=$db->recogerDatosMaxMind($ipCliente);
		//ciudad/provincia/CP están vacios?
		if($datosMaxMind['enontrado']==0){
			//si:
			//No podemos decidir aquí. tenemos que hacer gelocalización en javascript.
			$permtirVerPartido=-1;
		}else{
			//no: 
			$db->saveIpDb($datosMaxMind);
			$permtirVerPartido=$db->poblacionBloqueda($datosMaxMind,$datosPlayer['id']);
		}
	}else{
		//si:
		$permtirVerPartido=$db->poblacionBloqueda($datosDb,$datosPlayer['id']);
	}
}


if($permtirVerPartido!=0){
	//recoger datos publi:
	$datosPubli=$db->recogerDatosPubli($datosPlayer['id']);
}

//
/*
esta funcion peta desde pc por lo menos
*/
function getRealIP(){
 // echo "<br> getRealIP <br>";
 // echo "<pre> " ; print_r($_SERVER); echo "</pre>";
 
   //  if( $_SERVER['HTTP_X_FORWARDED_FOR'] != '' )  {
   if( isset($_SERVER['HTTP_X_FORWARDED_FOR']) )  { //No existe 'HTTP_X_FORWARDED_FOR'
	// echo 'Hay proxy';
	$client_ip = 
         ( !empty($_SERVER['REMOTE_ADDR']) ) ? 
            $_SERVER['REMOTE_ADDR'] 
            : 
            ( ( !empty($_ENV['REMOTE_ADDR']) ) ? 
               $_ENV['REMOTE_ADDR'] 
               : 
               "unknown" );
 
      // los proxys van añadiendo al final de esta cabecera
      // las direcciones ip que van "ocultando". Para localizar la ip real
      // del usuario se comienza a mirar por el principio hasta encontrar 
      // una dirección ip que no sea del rango privado. En caso de no 
      // encontrarse ninguna se toma como valor el REMOTE_ADDR
 
      $entries = preg_split('/[, ]/', $_SERVER['HTTP_X_FORWARDED_FOR']);
 
      reset($entries);
      while (list(, $entry) = each($entries)) 
      {
         $entry = trim($entry);
         if ( preg_match("/^([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/", $entry, $ip_list) )
         {
            // http://www.faqs.org/rfcs/rfc1918.html
            $private_ip = array(
                  '/^0\./', 
                  '/^127\.0\.0\.1/', 
                  '/^192\.168\..*/', 
                  '/^172\.((1[6-9])|(2[0-9])|(3[0-1]))\..*/', 
                  '/^10\..*/');
 
            $found_ip = preg_replace($private_ip, $client_ip, $ip_list[1]);
 
            if ($client_ip != $found_ip)
            {
               $client_ip = $found_ip;
               break;
            }
         }
      }
   }else{
	   
	// echo 'No hay proxy';   
	    $client_ip = 
         ( !empty($_SERVER['REMOTE_ADDR']) ) ? 
            $_SERVER['REMOTE_ADDR'] 
            : 
            ( ( !empty($_ENV['REMOTE_ADDR']) ) ? 
               $_ENV['REMOTE_ADDR'] 
               : 
               "unknown" );
   }
 
//	echo "<br> client_ip: ". $client_ip ."<br>";
   return $client_ip;
 
}
?>

<meta http-equiv="Content-Type" content="text/html"; charset="utf-8"/> 
<meta name="description" content="description">
<meta name="author" content="Streamgps">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<title>En Directo TV</title>
<link href='style.css' rel='stylesheet' type='text/css'>
<link href='styleVisor.css' rel='stylesheet' type='text/css'>
<script  type="text/javascript" src="https://code.jquery.com/jquery.js" ></script>
<script  type="text/javascript" src="jwplayer/jwplayer.js"></script>
<script  type="text/javascript">jwplayer.key="/0b8ypTK4fChtQcZ8gHP0F1pkzHm/BgfA/96JvTYAuw=";</script>
<script	type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script> 
<script  type="text/javascript" src="js.cookie.js"></script>		
<script  type="text/javascript" src="script.js"></script>
<script  type="text/javascript" >
/*****************************************************************
	@Descripcion		array de php a js / datos basicos
*****************************************************************/
	var permtirVerPartido = '<?php echo $permtirVerPartido; ?>';
	// console.log("permtirVerPartido: " + permtirVerPartido);
	
	var info_objeto = '<?php echo json_encode($datosPlayer) ?>'; //usamos json_encode porque es un array
	// console.log("ARRAY INFORMACION"); console.log(info_objeto); // comprobamos los valores del array
	
	//Convertimos de json_ a array
	var info = $.parseJSON(info_objeto);
	// console.log(info);
	
	var ads_objeto = '<?php echo json_encode($datosPubli) ?>'; //usamos json_encode porque es un array
	// console.log("ARRAY PUBLICIDAD"); console.log(ads); // comprobamos los valores del array
	
	//Convertimos de json_ a array
	var ads = $.parseJSON(ads_objeto);
	
	var totalPubli = (ads.length-1); 
	// console.log( "medida totalPubli: " +  totalPubli);   
	
/*****************************************************************
	@Descripcion		primera ejecucion
*****************************************************************/	
	jQuery(document).ready(function() {
		$("#overlay").css("display", "block");
		var myVar = setInterval(pantallaResize, 50);
		initIndex();
	});
	
	/*****************************************************************
		@Descripcion		
		
		Primero comprobamos si hay alguna poblacion bloqueada para el subdominio.
		Si -> Tendremos que saber si es un movil o un pc , necesitaremos una ip o una 'poblacion (provincia)'.
		No-> 
		
	*****************************************************************/ 
	function initIndex(){
			loadPlayer();		
			
		
	}
	
	

	// Si el live me devuelve 1 cargo el play directamente, sino compruebo la hora		
	function comprobacionesPreviasAlPartido(){
				
		$.ajax({
			async:true, 
			cache:false,
			type: 'GET',
			data: { 
				 'idUsuario' : info.id								
			},
			url:  'http://'+location.hostname+'/assets/io/proximoPartido/estadoPartido.php', 
			success: function(response){
				estadoPartido =  $.parseJSON(response);	
			//	console.log(estadoPartido[0].live);
			//	console.log("numResult: " + infoPartido[0].numResult);
				estadoPartido_live =  estadoPartido[0].live;

			} // fin success
		});	
		
	}	
	
	/*****************************************************************
		@Descripcion	visor jwplayer	
		
	*****************************************************************/ 
	function loadPlayer(){
		// mostrarPublicidad();
		// console.log(" loadPlayer() ");
		// console.log(info);
		
		// console.log(" loadPlayer("+info+") ");
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();

		if(dd<10) { 
			dd='0'+dd;
		} 

		if(mm<10) {
			mm='0'+mm;
		} 
		
		today = yyyy+mm+dd;
		// console.log("today: " + today)
		document.title =info.title;
		player=jwplayer('playertdGAEmcmjOwn');		
		// _mediaId=location.hostname; // estp para que es?? 
 
		// esto lo pongo directo?? 
		if(detectmob()){
			fileHls=info.fileHlsMob;
		}else{
			fileHls=info.fileHls;
		}
			
		// console.log("fileHls: " + info.fileHls)	;
		var fileA = "rtmp://54.229.60.25:1935/vods3/_definst_/mp4:amazons3/archivos.streamgps.com/empresas/20/56/20_56_2016-03-04-05.50.52.034.mp4" ;
		// console.log("fileHls: " + info.fileHls)	;
		player.setup({
			playlist: [{
				sources: [
					{ file: fileA },
					{ file: info.fileRtsp }
				],				
				image:info.poster
			}],
			title: info.title,
			width: '100%',
			stretching:"exactfit",
			aspectratio: '16:9',
			skin: 'glow',
			autostart: info.autostart,
			androidhls:'true',
			logo: {
				file: info.logoPlayer,
				position:info.logoPosition
			},
			ga: {} 
		});
		
			
		// jwPlayer evento resize
		player.on('resize',function(event) {	
		// console.log("player reize"); console.log(event);		
			var screenW = jQuery(window).width();  // Tamaño width ventana del navegador (anchura)
			var screenH = jQuery(window).height(); // Tamaño height ventana del navegador (altura)
			topOverlay = $("#overlay").css("top");
			numOverlayTop = topOverlay.slice(0, -2);  
      
			screenW_aux = screenW;
			screenH_aux = screenH;
          
			//Medidas del visor
			var visorW = $("#playertdGAEmcmjOwn").width();
			var visorH = $("#playertdGAEmcmjOwn").height(); 
			                 
			//Top del contenedor del banner publicitario, sera resta height (visor - boxBanner - 7px)
			var img_H = $("#img_publi").height(); 
			var topBanner = ( visorH - (img_H + controlbar_H ) )+ 5;          
			$("#overlay").css("top", topBanner+"px");
		});

		
		// jwPlayer evento error
		player.on('error',function(event) {  
			// console.log("player error"); console.log(event);
			if(etokm!=event.message){
				etokm=event.message;
				etok = S4()+S4();
				loadS("error",0,etok,event.message);
				mostrarDatos_proximoPartido();
			}
						
			var timeout=setTimeout(function(){
				player.load({file:  fileHls});
				player.play(); 
			},15000);
		});
		
		// jwPlayer evento 
		player.on('play', function(event){
		//	console.log("player play"); console.log(event);
			tempo=0;
			randtoken=S4()+S4();
						
			if (loadTime!=0){
				loadS("play",0,randtoken,loadTime);
				loadTime=0;
			};
			esconderDatos_proximoPartido();
			
			timer = setInterval(vTimer, 60000);
			mostrarPublicidad();  
 
		});

		// jwPlayer evento  
		player.on('firstFrame', function(event){
		//	console.log("player firstFrame: "); console.log(event);
			loadTime=event.loadTime;
			tempo=30;
			loadS("play",tempo,randtoken,event.loadTime);
		});
	
		// jwPlayer evento  
		player.on('visualQuality', function(event){
			// console.log("player visualQuality"); console.log(event);
			loadS("visualQuality",0,0,event.label);
		});

		// jwPlayer evento 
		player.on('buffer', function(event){
			// console.log("player buffer :"); console.log(event);
			clearInterval(vTimer);
			
			if(event.oldstate=="playing"){
				loadS("playing",tempo,randtoken);
			}
  
			tempo=0;
			randtoken=0;
			// console.log("buffer video:"+event.reason);
  
			if(event.reason!="loading"){
				loadS("buffer",0,0,event.reason);
			}
		});

		
		// jwPlayer evento  
		player.on('bufferChange', function(event){  
		// console.log("player bufferChange"); console.log(event);
			if(event.buffer==0){
				loadS("buffering",0,0);
			}
		});

		
		// jwPlayer evento 
		player.on('fullscreen', function(event){
			if(event.fullscreen==true){
				// console.log("IN fullscreen");
				var overlay= document.getElementById('cont_publicidad');
				var video= document.getElementById('playertdGAEmcmjOwn'); //v
				
				video.addEventListener('progress', function() {
					var show= video.currentTime>=5 && video.currentTime<10;
					overlay.style.visibility= show? 'visible' : 'visible';
				}, false);
				
				var img_H = $("#img_publi").height(); 
				subirMarginTopOverlay=player.getSafeRegion().height-img_H-controlbar_H; 				
				var screenH = jQuery(window).height();
			}else{
				  //$("#overlay").css("marginTop", "-120px"); // margin-top: 638px;
			}
		});
			
			
		// jwPlayer evento
		player.on('pause', function(event){
			// console.log("player pause"); console.log(event);
			clearInterval(stopPublicidad);//paro la publicidad
			clearInterval(vTimer);
			
			if(event.oldstate=="playing"){
				loadS("playing",tempo,randtoken);
			}
  
			tempo=0;
			randtoken=0;
			loadS("pause",0);
		});

		
		// jwPlayer evento 
		player.on('complete', function(event){
		//	console.log("player complete"); console.log(event);
		});
		
		
		// jwPlayer evento 
		player.on('playlistComplete', function(event){
		//	console.log("player playlistComplete"); console.log(event);
		});
		
		
		// jwPlayer evento 
		player.on('idle', function(event){
		//	console.log("player idle"); console.log(event);
			clearInterval(vTimer);
			
			if(event.oldstate=="playing"){
				loadS("playing",tempo,randtoken);
			}
  
			tempo=0;
			randtoken=0;
				
		});
		
		
		mostrarDatos_proximoPartido();

}




// tiempo de desfase
	function desfaseHoraria (){
		var desfase_aux = new Date().getTimezoneOffset();
		
		// si es un numero negativo se suma, si es un numero positivo se resta
		if(desfase_aux < 0 ) {
			return desfase = Math.abs(desfase_aux);		  
		}else if(desfase_aux > 0 ){
			return desfase = -desfase_aux;
		}else{
			return desfase = 0;
		}
	}




</script>


	
	
	<div id='playertdGAEmcmjOwn'  style="display:block;" ></div>
	
	
	
	
	
	
		</section>
	</div>
	<br>	