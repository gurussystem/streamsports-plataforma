<?php
    header("Content-Type: text/html;charset=utf-8");
	//require_once 'vendor/autoload.php';
	require_once '../assets/vendor/autoload.php';  
	use GeoIp2\WebService\Client;
	
	# Conectamos con la BD
	include '../assets/io/conexion.php';
	include '../assets/io/funciones.php';

	$idPartido = comprobarParametros ('idPartido');
	//echo "<br> idPartido: " . $idPartido;

	# Definicion de arrays
	$data = array();
	$tiemposPartido = array();
	$tiemposPartido_utc = array();
	$contadorUsuarios = array();
	$idUsuariosConexion = array();
	$usuarios = array();
	$tiempoMinimo = 0;
	$contador = 0;

	# Ejecutamos la consulta 
	$stmt = $mysqli->prepare("call stats_streamsports(?)");
	$stmt->bind_param("i",  $idPartido);	
	$stmt->execute(); 
	$result = $stmt->get_result();	
	
	# Si no hay datos paramos.
	if($result->num_rows <= 0){
		$data['mensaje']="No hay datos para hacer estadisticas.";	
		$data['proceso']=0;	
		echo json_encode($data);
		// echo "<br> No hay estadisticas ";
		die();
	}

	# Evaluamos si ha ido todo bien o habido algun fallo
	if($stmt->errno){
		echo "<br> error: ". $stmt->errno;
	}	
	
	# Usuarios conectados 
	$numConexionesTotales = $result->num_rows;
	$sumaDuracionConexion  = 0;
	while($row=$result->fetch_assoc()) {
		//echo '<pre>'; print_r($row); echo '</pre>';  
		
		# rellenamos el array que utilizaremos en este php
		$usuarios[] = array(
			"idConexion"=>$row["idConexion"], 
			"idUser"=>$row["idUser"],  // id user cliente, ej cornella = 2
			"idUserSession"=>$row["idUserSession"], 
			"hostUser"=>$row["hostUser"], 
			// "titulo"=>utf8_encode($row["titulo"]) ,	
			"titulo"=>$row["titulo"],	
			"horaPartido_inicio"=>$row["horaPartido_inicio"],
			"horaPartido_fin"=>$row["horaPartido_fin"], 	
			"fechaPartido"=>$row["fechaPartido"],
			"fechaTitulo"=>$row["fechaTitulo"],
			"fechaHoraConexion_inicio"=>$row["fechaHoraConexion_inicio"], 
			"fechaHoraConexion_fin"=>$row["fechaHoraConexion_fin"], 
			"duracionConexion"=>$row["duracionConexion"], 
			"duracionPartido"=>$row["duracionPartido"], 
			"horaConexion_inicio"=>$row["horaConexion_inicio"], 
			"horaConexion_fin"=>$row["horaConexion_fin"], 
			"fechaConexion"=>$row["fechaConexion"] ,	
			"cont_pc"=>$row["cont_pc"], 	
			"espectadores"=>$row["espectadores"] ,				
			"ip"=>$row['ip'],
			 "eLocal"=>($row["eLocal"]) ,	
			#"eLocal"=>utf8_encode($row["eLocal"]) ,	
			#"eVisitante"=>utf8_encode($row["eVisitante"]) 	
			 "eVisitante"=>$row["eVisitante"] 	
		);
			
		
			
		# Sumamos el total de la duracion de las conexiones por usuario
		$sumaDuracionConexion = $sumaDuracionConexion + $row["duracionConexion"];
		
	}  #fin while
	$stmt->close();
		
		
	# Asignamos a una variable los espectadores		
	$numEspectadores= $usuarios[0]["espectadores"];
	// echo "<br> numEspectadores: " . $numEspectadores ;
		
	# Calculo de los porcentajes para el rosco 
	$cont_pc =  $usuarios[0]["cont_pc"];
	$cont_app =   $numEspectadores - $cont_pc;
	// echo "<br> cont_pc app: " . $cont_pc ;
	// echo "<br> cont_app app: " . $cont_app ;
	$con_app_porcentaje = round(( $cont_app  * 100 ) / $numEspectadores);
	$con_pc_porcentaje = round( ( $cont_pc  * 100 ) / $numEspectadores);
	if ( $con_pc_porcentaje == 100 ) {
		$con_pc_porcentaje = 99;
		$con_app_porcentaje=01;
	}
		
		
	# Traduciendo fecha del titulo			
	$fechaTituloParts = explode(" ",$usuarios[0]["fechaTitulo"]); //0 dia //2 mes
	// echo '<pre>'; print_r($fechaTituloParts); echo '</pre>'; 
		
	$fechaTitulo_dia = diaSemana_fecha($fechaTituloParts[0]);
	$fechaTitulo_mes = mes_fecha($fechaTituloParts[2]);
	$fechaTitulo = $fechaTitulo_dia . " " .$fechaTituloParts[1] . " de " . $fechaTitulo_mes . " " . $fechaTituloParts[3]; 
		
	// echo "<br> fechaTitulo: ". $fechaTitulo;
				
	# Duracion partido (sacamos el numero de minutos que ha durado el partido)
	$duracionPartido = round($usuarios[0]["duracionPartido"]/60);
	// echo "<br>duracion partido mint: " . $duracionPartido . "minutos";
	
	// echo "<br> hora inicio partido: " . $usuarios[0]["horaPartido_inicio"];
	$tiempoParts = explode(":",$usuarios[0]["horaPartido_inicio"]);
			
	$horas = $tiempoParts[0] ;
	$minutos = $tiempoParts[1];
	$segundos = $tiempoParts[2];
	$contadorMinutos = $minutos;
	// echo "<pre>"; print_r($tiempoParts); echo "</pre>";
	
				
	//echo "<br>h:m:s ".  $horas . " : " . $minutos . " : " .  $segundos . " contador ( ". $contadorMinutos . ")";
	# Sacamos el array de los tiempos por minutos 18:20, 18:21....
	for($h=0; $h<=$duracionPartido; $h++){
		if($h!=0){
			if ($minutos <= 59){
				$horas = $horas ;
					
				if($minutos != 59) {					
					if($minutos < 9)
						$minutos = '0'.($minutos+1);
					else
						$minutos = $minutos+1;
					
				}else{	
					$horas = $horas+1 ; 
					$minutos = '00';
				}				
			}	// fin if 'minutos <= 59'
		} // fin if
					
		$varTiempo = $horas.":".$minutos;
		$tiemposPartido[$h] = $varTiempo; 
		$tiemposPartido_comparar[$h] = $usuarios[0]["fechaPartido"]. " ".$horas.":".$minutos. ":00";
		//echo "<br> varTiempo : " .$varTiempo ;
		
		# Creamos el array de tiempo de la zona europea para mostrar por pantalla
		date_default_timezone_set("Europe/Madrid");
		
		$local =date('r', strtotime($tiemposPartido_comparar[$h]) + date('Z', strtotime($tiemposPartido_comparar[$h])) );
		$localParts = explode(" ",$local);
		// echo '<pre>'; print_r($localParts); echo '</pre>'; 
		
		# Solo quiero la hora y el minutos
		$localParts2 = explode(":",$localParts[4]);
		$tiemposPartido_utc[$h] = $localParts2[0].":".$localParts2[1];			
			
		$contadorMinutos = $contadorMinutos + 1;			
	} //fin for

	// echo '<pre>'; print_r($tiemposPartido); echo '</pre>'; 
	// echo '<pre>'; print_r($tiemposPartido_comparar); echo '</pre>'; 
	// echo '<pre>'; print_r($tiemposPartido_utc); echo '</pre>'; 
		
	
	# Comparamos tiempo con usuarios , en cada tiempo cuantos usurios se han conectado????
	#Tiempo
	for($t=0; $t<=$duracionPartido; $t++){
		//echo "<br> t =" . $t . "<br>";
		$tiempoP = strtotime($tiemposPartido_comparar[$t]); // tiempo partido x minutos
			
		for($u=0; $u<=$numConexionesTotales-1; $u++){
			// echo "<br> u =" . $u . "<br>";
			$fechaHoraConexion_inicio = strtotime($usuarios[$u]["fechaHoraConexion_inicio"]);
			$fechaHoraConexion_fin = strtotime($usuarios[$u]["fechaHoraConexion_fin"]);				
			if( ($fechaHoraConexion_inicio < $tiempoP) && ($tiempoP < $fechaHoraConexion_fin) ){
				$contador  =  $contador + 1; 	
			}//fin if	
		} // fin for u
			
		//	if(empty($contador)){$contador = 0;}			
		$contadorUsuarios[] = $contador;
		$contador = 0;
	}//fin for t
	//	echo '<pre> users'; print_r($contadorUsuarios); echo '</pre>'; 
		
	# Calculo de Tiempo medio de visualizacion ( suma duracion  / espectadores)
	$mediaVisualizacion = $sumaDuracionConexion /  $numEspectadores ;
	
	# Calculo de viewersHours = Total espectadores * tiempo medio de visualizacion
	$viewersHours = (($mediaVisualizacion * $numEspectadores)/(60*60)) ;
	
	/*********************************************************************/
	# Ciudades
	# Consultamos primero en la bd 'stats_ciudades' sino estan los datos consultamos al servicio
	$ciudades = array();
	$posicionCiudad=0;
	$contandoConexionesMarisa =0;
	$a = 0;
	for($j = 0; $j<= count($usuarios) - 1; $j++){
		// echo '<br> '.$j .' - '.$usuarios[$j]['ip'];
		$stmt = $mysqli->prepare("select * from   streamsports.stats_ciudades where  ip = ?");
		$stmt->bind_param("s", $usuarios[$j]['ip']);
		$stmt->execute();
		$result2 = $stmt->get_result();	
		// echo "<br> conexion: " .  $result2->num_rows;

		if($result2->num_rows != 0){
			while($row2=$result2->fetch_assoc()) { 
				$cadena = $row2['ciudad'];
				$usuarios[$j]['ciudad'] =   $row2['ciudad']; // utf8_encode($row2['ciudad']);
			}
			$stmt->close();
			
		}else{	
			$stmt->close();
			$ip = $usuarios[$j]['ip'];
			$datosMaxMind = recogerDatosMaxMind($ip);
			//	echo "<pre>"; print_r($datosMaxMind); echo "</pre>";
			# Una vez tengo el array  hacemos el insert
			//echo "<br>encontrado: ".$datosMaxMind['enontrado'];
				
			if($datosMaxMind['enontrado'] == 1){
				# Comprobar que existe el codigo postal
				 $stmt = $mysqli->prepare(" SELECT	pob.poblacion,     pro.provincia,    c.comunidad FROM 
					streamsports.poblacion as pob
					inner join streamsports.provincia as pro on (pob.idprovincia = pro.idprovincia) 
					inner join streamsports.comunidad as c on (c.idcomunidad = pro.idcomunidad) where postal = ? ");
				$stmt->bind_param("i", $datosMaxMind['cp']);
				$stmt->execute();
				$result3 = $stmt->get_result();	
				$row3=$result3->fetch_assoc();
					
				
				if(empty($datosMaxMind['ciudad']))
					$datosMaxMind['ciudad'] = $row3['poblacion']; //utf8_encode($row3['poblacion']);
					
				if(empty($datosMaxMind['prov']))
					$datosMaxMind['prov'] = $row3['provincia']; // utf8_encode($row3['provincia']);
					
				if(empty($datosMaxMind['comunidad_autonoma']))
					$datosMaxMind['comunidad_autonoma'] = $row3['comunidad_autonoma']; // utf8_encode($row3['comunidad_autonoma']);
						
			} // fin encontrado = 1; 
			else{
				$datosMaxMind['ciudad'] = "Resto";
				$datosMaxMind['prov'] = "Resto";
				$datosMaxMind['cp'] = 00000;
				$datosMaxMind['comunidad_autonoma'] = "Resto";
				$datosMaxMind['pais_code'] = "Resto";
				$datosMaxMind['pais'] = "Resto";
				$datosMaxMind['continente_code'] = "Resto";
				$datosMaxMind['continente'] = "Resto";
				$datosMaxMind['lat'] = 0;
				$datosMaxMind['lng'] = 0;
				$datosMaxMind['ip'] = $ip;
			}
			//$stmt->close();
		
			$usuarios[$j]['ciudad'] =  $datosMaxMind['ciudad'];
			try{
				$stmt = $mysqli->prepare("INSERT INTO streamsports.stats_ciudades(provincia, ciudad, comunidad_autonoma, pais_code, pais, continente_code, continente, codigo_postal, lat, lng, ip) VALUES (?, ?, ?, ? , ?, ?, ?, ?, ?, ?, ?)");			
			}catch (mysqli_sql_exception $e) { 
				echo $e->getMessage(); 
				echo "<br>den error: ".  $mysqli->error;	
			} 

			$stmt->bind_param("sssssssssss",$datosMaxMind['prov'], $datosMaxMind['ciudad'] , $datosMaxMind['comunidad_autonoma'], $datosMaxMind['pais_code']  , $datosMaxMind['pais'] , $datosMaxMind['continente_code']  , $datosMaxMind['continente'] , $datosMaxMind['cp'], $datosMaxMind['lat'] , $datosMaxMind['lng'], $datosMaxMind['ip']);
			$stmt->execute();
			$stmt->close();
		}//else max mind
					
		
		# Controlamos por userId de la conexion, como contamos los espectadores
		if( !isset($idUsuariosConexion[$usuarios[$j]['idUserSession']])  ){	 // userId 	
			//echo "<br> !isset if " . $j ;
			$contandoConexionesMarisa ++;
					
			$idUsuariosConexion[$usuarios[$j]['idUserSession']] = 1;
			$idUsuariosConexion[$j]['posicion'] = $a;
					
			if( !empty($usuarios[$j]['ciudad']) ) // sino esta vacio su nombre 
						
				$idUsuariosConexion[$j]["ciudad"] = $usuarios[$j]['ciudad'];		
			else
				$idUsuariosConexion[$j]["ciudad"] = 'Resto';						
			
			//echo "<br> ciudad: ". $idUsuariosConexion[$j]['ciudad'];		
			# Sumamos para pasar a la siguiente ciudad
			$a++;
		}
		else{
			//echo "<br> !isset else" . $j ;
			$idUsuariosConexion[$usuarios[$j]['idUserSession']]  ++;	
			$idUsuariosConexion[$j]['posicion'] = $a;
					$idUsuariosConexion[$j]["ciudad"] = 'Nula';
		  }			
		}//for
		//echo '<pre> '; print_r($idUsuariosConexion ); echo '</pre>';  
		
		# Hago el recuento de ciudades con el array idUsuariosConexion
		for($m = 0; $m<= count($usuarios) - 1;  $m++){
			if($idUsuariosConexion[$m]["ciudad"] != 'Nula'){
				if( !isset($ciudades[$idUsuariosConexion[$m]["ciudad"]]) )				
					$ciudades[$idUsuariosConexion[$m]["ciudad"]] = 1;
				else
					$ciudades[$idUsuariosConexion[$m]["ciudad"]]  ++;
			}
		}
		
		
	//	echo "contandoConexionesMarisa: " . $contandoConexionesMarisa;
	//echo '<pre>'; print_r($idUsuariosConexion); echo '</pre>';  
	//	echo '<pre> ciudad: '; print_r($ciudades ); echo '</pre>';  
		
	# Ordenamos array de las ciudades por el valor, asi solo tenemos que mostrar la informacion
		
		
	//echo '<br>'.utf8_encode($usuarios[0]['eLocal']);  
			
		//echo '<pre>';		print_r($ciudades);		echo '</pre>';			
		$data['titulo'] = $usuarios[0]['titulo'];
		// $data['titulo'] = $usuarios[0]['titulo'] . ": " . $usuarios[0]['eLocal'] . " - " . $usuarios[0]['eVisitante'] ;
		$data['eLocal'] =  $usuarios[0]['eLocal'];
		$data['eVisitante'] =  $usuarios[0]['eVisitante'];
		$data['fechaTitulo'] = $fechaTitulo;
		$data['minPartido'] = $tiemposPartido_utc;
		$data['contUsuarios'] = $contadorUsuarios;
		$data['espectadores'] = $numEspectadores;
		$data['espectadores_aux'] = $contandoConexionesMarisa;
		$data['numOrdenadores'] = $con_pc_porcentaje ;
		$data['numMoviles'] =  $con_app_porcentaje;
		// $data['tmpMediaVisualizaion'] = gmdate("H:i", $mediaVisualizacion);
		$data['tmpMediaVisualizaion'] = gmdate("i:s", $mediaVisualizacion);
		$data['minutosTotal'] =  $duracionPartido ;
		$data['viewersHours'] =round($viewersHours,2);
		
		//echo '<pre>';		print_r($ciudades);		echo '</pre>';	
		
		$data['listaCiudades'] = $ciudades; 

		//echo '<br> *********************************************************<pre>'; print_r($data); echo '</pre>'; 
		echo json_encode($data);
	
	$mysqli->close();
	
	
	

?>